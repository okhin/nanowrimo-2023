title:          Simena
ID:             232
type:           md
summaryFull:    La diffusion de la vidéo de sa baston, amène tout le monde à la considérer comme étant dangereuse, y compris Xicha	Xicha demande à Simena de ne plus revenir au Naked Brocoli, suite à la diffusion de la vidéo.
                
POV:            0
notes:          {C:1:Coda-6}
                {C:8:Xicha}
                {P:3:Simena et Xicha} Pinch
                {P:1:Le Naked Brocoli} Pinch
                {W:63:Rythme rouge blanc}
                {C:11:Octane}
                {C:0:Simena Von Guyen the Third}
                {W:37:anneau zénithal}
                {W:6:Naked Broccoli}
                {C:4:Giuse-P}
label:          4
status:         5
compile:        2
setGoal:        1500
charCount:      8979


L'éclairage d'ambiance du compartiment bascule dans des couleurs chaudes, marquant le début de la ronde rouge. Simena n'a pas réussi à dormir, hypnotisée par les images qui défilent sur son moniteur de poignet, absorbé par les centaines de notifications sociales qui s'accumulent sur son profil.
Iel se voit en boucle étriper, littéralement, un de ses agresseurs. Iel se voit être accusé d'être à la solde du consortium, de n'être qu'un animal. Iel s'attendait aux questions déplacées et indiscrètes. Iel s'attendait à une certaine indifférence ou une certaine appréhension à son égard. Iel à l'habitude, et iel comprend, en partie, que certaines personnes soient mise mal à l'aise par son apparence. Les agressions physiques ne lea gène pas plus que ça, iel à souvent eu à se battre pour prouver qu'iel existe. Mais iel pensait avoir laisser ça derrière ellui, au fond des puits de gravité.
Ce qui lea blesse le plus, au final, ce sont les injures spécistes. L'animalisation de son image. Parce que c'est quelque chose que ses créateurs, au sein de Gene Sys, on scrupuleusement établi. Iel est génétiquement plus un animal qu'un être humain Et en tant que tel, iel n'as pas la même reconnaissance de droits que ses collègues mineurs de fond.
Mais les syndicats ouvriers avaient, en quelque sorte, réussit à faire en sorte que les autres considèrent les hybrides comme, plus ou moins, des égaux. C'est ce qui l'énerve d'autant plus. Même les syndicats là bas, au fond des puits de mine, sont plus armés pour gérer les problèmes que son existence soulève, que les personnes ici. Peut-être parce qu'ielles n'ont jamais eu à y faire face. Peut-être parce qu'ielles ont une vision plus restreinte de ce qu'est censé être une personne. Mais ce n'est pas son problème, ielle ne devrait pas avoir à subir les effets de leur ignorance.
La porte du compartiment s'ouvre, lea sortant de ses pensées. Octane entre dans l'espace, et active un peu plus de luminosité, pour casser la morosité. Simena se redresse et rassemble ses quelques affaires dans son sac, pour laisser à Octane la place dont elle a besoin.
"Hey, pars pas…" dit-elle, alors que Simena franchit le seuil. Sa voix est marqué par l'épuisement, elle essaye de trouver un moyen de continuer à faire circuler l'information entre les nomade, de continuer malgré tout à maintenir leur RAFT en état, et elle passe beaucoup trop de temps éveillé.
"J'ai besoin de me changer les idées," répond Simena. "Et toi de dormir."
Octane ne répond pas, se dirigeant vers la couchette. Simena attrape un des blousons de la mécanoe, et s'apprête à fermer la porte du compartiment qu'iels partagent.
"J'oubliais," ajoute Octane, étouffant un bâillement. "Prend un des talkie. On reste en contact comme ça. Et… Quoi qu'il se passe, tu es toujours la bienvenue ici."
Simena attrape le talkie qu'Octane ellui a préparé, et le fourre dans son sac, avec le reste de ses affaires. Iel disparaît dans les coursives de la station, enfonçant son visage le plus profondément possible sous la capuche du blouson carmin, un peu trop serré pour iel.
Iel n'entend pas le "Prend soin de toi" que lui lance Octane, qui part s'effondrer dans sa couchette. Iel erre un peu dans les coursives qui parcourent les espaces d'habitations du secteur. En dépit de ses efforts pour essayer de dissimuler son visage, iel sent les regards la dévisager. Iel entends les injures murmurés à son attention. Iel voit les couples accélérer le pas, intimidé par sa présence. Simena sert les poings dans ses poches, iel ne veut pas leur donner à voir ce qu'iels veulent.
Son subconscient guide ses pas, et iel se retrouve face au Naked Brocoli. Un des rares lieu dans lequel iel se sent bien et à peu près accepté. Mais alors qu'iel franchit les portes battantes et qu'elle pénètre dans la salle à manger collective, iel est accueillie par un silence. Un silence chargé d'accusation, de réprobation. Une accusation tacite d'avoir transgressé un interdit. Et tout le monde qui lea dévisage, qui lea se fait sentir encore plus étrangère qu'iel ne l'est déjà.
"Allez, c'est juste moi…" dit-elle, essayant de rassurer les inquiétudes des habituées, des gens avec qui iel à passer quelques soirées à boire et discuter. Espérant qu'iels sont au-dessus de tout ça. Qu'au moins certaines feront l'effort d'ignorer la tempête sociale qui ravage ses notifications.
Mais personne ne bouge. Personne ne lui répond. Les regards se fixent sur le fond des verres, sur les motifs floraux des papiers peints délavés, sur n'importe quoi qui ne puisse leur renvoyer leur regards lâches, sur n'importe quoi qui ne les fera pas se sentir coupable et honteux.
"Simena," la voix de Xicha brise le silence, et la fait sursauter. "Viens, il faut qu'on parle", ajoute-t-elle. Sa voix a les intonations des chefs d'équipes, quand ils devaient aller annoncer les accidents de mines aux familles. La voix de celles et ceux qui ont été désignés pour transmettre les mauvaises nouvelles.
Simena suit Xicha dans la cuisine, de laquelle sortent rapidement deux personnes, laissant en plan l'assemblage de ce qui ressemble à un gâteau.
"Il se passe quoi ?" demande Simena.
"C'est la merde…" commence Xicha. Se tenant juste hors de portée d'étreinte de Simena.
"Je… J'ai essayé de leur faire comprendre, mais ils sont trop têtus. Ils ont peur. Peur de toi. Peur d'être associés à toi. Et… Et ils ont forcés un vote, comme si c'était comme ça qu'on prenait les décisions ici."
Xicha tourne autour du pot. C'est la première fois que Simena la voit manifestement désemparée par quelque chose, au point qu'elle n'arrive pas à aller a l'essentiel.
"Ils ont voté… Et j'ai insisté pour que ce soit moi qui te dise. Parce que je t'apprécie, et qu'il vaut mieux que ça vienne de moi je pense. Même si je pense qu'ils ont tort. Ils…"
Xicha ferme les yeux, et essaye de se recomposer une image de calme. Simena est suspendue à ses mots et attends une sanction, ça ne peut pas être autre chose.
"Ils ne veulent plus que tu vienne ici. Ni que tu n'utilise la plateforme sociale du Naked Brocoli. Ils essayent de se justifier en disant qu'on est pas censé utiliser la violence pour régler nos problèmes, mais ils ont simplement peur de toi, et ils ne veulent pas l'admettre."
"Et toi, répond Simena, tu as peur de moi aussi ?" iel a du mal à articuler, le verdict lea percutant en pleine poitrine, lui coupant le souffle.
Xicha marque une pause avant de commencer à formuler une réponse, mais Simena ne veut pas l'entendre. Iel ne veut pas prendre le risque d'être décu par Xicha.
"T'embête pas à répondre, je préfères ne pas savoir…"
Son hésitation parle de toutes façon pour elle. Simena sort de la cuisine, une sensation de vertige s'emparant d'ellui alors que son monde semble s'effondrer. Son moniteur est devenu silencieux. Ses notifications ont disparus, ainsi que son réseau social, mais iel n'y prete pas attention, iel titube en sortant du Naked Brocoli, sous les regards accusateur et les rires moqueurs d'une partie de l'assistance, qui se détache du silence de culpabilité qui empli toute la pièce.
Iel trébuche sur quelqu'un, qui lea rattrape et l'aide à se tenir droite sur ses jambes, lea tenant par l'épaule.
"Ça va ?" demande la personne vêtue d'une combinaison bleu nuit démodée, donnant un peu l'impression qu'il est vêtu d'un de ces costumes que portent les cadres des puits de gravité.
Simena ne répond pas. Ellui jette juste un regard menaçant, le surplombant d'une vingtaine de centimètre. Il est particulièrement petit et maigre, presque émacié. Ses yeux gris sont emplis d'un mélange d'ambition et de sympathie. Simena y trouve quelque chose de récomfortant.
"Je suis Giuse-P," dit-il, lea tenant toujours par l'épaule, n'attendant pas une réponse à sa première question. Ce nom rappelle lui est familier, mais iel ne parvient pas à trouver un contexte associé.
"Et?" répond-iel.
"Et alors tu n'es pas seule," répond-il simplement.
"Ça me fait une belle jambe."
"Je t'ai laissé un message, lit-le et recontacte moi." il lui lâche enfin le bras, et commence à se détourner.
"Attends…" lui demande Simena. "Je n'ai plus de messagerie. Il dit quoi ton message ?"
Guise-P lui explique ce qu'il comprend de la situation, comment iel a été piégé, et par qui. Que depuis le début, iel n'avait aucune chance. Il lui explique qu'il a réuni un petit groupe de personnes qui ne croient pas en la version officielle de cette attaque, et qu'ils se réunissent au Monster Bash de temps en temps, et qu'iel est lea bienvenue, qu'iel trouvera là-bas du monde pour l'écouter, lea croire, et l'aider.
Puis, une fois qu'il a finit son discours, il s'en va, un léger sourire aux lèvres, laissant partir Simena. L'écouter a permis à son anxiété de se calmer. Iel a les idées un peu plus claires maintenant, suffisamment pour ne pas vouloir trop réfléchir à son état émotionnel, à Xicha, à Kit ou à Octane.
Iel se mets en route vers un endroit depuis lequel iel sera un peu plus tranquille. Et depuis lequel iel pourra se recréer un compte sur une plateforme sociale 