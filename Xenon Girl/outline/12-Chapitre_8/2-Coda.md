title:          Coda
ID:             230
type:           md
summaryFull:    Vestal rentre en contact avec Simena pour une interview non publique, et parler de son agression
POV:            1
notes:          {C:1:Coda-6}
                {C:0:Simena Von Guyen the Third}
                {P:5:Coda et Simena} Plot turn
                {C:10:Bey}
                {W:14:Idols}
                {C:14:Staxy}
                {W:3:construct artificiel de réminiscence autosuggestive}
                {W:24:Syndicat}
                {C:2:Coelia}
                {W:105:le mouvement}
                {W:86:oracle}
                {C:16:Sanchez}
                {W:62:Compte social}
                {W:5:Xenon Girl}
label:          4
status:         3
compile:        2
setGoal:        1500
charCount:      8844


Coda est anxieuse. Il se passe quelque chose en ce moment dans le complexe social de RAFT qui ne lui plaît pas, et ses tripes lui hurlent de fuir. Pour une fois qu'elle est en accord avec ses émotions, et pas celles de Coelia, il faut que ce soit sur du stress et de l'anxiété.
Bey s'est incrustée dans son paysage émotionnel dévasté, mais dont l'inquiétude pour Coda semble être en décalage complet avec l'environnement gris et lourd qui entoure Coda. Décalage encore renforcé par la robe à fleur, aux couleurs vives, que porte la scénariste.
Des fleurs au milieu de la cour bordée de murs brutaliste gothique. L'écart mémétique est tellement important que l'esprit de Coda préfère ignorer cette dissonance, et reste concentrée sur les analyses sémantiques de la crise actuelle.
Crise qui l'a un peu prise par surprise. Le montage grossier et évident de la vidéo violente de cette agression n'aurait pas du survivre plus de quelques heures dans l'infosphère. Et les premières réactions avaient convaincues Coda qu'il n'était pas nécessaire de traiter le sujet immédiatement, et qu'elle aurait le temps de mener une enquête approfondie, et d'identifier et d'arrêter les coupables.
Son train de pensée manque de dérailler. Elle n'est pas censée penser comme ça, ce sont les effets secondaire de Sanchez qui l'amène à envisager des arrestations. Ce à quoi elle devrait penser, ce serait de faire en sorte que ce genre d'agression physique ne se reproduise plus. D'essayer de comprendre les facteurs structurels et individuels, et d'essayer de proposer des façons de régler ça.
C'est son taff d'idol. C'est ce pour quoi elle a choisit, initialement, de faire ce taf. Mais la situation a dérapé, est partie complètement hors de contrôle. Elle n'est pas encore certaine de savoir pourquoi, mais son intuition lui hurle que c'est lié à la crise du mouvement, de cette conspiration qui est persuadée qu'une cabale capitaliste a pris le contrôle de RAFT.
Peut-être que la fatigue générale a rendu le système d'attention plus poreux aux thèses de ce mouvement ? Peut-être que l'aspect de la victime, Simena, suffit à l'aliéner ? Ou peut-être que, à force de toujours tourner en rond, les complexes culturelles des habitantes de la station se sont emballés, se gavant de cette explosion de violence, oubliant au passage tout sens critique.
Et des choses aussi simple que les protocoles de crises utilisés depuis des années pour essayer de dés-escalader la situation semblent maintenant hors jeu. Du moins, c'est le sentiment qu'à Coda dans sa discussion passionnée avec Bey.
"Je sais qu'iel est montrée comment étant l'agresseure, Bey. Je le vois bien. Mais justement… C'est la seule chose que nous montre ce montage de vingt secondes. Tu ne crois pas qu'il faudrait essayer d'aller l'écouter ?"
"On ne peut pas donner l'impression que l'on est OK avec la violence physique."
"Mais ça n'a rien à voir avec ça, merde !" Coda se sent basculer sur la défensive. On essaye de lui retirer son dossier, de la forcer à courber l'échine, et à laisser le Syndicat gérer les choses.
*Et je n'ai pas l'intention de me laisser sucrer le dossier sans me battre*. Encore Sanchez qui déborde.
"Je veux juste aller lea voir, s'assurer qu'iel va pas trop mal. Et entendre ce qu'iel a à dire," ajoute Coda.
Depuis qu'elle a découvert l'identité de l'hybride homme-cochon sur les vidéos, Coda essaye de trouver un compte social actif. La seule chose qu'elle a à se mettre sous la dent, pour le moment, c'est un compte sur l'instance du Naked Brocoli. Mais celui-ci est archivé, ce qui implique que la personne s'occupant du compte a, d'une façon ou d'une autre, perdu l'accès à celui-ci.
Mais une rapide analyse du flux de l'instance du Naked Brocoli indique à Coda que Simena n'est plus lea bienvenue dans ce lieu. Et que même si ce n'était pas le cas, Coda imagine mal comment iel pourrait encore y mettre les pieds, vu le ton que prend la conversation autour de la vidéo.
Une majorité des commentaires, les plus soutenus, ceux qui sont mis en avant par les administrateurices de l'instance, sont sur une ligne ferme mais polie de rejet de la violence, et condamne globalement les actions de Simena. Le reste est partagé entre celles et ceux qui, autour d'un certain Kit, essayent de convaincre tout le monde d'aller au-delà de cette vidéo, et d'essayer de comprendre ce qu'il a réellement pu se passer, et ceux qui sont clairement de mauvaise fois et cherchent juste à vouloir défendre leurs espaces contre les monstres du capitalisme, usant d'un humour ciblé ou de non-argument, reprenant une partie des techniques d'argumentation que Coda a vu dans les espaces interstitiels ces temps-ci.
Et Coda sait que cet espace de l'infosphère est à l'image de beaucoup d'autres. Que ces communautés, déjà mises à l'épreuve par les attaques sur les groupes de travail, sont en train de perdre leur cohérence et leur cohésion. Pour ajouter encore plus au chaos ambiant, les administrateurices de certaines instances sociales ont commencés à couper les liens de syndication avec d'autres.
Leur but est de limiter l'afflux de message, de limiter les inondation de messages qui ralentissent une bonne partie des réseaux sociaux, et de pouvoir se concentrer sur la gestion interne des discussions. Mais il devient de plus en plus difficile pour Coda, ou pour le Syndicat, de suivre les tendances et les conversations.
"Iel a été viré du Naked Brocoli", dit Coda, rompant le silence, et commençant à traquer l'activité passée de Simena, notamment via son activité au sein des communautés techniques. "Iel est seul, Bey. Il faut qu'on fasse quelque chose…"
"En plus de ton dossier en cours ? Tu es certaine de vouloir faire ça ?" Bey a changé de ton, elle n'essaye plus de défendre une ligne politique pour le Syndicat, mais elle essaye de guider son idol.
"Il faut bien que quelqu'un le fasse, non ? C'est pas à ça que son censée servir les idols ? C'est pas à ça que je suis censée servir ? Et si personne ne le fait, il va se passer quoi… Iel va continuer comme ça. isolé au milieu de tout ce monde qui a peur d'ellui ? Ou on attend que quelque chose de pire ne se passe ?"
"Je m'inquiètes juste un peu pour toi. Tu as l'air investie émotionnellement, et c'est bien, ça te change. Mais…" Bey cherche ses mots.
"Mais ?" demande Coda, poussant Bey à lui dire ce qui ne lui va pas.
"Mais je suis pas à l'aise avec les CARA-1K, avec toute ces charges de travail, avec cet état de stress dans lequel tu es. Dans lequel on es toutes en vrai. Mais toi particulièrement. Et tu n'as pas à le faire, tu n'as pas à être la seule sur le coup."
"Je ne suis pas seule," répond Coda rapidement, par réflexe. "Il y a Coelia qui est là pour ça. Et puis j'ai une partenaire sur le coup aussi avec Staxy." Le vocabulaire de Sanchez qui continue de déborder sur sa diction, sur sa grammaire.
"Je veux juste que tu fasse attention à toi, c'est tout," lui dit Bey. "Et que l'on se coordonne pour la publication de quoi que ce soit. Parce que c'est déjà le bazar un peu partout, il faudrait qu'on évite d'alimenter tout ça."
Bey reste encore quelques instants assise à côté de l'idol avant de laisser Coda parcourir les médias sociaux qui ont colonisé son espace cognitif les uns après les autres.
Elle n'a toujours pas trouvé de moyen direct de contacter Simena, si ce n'est qu'elle semble avoir pas mal bossé avec Octane, la mécano qui organisait la réunion à laquelle elle participait l'autre jour, celle qui a dégénéré. Staxy la connait bien apparemment, peut-être qu'elle tiens un moyen d'approcher Simena, d'essayer de lui faire passer des informations.
Staxy est en ligne. Elle glisse dans ses messages privés.
"Hey."
"Hey ma belle", répond Staxy.
"Je suppose que tu as vu la vidéo de baston ? Je sens qu'il y a un lien avec notre histoire et je me disais qu'on pouvait peut-être creuser un peu ? J'aimerai lui parler."
"D'accord," la latence entre leurs messages crée une distance, et semble amener Staxy a la concision. À moins que ce ne soit le fait que Coda ai décidé de ne pas relever les marques d'affection de celle-ci. Elle ne sait pas trop quoi en penser pour le moment
"Iel semble être en contact avec Octane, tu pourrais lui transmettre une demande d'interview ? Avec Coelia. Pas un direct, personne n'a la bande passante pour ça, mais une discussion en off ?"
"Je fais suivre."
"Je te dérange ?"
"Non," répond laconiquement Staxy, avant qu'un autre message n'arrive. "Désolée pour le ton un peu abrupt. Il faudrait qu'on se croise pour parler d'un truc ou deux. Du personnel, du genre qui ne passera mieux que de visu."
Avant que Coda ne réponde, un autre message arrive.
"Et je te mets en lien avec Octane. Je pense que tu as raison, il faut parler à cette personne. Surtout si on veut prendre le temps de faire les choses bien. Je lui dit de te contacter."
Puis, en signe de fin de transmission, un "Bisous," vient clôturer la conversation.