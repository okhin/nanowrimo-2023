title:          Simena
ID:             231
type:           md
summaryFull:    Trop impressionnée par le fait de rencontrer son idole, Simena panique, se braque et s'enfuit face aux questions
POV:            0
notes:          {C:2:Coelia}
                {C:0:Simena Von Guyen the Third}
                {P:5:Coda et Simena} Pinch
                {W:5:Xenon Girl}
                {W:51:douce-néon}
                {W:120:puits de gravité}
                {W:72:riot porn pop}
                {W:14:Idols}
                {W:78:agent}
                {W:3:construct artificiel de réminiscence autosuggestive}
                {W:112:interface entoptique}
                {W:102:vitres polarisées}
                {W:14:Idols}
label:          4
status:         3
compile:        2
setGoal:        1500
charCount:      8830


Simena a d'abord cru qu'Octane se moquait d'ellui, avant que celle-ci ne lui montre les messages que Coelia lui a envoyé. Les messages qui lui demandent si Octane a un moyen de contacter Simena, et de lui demander s'iel veut bien lui parler.
Simena a ensuite pensé que Coelia cherchait surtout à défendre les habitantes de RAFT, puis, après de longs aller-retours par messagerie textuelle, et après avoir été convaincue par l'idol de ses intentions, Simena a ommencé à perdre ses moyens.
Coelia, la front-girl des V.S.T.L, la personne avec qui iel a fait le trajet depuis les puits de mine de Luna, certes, sous forme synthétique, la personne qui a hanté et hante toujours en partie ses rêves quand iel se sent trop seule, cette Coelia, veut lui parler. Simena n'a pas su répondre de suite, paralysée par la timidité.
Mais maintenant, iel est arrivé au Xenon Girl, leur lieu de rendez-vous. La décoration neon-sweet illumine de couleurs cyanosée le lieu, évoquant les clubs de strip un peu classe de Valles Marineris, le genre d'endroit dans lequel Simena n'était pas lea bienvenue. Organisé autour d'un ilot central depuis lequel les volontaires font circuler les cocktails signature du lieu, et sur lequel les performeurs pratiquent probablement leur art, l'espace est ensuite ouvert, sans organisation perceptible. Et au-dessus, organisés comme les balcons des salles de spectacles terrienne antique, les box privés.
C'est dans un de ces box que Simena s'est installé. Iel à poser sur la table son talkie, qui est branché sur une des fréquences d'écoutes de la ci-bi. La voix d'Audrey annonce les prochains titres qui vont passer, mais Simena n'y prête pas vraiment attention. Iel a du mal à se concentrer sur quoi que ce soit, d'autant que les shots de moonshine commencent à faire leur effet.
Iel s'est remis à boire. Et, vu la résistance de son métabolisme, iel boit beaucoup pour sentir les effets de l'ivresse. Ça n'améliore pas son humeur, mais l'alcool amorti les regards des autres, la sensation d'être une étranger, d'être une animal.
Audrey annonce encore une paire de morceau dans sa petite ci-bi, et Simena commence à s'impatienter. Iel préfère attendre ici, dans ce box aux vitres polarisées, isolé du reste du monde. Même si le Xenon Girl est, pour ainsi dire, vide, si iel doit attendre seul que le temps passe, iel peut faire ça depuis le compartiment d'Octane.
Mais cette dernière insiste pour qu'iel sorte régulièrement des vingt mètres cubes qui délimite son espace. Alors, iel sort et cherche à se mettre à l'écart, allant dans les endroits ou presque plus personne ne va, emmitouflé dans d'épais blouson à capuche, essayant de rester occupé en réparant ce sur quoi iel arrive à se concentrer.
Au moins, ici dans ce bar abandonné de presque tout le monde, personne ne vient l'embêter.
Iel se laisse porter dans ses pensées circulaires, lorsqu'iel réalise que Coelia est entrée dans son box. Sa silhouette fine, élancée, est découpée par les lumières néons de l'espace collective. Son corps est à peine couvert d'une tunique blanche, que Simena jurerais être transparente, ses mollets sculptés par les heures de chorégraphie, sont enserré dans des lacets de cuir synthétique, ses cheveux rose sont retenus en arrière par un bandeau bleu et, alors qu'elle sort du contre jour pour venir s'asseoir à côté de Simena, son visage est lourdement maquillé, cachant le manque de sommeil, mettant en valeur ses pommettes saillante et son nez retroussé.
Le cœur de Simena manque un battement. Sa dépression est anéantie par l'arrivée de l'idol, qui envahi toute la conscience de l'hybride. Le petit sourire de Coelia achève les quelques digues émotionnelle de cellui-ci, inondant ses capacités cognitives avec ses fantasmes.
Iel sent une montée de chaleur alors que Coelia finit par s'asseoir. Elle lui a peut-être dit quelque chose. Mais Simena n'a pas enregistré cette information. L'alcool, le stress, la fatigue, la dépression, tout ce cocktail a raison de ses capacités cognitive et iel est dans un état de choc.
L'idol se penche vers elle, la touchant du bout des doigts, cherchant à déterminer si Simena consent à un contact physique plus prononcé. Iel finit par reprendre légèrement le contrôle, l'alcool ayant été dissipé par le choc émotionnel. Iel sent des larmes couler sur ses joues, alors que Coelia lui parle, lea rassure d'une voix calme. D'une voix de quelqu'un habituée à se trouver dans ce type de situation.
"Respire… Prend ton temps…" lui dit-elle.
"Tu veux boire quelque chose ?" ajoute l'idol, voyant que Simena finit par reprendre un peu ses moyens.
Iel refuse l'offre d'un geste et se mets à fixer la table, manipulant le talkie de ses larges mains, sautant de fréquence en fréquence, plus pour s'occuper que cherchant réellement de la musique. De toutes façon, les canaux de la ci-bi sont saturés de messages et de discussions, formant une communauté technique persistant à travers les ondes, en dépit des soucis de charges des moyens numériques.
Coelia lui a passé un bras autour de l'épaule, et l'a invité à poser sa tête sur la sienne.
"Désolée pour ça" dit Simena, après que ses larmes aient finalement expulsé la surcharge émotionnelle.
"C'est OK. Je suis là pour ça," lui murmure Coelia, du ton que l'on prend lorsque l'on échange des confidences sur l'oreiller, de cette voix impliquant une confiance mutuelle, une intimité partagée.
"Je suis aussi là pour qu'on parle." ajoute-t-elle, passant sa main dans les cheveux rêches et court de Simena.
"Parler de quoi ?" répond-iel, se redressant.
"De ce qui t'es arrivée," dit Coelia, après avoir cherché quelques instants ses mots. "Parce que ce n'est pas quelque chose qui est censé se produire. Pas sur RAFT du moins."
"C'est à cause de moi," répond Simena. Iel dresse un constat, une résignation.
"Je ne pense pas. Du moins, pas à cause de choses sur lesquelles tu as un contrôle. Mais ce qui m'embête le plus, dans tout ça, c'est que la seule chose qu'on a, qui circule, est un clip vidéo mal monté. Et ça viens se greffer à un genre de culte conspirationniste, et tout ça crée un peu une tempête mémétique parfaite. Dont tu es une victime."
"C'est la faute de personne alors, c'est ça ?" répond Simena. "Quatre personnes se décide de se payer une tranche de jambon, de se servir sur la bête, mais c'est la faute de personne en particulier, c'est juste une tempête parfaite et personne n'y est pour rien ?"
"Ce n'est pas ce que j'ai voulu dire," répond Coelia. Simena se dégage, glisse sur la banquette et prend de la distance avec son idol. Iel fait un effort pour enfouir les images sexuelles, nombreuses, de l'idol qu'iel s'est construite, et essaye de ne pas se laisser avoir par son envie de proximité physique, d'intimité.
"Alors, qu'est-ce que tu as voulu dire ? Que vous êtes toustes complètement à la masse, incapable de gérer un peu d'aliénation ? Qu'il est plus confortable pour vous de voir ça comme quelque chose d'inévitable et de venir ensuite me faire quoi… la morale ? la leçon ? me demander de changer mon comportement pour que ça ne se reproduise pas ?" Simena sait qu'iel est injuste, mais iel s'en moque. Iel dit, hurle plus exactement, ce qu'iel a sur le cœur. Iel reproche à Coelia l'ensemble de ses problèmes, compare les membres de la station aux pires communautés isolationniste lunaire, fait des équivalences politiques qui ne supporteraient même pas un examen superficiel par un gosse de dix ans.
Les reproches pleuvent, Coelia ne dit rien, elle reste là, assise dans sa tunique très courte, au décolleté très plongeant, elle écoute et supporte le déferlement de colère de Simena, calmement, sans être effrayée par la colère de la personne en face d'elle.
Puis, une fois qu'elle ne trouve plus rien à reprocher à quiconque à part à elle-même, Simena, déjà debout, se précipite hors du box. Coelia lea rattrape d'une main.
"Reste en contact. Je te promet que tout ce que tu me dis, ou hurle, reste juste entre nous." lui dit-elle. "Je suis disponible pour toi tout le temps."
Simena ne sait pas si c'est quelque chose de normal, que Coelia dit à tous ses fans, ou si iel bénéficie d'un traitement de faveur. Iel descend en courant les escaliers du Xenon Girl, manque une marche et se rattrape de justesse à la rambarde métallique. Iel titube presque en sortant du bar et iel marque une pause une fois arrivé dans les coursives, loin du regard des gens, hors de la volonté de Coelia de l'aider.
Iel ne sait pas comment gérer ça. L'impassibilité de Coelia face aux horreurs qu'iel lui a balancé. Le fait qu'elle veuille toujours l'écouter, l'aider. Pas comme Octane, qui subit Simena plus qu'autre chose. Non, comme quelqu'un qui semble être réellement intéressée pour l'aider.
Et, sur son communicateur, une notification associée au nouveau compte qu'Octane lui a créé sur une des petits serveurs des nomades. Une demande d'ami de la part de Coelia.
