title:          Giuse-P
ID:             1867
type:           md
summaryFull:    Faisant face à la diffusion large de la vidéo de son agression, Simena broie du noir.
                Giuse-P la contacte.
POV:            4
notes:          {C:4:Giuse-P}
                {W:11:Monster  Bash}
                {C:12:Tik}
                {C:17:Yas}
                {W:105:le mouvement}
                {W:25:Vocas}
                {W:50:coursiers de maintenance}
                {C:0:Simena Von Guyen the Third}
                {W:36:Chernobyl}
                {W:6:Naked Broccoli}
                {W:121:moonshine}
                {C:5:Mark-V}
                {W:96::-}
label:          4
status:         5
compile:        2
setGoal:        1500
charCount:      9816


Assis sous une fresque phosphorescente représentant une horde de momie se battant contre une meute de loup garous, Giuse-P préside l'assemblée réunie dans l'espace commun du Monster Bash. Il s'est installé définitivement dans les espaces supérieusr, désertés par les habituées du lieu, et il passe l'essentiel de ses périodes d'activité ici, à rassembler les preuves de l'existence de la conspiration.
Tout autour de lui, installés aux tables d'acier, ceux qu'il considère plus comme ses ouailles que comme des amis ou des camarades sont plongés dans leurs plateformes sociales. Ils s'attaquent au groupes de travail. C'est une cible facile. Il leur suffit de les inonder de questions et ils finissent par devenir incapable de fonctionner, appelant au calme et au silence, comme les bons agents de la cabale qu'ils sont.
Et le plan de Giuse-P, pour peu qu'il en ait un, est de forcer la cabale à s'exposer, pour permettre à tout le monde d'ouvrir les yeux sur la réalité de leur vie.
Derrière lui, légèrement en retrait, se tiens Yas, une voca habituée des lieux et qui est devenue sa discrète assistante. Même si elle n'a que la moitié de son âge, elle fait preuve d'une loyauté sans faille doublé d'un fort besoin de reconnaissance. Besoin qu'il exploite à son avantage, et qui lui permet d'avoir quelqu'un sur qui il peut compter pour savoir ce qui se dit en son absence.
Elle est absorbée dans les analyseurs mémétiques, dans les graphes sociaux qui se forment dans les espaces interstitiels, mais aussi au sein de collectifs de la station. Elle a déjà remarqué quelques lignes de ruptures vis à vis de la façon de réagir à la crise. Il l'observe discrètement, scrute sa poitrine à peine dissimulée par la combinaison bleue vif qu'elle porte ouverte jusqu'au nombril. Discrètement, il attrape sa vieille caméra. Une vieillerie non connectée, qu'il faut alimenter avec des cartes mémoires. Une caméra qui n'est pas affectée par les brassard des vocas. *Clic*. Le bruit du déclencheur capture la silhouette de Yas et la stocke dans les mémoires persistante de l'appareil en aluminium brossé.
"G tu devrais jeter un œil à ça," lui dit Tik, son second, s'asseyant face à lui et le sortant de sa fixation. "C'est en train d'exploser dans tous les cercles sociaux sur lesquels on a un œil, genre plusieurs milliers de vues en seulement quelques minutes, et ce en dépit de la latence."
Tik lui tend un terminal en plastique orange. Un de ces terminaux qu'ils utilisent pour paraître plus nombreux, pour perturber les pistes numériques et compliquer la vie de celleux qui vont inévitablement essayer de les identifier.
Ils ont déjà déjoué une tentative d'infiltration d'une idol, il faut qu'ils soient plus prudent. Cet incident lui a cependant permis de resserrer un peu plus les liens du collectif, et, depuis, ils récupèrent énormément de terminaux abandonnés et créent une armée de compte pour parcourir les instances sociales, et pouvoir bouger facilement en cas d'infiltration.
Sur l'écran, une vidéo en pause. La légende est simplement "Capitalist pig". Une vidéo courte, enchaînant rapidement des plans très serré, montre une femme cochon se précipiter sur un groupe de coursiers de maintenance et en étriper un, un sourire vicieux sur les lèvres alors que son visage se retrouve couvert de sange, avant de prendre la fuite.
Giuse-P passe la vidéo plusieurs fois. Le clip ne dure guère plus qu'une dizaine de seconde, mais il cherche à confirmer s'il s'agît d'un montage ou si ce sont des choses qui ont eu lieu. Derrière lui Yas se rapproche, pour regarder la vidéo par-dessus son épaule. Il concentre son attention sur le sujet de cette vidéo, et repensera à Yas plus tard, quand il sera seul et qu'il pourra regarder les photos d'elle qu'il prend.
"Tu me fais un résumé du sentiment général ?" cette remarque est autant à destination de Tik, qui a probablement une réponse, mais aussi à Yas, qu'elle mette en place un filtre de recherche sur l'un des relais de vocas auquel elle a accès, et que les vocas utilisent pour accéder, au moins en lecture, aux plateformes sociales desquelles ils sont censés être privés.
"D'abord un état de choc, de panique. Une telle violence est quand même quelque chose de rare chez nous. Et, bon, on voit bien que la vidéo est lourdement éditée, ce qui a ensuite généré une petite salve de messages de doutes."
Les habitudes de critiques des médias sont bien implantées dans les habitants de la station, c'est ce qui fait que leur travail est parfois complee, et que les idols sont affreusement efficaces.
"Mais, reprend Tik, cette critique du média a dérapé, des doutes sur les motivations des critiques ont commencés à émerger et à reprendre certains de nos argumentaires."
"Le sentiment dominant, ajoute Yas, semble être celui de la peur, de la méfiance. Par contre, la bande passante de ces messages commence à se faire sentir, surtout avec la vidéo. J'ai pas mal de latence."
"Donc voilà," reprend Tik, jetant un regard à Yas lui intimant de se taire et de le laisser parler, "On a notre avancée, non ? Notre message est renforcé par cet incident, et de plus en plus de monde questionne la présence d'une autorité morale ou l'idée de laisser des agents corporatistes évoluer librement dans RAFT."
Giuse-P reste silencieux. Il joint ses mains devant sa poitrine, et essaye de se donner un air d'importance, de réflexion. Cette vidéo, cette agression, est une aubaine. Mais le message autour lui échappe, ce n'est pas lui qui est en charge de sa propagation ni, pour autant qu'il ne le sache, de l'agression initiale.
"Bon, il va falloir reprendre le contrôle narratif sur ça. Yas, essaye de me trouver l'identité des victimes. Et essaye de voir aussi s'il n'y aurais pas une version plus longue, avec plus d'images.
"Tik, il va falloir qu'on en sache plus sur elle. Depuis quand elle est là, à quoi ressemble son profil social, tout ça."
"Elle… la truie ?" demande Tik, reprenant le terminal des mains de Giuse-P.
"Oui, elle. On va devoir basculer le narratif si on veut qu'il nous soit utile. Et je vais avoir besoin de lui parler. Alors je veux tout savoir, je veux que tu la suive s'il le faut."
"Tu as un plan précis ?" demande Tik, faisant paraître, une fois encore, des doutes sur la capacité de Giuse-P à diriger les opérations. Il ne cesse de remettre en question ses décisions, ses plans. Il faut que Giuse-P le remette à sa place. Mais il est de bonne humeur en ce moment, alors il va faire ce que lui suggère Yas quand ils sont seuls, et qu'ils parlent de la gestion du groupe. Il va prendre la peine d'expliquer sa pensée au lieu de juste le renvoyer à sa place.
"La vidéo est lourdement éditée. Que des plans serrés, filmés depuis l'épaule des quatre victimes. Ce qui me fait dire que le reste de la séquence vidéo ne montre pas du tout la même chose. Du véritable travail d'amateur.
"Moi, je me serai arrangé pour faire une séquence plus longue, plus difficile à réfuter. Les premières critiques ont raisons, quelque chose ne va pas dans cette vidéo. Et j'en déduit donc que la truie là, n'est pas l'agresseuse.
"Et donc, je veux la rencontrer. parce que si elle est la victime, alors toute cette opération est…"
"Est une diversion de la cabale," complète Tik, acceptant le raisonnement comme le sien. "Elle s'appelle Simena. Et… C'est iel en fait."
"Comment tu l'as trouvée aussi vite ?" demande Giuse-P, légèrement surpris par la vitesse d'action de Tik,
"Parce qu'il n'y a qu'un seul hybride homme cochon dans toute la station, et qu'iel est arrivé avec le Chernobyl. Tu sais, celui qui vient de Jupiter ? Bref, iel s'appelle Simena von Guyen the Third, d'après le manifeste publié sur Contrôle, et iel passe beaucoup de temps au Naked Brocoli. Je te file un lien vers son profil complet."
Il y a quelque chose dans la façon de répondre de Tik qui énerve Giuse-P. Mais il se tait, ce n'est pas à lui d'entrer dans ce jeu, il trouvera d'autres moyens de le faire.
Il prend le profil de Simena et le parcoure rapidement. Giuse-P se lève et se dirige vers les escaliers qui l'amène dans ses appartements privés, Yas le suivant quelques pas en retrait.
"Je vais contacter cette Simena, et j'ai besoin de rester un peu seul," *et de te mater à moitié à poil sur ces photos* pense-t-il. "Prend le reste de la journée, défoule toi dans la salle d'arcade. On va avoir du boulot plus tard, alors profites-en."
Elle lui sourit, fait demi-tour et pars vers le fond de la salle d'arcade, non sans que Giuse-P ne sorte subrepticement sa caméra et augmente sa collection personnelle de quelques clichés pris à son insu.
Il s'enferme dans son bureau et réfléchit à la marche à suivre. Il n'a aucune chance de se faire accepter au Naked Brocoli. Il n'as pas le bon passing pour cette communauté, et il n'as pas envie de trop chercher à se déguiser. Mais il peut contacter Simena directement, et ellui proposer un rendez-vous ailleurs.
"Salut Simena, commence-t-il. On ne se connaît pas, mais j'ai vu ce qui t'étais arrivé. Je ne crois pas que tu ne soit autre chose qu'une victime dans cette histoire. J'aimerai te rencontrer, et discuter avec toi. GP:-" ellui écris-t-il dans les messages privées du compte de l'hybride. L'indicateur d'envoi du message se fige une paire de seconde avant de signaler la réussite de l'envoi du message.
Il n'espère pas vraiment de réponse. Mais ce message lui sert de préambule à une rencontre physique. Il sait où traîne Simena, il lui suffit d'aller profiter des jardins en face du Naked Brocoli, et d'attendre qu'elle y passe. Mais avant de se mettre en route, il repense à Yas, à sa peau brune, reflétant les néons du lieu sur son ventre.
Il attrape la caméra, et commence à faire défiler les clichés qu'il a commencé à accumuler de la voca. Il arrive même à se convaincre qu'elle fixe la caméra sur plusieurs d'entre eux, qu'elle est d'accord, que c'est leur petit jeu à eux.