title:          Simena
ID:             254
type:           md
summaryFull:    Elle décide de rester, parce qu’elle a maintenant quelque chose à perdre..
                Simena et Xicha sortent ensemble
POV:            0
notes:          {C:0:Simena Von Guyen the Third}
                {C:8:Xicha}
                {P:3:Simena et Xicha} Resolution
                {P:0:Intégration de Simena} Plot turn
                {W:74:Zone de transfert}
                {W:112:interface entoptique}
                {W:18:bazar}
                {W:144:curry}
                {W:91:cuves hydroponique}
                {W:145:tokamaks}
                {W:10:Consortium}
                {W:6:Naked Broccoli}
                {W:19:gravel rock}
                {C:26:Sven}
label:          4
status:         5
compile:        2
setGoal:        1500
charCount:      11700


Les odeurs d'épices se mêlent à celles du mycélium extrudé et grillé et emplissent l'atmosphère surchauffée de la petite salle du restaurant auquel Xicha a emmené Simena. les murs sont couverts de photos de Jakarta, d'avant sa destruction par la montée des eaux. D'avant l'évacuation des indonésiens vers les différents chantiers spatiaux.
En dépit de l'affluence, Xicha a pu négocier une table juste pour iels deux, avant de disparaître pour discuter avec une partie des habituées. Simena en profite pour essayer de comprendre le menu qui lui est proposé, flottant quelque peu sur un nuage émotionnel. Iel se sent bien pour la première fois depuis longtemps et Xicha n'y est pas étrangère.
Cette dernière finit par arriver, les bras chargés de plats que Simena ne parvient pas à identifier. Les plats de riz, de brochettes d'extrusion de mycélium et les légumes en sauces épicées peuplent rapidement la petite table de plastique.
Xicha enlève sa veste de plastique et la suspend dans une des sangles accrochée à la cloison qui borde leur table, réarrangeant ses longs cheveux noirs en une queue de cheval un peu lâche et s'assoit en face de Simena, commençant à piocher dans les bols les différents éléments de leur repas.
Simena prend un peu de temps avant de la rejoindre, iel est encore un peu perdu dans ses émotions et essaye de profiter un peu de cet instant. Et iel a trop chaud. Que ce soit la température trop élevé de ce petit boui-boui mal aéré, les épices qui stimulent un peu violemment ses papilles ou les pieds nus de Xicha qui jouent contre sa jambe, alors que celle-ci lui envoie des petits sourire entre deux brochettes au saté, iel a bien trop chaud.
"Tu aimes ça ?" demande Xicha, remarquant que Simena a à peine touché à la nourriture.
"Oui, beaucoup. C'est juste… J'ai pas l'habitude." répond-iel.
"L'habitude de quoi, de la bouffe ?"
"Oui, mais c'est pas ça. Je veux dire, même la bouffe rapide ici est cent fois meilleure que ce que je connaissait avant de venir ici. Mais c'est être posée tranquillement, avec quelqu'un dont je n'ai pas l'habitude. J'essaye d'en profiter un peu du coup."
Xicha avale une gorgée de thé d'une capsule pressurisée, puis elle pose sa main sur celle de Simena, posant ses doigts fins, mais entaillés par les lames de couteaux de cuisine, sur la sienne aux doigts ronds, fait pour manipuler des foreuses au fin fond d'une mine.
"Je suis désolée que tu en soit arrivée là," finit par dire Xicha. "Que tu n'ai pas pu te sentir tranquille ici. Je…"
Simena sent de légers tremblements dans la main de Xicha.
"C'est pas ta faute."
"Peut-être. Mais j'aurais pu faire plus. J'aurai _dû_ faire plus. Et maintenant, il va falloir réparer tou…"
"Stop," l'interromps Simena, retirant sa main, croisant ses bras sur sa poitrine. "S'il te plaît, stop. J'ai pas envie de t'en vouloir. Et je ne suis pas quelque chose à réparer, d'accord ? On répare un vaisseau ou un système de filtration. Mais moi je ne suis pas un objet à réparer." Iel inspire un peu. Tant pis pour la tranquillité, mais iel préfère dire ce qu'iel a besoin de dire plutôt que d'être tranquille.
"Et si je ne suis pour toi que quelque chose de blessé, de cassé, un projet de réparation et de remise en état… Alors je préfère rester seul."
Il faut quelques instants pour que Xicha réponde. Elle prend le temps de chercher ses mots.
"Tu n'es pas juste un projet pour moi. J'ai envie de te connaître, de passer du temps avec toi. Mais il va falloir que nous, les habitants de cette station, dont j'aimerai que tu fasse partie, procédions à quelque changement. Parce que tu ne peux pas rester dans cet état d'accord ?  
"Et avant que tu ne me dise que tu as des raisons d'être autant sur la défensive, je sait. Et je ne te dis pas que tout va bien, ça va être un processus. Ça risque de prendre du temps, mais j'aimerais qu'un jour tu puisse te poser quelque part dans cette magnifique station, et que tu te sentes en sécurité. Et que je fasse partie de ta vie.
"Je ne te demande pas le bénéfice du doute, ni de me faire confiance. Pas encore. Mais il faudra que tu me laisses rentrer dans ta vie à un moment. Et en attendant, je suis là. Et je suis désolée."
Simena n'aime pas tellement la sensation de culpabilité que les mots de Xicha éveille en ellui. Une culpabilité qui lui donne envie de se lever et de partir pleurer. Mais iel sait que Xicha a raison, du moins en partie. Il va falloir qu'iel aille mieux, qu'iel apprenne à laisser rentrer des gens dans sa vie, même s'iel n'a jamais eu l'occasion de le faire.
Iel attrape la main de Xicha, qu'elle a laissé de son côté de la table, son bras séparant les bols métallique vides qui encombrent encore la table, et la serre dans la sienne.
"Je vais essayer," finit-iel par répondre en cherchant à mettre de côté cette culpabilité. "Mais il va falloir qu'on m'apprenne comment faire ça."
"Tu veux en parler ?" demande Xicha.
Non, Simena ne veux pas en parler. Mais iel va devoir si iel veut que Xicha reste dans le coin. Alors iel résume sa vie, à rebours. De son arrivée sur RAFT, à sa "naissance" il y a quinze ans dans une cuve amniotique, avec un corps adulte, et une psychée patchwork, assemblée à partir de CARA et de patch mnémorithmique.
Iel raconte comment, de la douzaine d'autre polyzigote qui ont éclot en même temps qu'ellui, la plupart n'ont pas survécu à leur première année, disparaissant dans les puits de mine au fur et à mesure des accidents.
Comment iel a mis une dizaine d'année à comprendre que ses conditions de vie n'étaient pas normale, et à continuer de perdre ceux que la plupart des gens auraient considérés comme étant ses frêres et sœurs, comment les gréves des syndicats et leur répression l'ont emmenées à participer à des opérations de sabotage, faisant le travail risqué que personne ne voulait faire. Comment iel a finit par comprendre que même pour ses camarade, iel n'était bon que pour aller à la baston et rien d'autre.
Récapitulées ainsi, les quinze années de sa vie ressemblent à une épreuve constante. Mais il y avait de meilleurs moments dit-iel, pour essayer de se rassurer aussi bien que pour rassurer Xicha. Les idols déjà, étaient capable par leur sourire de prendre sur elles une partie du fardeau de son existence. Même si elles étaient payées pour ça, que toute cette socialisation est basée sur des mensonge, elle permettait à Simena de tenir le coup.
Iel sait que c'est la raison d'être des groupes d'idol. Que c'est pour ça qu'il y a autant de karaoké sexy impliquant les V.S.T.L. Que c'est ce qui permet aux travailleurs d'accepter les choses. Mais Simena n'a jamais eu vraiment de personne à qui faire confiance.
Iel finit par se taire. Xicha est evnu s'asseoir à côté d'ellui. Elle ne fait pas de commentaire, ne pose pas de questions. Elle accepte juste tout ce que Simena a besoin de dire en lea tenant dans ses bras, et en l'embrassant dans la nuque de temps en temps, pour l'encourager à continuer, pour lui dire qu'elle est là.
Iels restent silencieux quelques instants, Simena se roulant en boule dans les bras de Xicha avant que celle-ci ne se décide à briser le silence.
"Allez, viens. On va commencer par régler le problème du Naked Brocoli," dit-elle. "Fais moi confiance, ils ne peuvent pas vraiment se passer de moi de toutes façon, même s'ils ne veulent pas l'admettre."
Simena se remémore l'état du Naked Brocoli la dernière fois qu'iel a essayé d'y aller, se rappelant les piles de vaisselles s'entassant, l'absence d'odeur de pâtisserie chaude, ou la scène désespéramment vide.
"J'ai vu. Mais c'est quelque chose que je ne comprends pas… Pourquoi personne n'as pris le relais ?"
"Parce que les volontaires qui donnaient un coup de main savent qu'ils ne doivent pas aider. Parce que je le leur ai demandé, faire grève toute seule n'a pas de sens."
"Mais si une majorité de gens te soutiennent comment…"
"Pas une majorité. Une poignée de volontaires. Des vocas ou des gens comme Kit, qui font en sorte que les lieux fonctionnent. Mais pas une majorité."
Iels couvrent rapidement la distance entre le restaurant et le Naked Brocoli. Simena remarque qu'il y a plus de monde dans les coursives qu'au plus gros de la crise, les habitantes essayant probablement de se reconnecter les unes aux autres, alors que certaines plateformes commencent à renégocier des accords de syndications.
Alors que le trottoir roulant les ramènes vers le Naked Brocoli, Simena sent son cœur accélérer et sa vision se réduire.
"Ça va aller," lui dit Xicha alors qu'iels descendent du trottoir et s'approchent de la cantine. Elle ne lui laisse pas le temps de reprendre son souffle, avant de franchir les portes et de revenir dans le lieu.
Les regards de a douzaine de personnes présentes dans la salle se tournent progressivement vers iels. Simena sent ses mains devenir moite, elle se sent réduire en taille, se faire écraser par l'obscurité du lieu. L'imposant silence donne l'impression d'avoir ordonner aux circulateurs atmosphérique de se taire alors que Xicha, la tirant par la main, l'amène à ses côté au niveau d'un comptoir encombré de déchets et de vaisselle sale.
Sven, dominant Xicha d'une bonne tête, se lève avant de déclamer, se donnant toute l'importance qu'il parvient à accumuler :
"Iel n'est pas bienvenu ici. Le collectif a voté et…"
"Et je m'en fout," répond Xicha, d'une voix ferme ne laissant pas de place à l'argumentation. "Iel est avec moi, et tant pis pour ceux que ça emmerde, vous allez devoir vous habituer."
"Mais…" essaye tout de même de répondre Sven, soutenant difficilement le regard de colère de Xicha.
"Est-ce que j'ai l'air de suggérer que c'est ouvert à débat ? Vous m'avez contraint à participer à ce vote stupide, alors que ce n'est pas comme ça que l'on prend les décisions ici," Xicha fixe alternativement les autres habitués du lieux, les forçant les uns après les autres à baisser les yeux. "Et en plus, vous n'êtes même pas capable de maintenir ce lieu en un état fonctionnel et accueillant. Alors voilà comment ça va se passer."
Xicha marche vers Sven, tenant toujours la main de Simena, et levant les yeux pour fixer le colosse blond.
"Tu te barres de mon chemin, tu me laisse aller chez moi avec Simena, et tu n'interfère pas dans ma vie. Et, si tu veux que je revienne vous donner un coup de main, si vous voulez que d'autres volontaires viennent redonner de la vie à ce lieu, tu as intérêt à changer d'attitude."
Sven détourne le regard et ses épaules s'affaisse. Il sait qu'il ne peut rien contre Xicha. Il sait aussi que personne d'autre ne voudra s'opposer à elle.
"Oh," ajoute-t-elle, se retournant vers Simena, "J'ai failli oublier. J'espère qu'après ça le sujet sera clos." Et elle embrasse Simena, doucement d'abord, parce que Simena est tendu, figé sur place, puis un peu plus fougueusement, faisant courir ses mains sur la peau de Simena, alors que Simena lui rend la pareille.
"Voilà. J'espère que c'est clair," dit Xicha, s'écartant de Simena. "Et vous avez intérêt à avoir rangé ce bordel quand on reviendra."
Simena sourit un peu bêtement, iel est encore tendue, mais Xicha a décidé de ne pas lui laisser le temps de souffler, et la tire vers l'escalier qui amène aux cabines d'habitations. Il y a bien quelques protestations qui s'élèvent de la salle, mais Xicha n'y prête pas attention, et Simena est absorbée par l'indonésienne qui la traîne dans sa cabine.
Avant que Xicha me referme la porte derrière elle, et après qu'elle ait jeté Simena sur son lit, iels entendent de la musique s'élever de la salle commune.
Simena commence à vouloir dire quelque chose, mais Xicha lui mets un doigt sur la bouche.
"On parle plus tard. Pour le moment j'ai d'autres idées en tête. Si tu veux bien ?"
Simena veux bien et se laisse porter par Xicha alors que celle-ci prend le contrôle de la situation.