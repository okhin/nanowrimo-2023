title:          Yas
ID:             242
type:           md
summaryFull:    Simena est acceptée au Monster Bash, mais veut reprendre le contrôle de son identité, qui lui échappe.
POV:            17
notes:          {C:0:Simena Von Guyen the Third} {C:17:Yas}
                {P:0:Intégration de Simena} Midpoint
                {P:8:Émancipation de Yas}
                {W:112:interface entoptique}{W:11:Monster  Bash}{C:4:Giuse-P}{C:12:Tik}{W:12:Bonecos de empurar}{C:5:Mark-V}{C:2:Coelia}{W:40:Just a Big Dumb Spaceship}
label:          4
status:         5
compile:        2
setGoal:        1500
charCount:      8472


Les photos défilent dans le champ visuel de Yas, son interface entoptique les superposant à son champ de vision. Suspendue au millieu des bougainvilliers et autres nyctaginacées qui s'accrochent à  la façade du Monster Bash. Elle passe trop de temps dans le lieu, et personne ne s'occupe de maintenir en état les abords du lieu collectif, comme sont censés le faire les participants et habitants des différentes sections. Mais les habitués de la salle d'arcade ne viennent presque plus.
Les dernières vocas à venir éclater les contrôleurs de DDR, et qui ont essayées de nombreuses fois de venir passer du temps avec elle ont finit par jeter l'éponge. Elle se demande comment elles occupent maintenant leur temps entre leurs entraînement de poursuite. Probablement en allant danser dans une autre salle d'arcade. Ou à traîner dans les salons de karaoké, à émuler les icônes des idols de leur choix.
Les voix distordues des *Bonecos de empurar* rugissent dans son casque, l'isolant des cris venant de l'intérieur du Monster Bash. Une autre des raisons pour lesquelles elle préfère être dehors. Giuse-P et Tik s'engueulent. Encore. Au sujet du papier fleuve qu'ont publié Coelia et Staxy. Elle en a commencé la lecture et elle ne sait pas quoi en penser. Giuse-P lui a défendu de le lire, alors elle préfère ne pas lui en parler, mais elles ont raisons sur le fait que cette histoire de message prophétique ne tiens pas la route.
En marge de son champ de vision, les photos que Tik leur demande de faire circuler continuent de défiler. Elle ne peut pas y répondre, juste y accéder en lecture-seule. Et uniquement à travers un relai : un bout de code qui tourne discrètement sur un des serveurs de vocas et qui permet à ceux-ci de pouvoir suivre un peu ce qu'il s'écrit sur l'infosphère publique. Elle annote les images, mais surtout les interactions de celle-ci, celles qui génèrent le plus de réactions.
Invariablement, ce sont toutes celles qui utilisent les vues les plus violentes de l'altercation entre Simena et ses agresseurs qui génèrent les effets que recherchent Giuse-P. Elle devrait d'ailleurs les isoler pour les recommander et améliorer l'engagement, mais elle ne parvient pas à s'y résoudre. Surtout, la stratégie de Giuse-P, au moins depuis la publication de l'article, n'est plus une stratégie cherchant à convaincre. C'est une stratégie d'engagement pour l'engagement. De division. Même elle, qui n'est pourtant dans le métier depuis longtemps, le vois bien.
"Hey, Simena!" crie-t-elle à l'hybride qui se dirige d'un pas déterminé vers le Monster Bash. Cellui-ci s'arrête, cherchant du regard la voix qui l'appelle. "En haut! Dans les plantes!". Simena finit par capter son regard. "Bouges pas, j'arrive!". Yas raccroche ses outils de taille à son baudrier et d'un coup de descendeur, se retrouve sur la dalle de béton, sentant la chaleur de celui-ci sous ses pieds nus.
"Quoi de neuf?" demande Yas.
"Giuse-P est là ?" demande Simena, "il faut que je lui parle."
"Ouais, mais … Je serais toi j'attendrai un peu qu'il ai finit de s'expliquer avec Tik." Simena semble hésiter entre jaillir à l'intérieur du bar, poussant les portes battante d'un coup de botte, et être soulagée de devoir attendre un peu.
Yas suspend le flux d'image venant du relais qui lui parasitent son champ de vision. Elle n'a pas envie de la voir comme ça, comme un animal enragé.
"Tu voulais lui parler de quoi?" lui demande Yas, s'asseyant sur la rambarde séparant la coursive du vide, donnant sur les jardins qui tapissent le fond de l'anneau.
"De trucs et d'autres avec lesquels je suis pas forcément d'accord." Simena dégraphe le plastron rigide de sa combinaison technique, et s'appuie sur la rambarde, essayant de ne pas trop regarder vers le bas.
La seconde peau ainsi exposée fume, laissant échapper la transpiration de Simena, témoin de la forte capacité d'isolation de ces combinaisons semi-rigide. Yas se demande pourquoi quelqu'un porterait une combinaison de ce type aussi loin à l'intérieur d'un habitat.
Un groupe de trois personnes passe en contrebas et, remarquant Simena au-dessus d'eux, lui adressent des insultes et lui font des gestes obscènes, avant de continuer leur route.devant la terrasse et laisse fuser quelques injures avant de partir en courant. Simena essaye de rester imperturbable, mais Yas voit ses phalanges blanchir alors qu'iel serre les poings.
"En fait je voulais lui parler de ça," dit Simena, se redressant légèrement. "Je ne peux aller nulle part sans me faire insulter comme ça, et cette campagne qu'il mène ne fait qu'empirer les choses."
"Il sait ce qu'il fait." Yas répond sans même y avoir réfléchit, sur la défensive.
"D'accord, répond Simena après quelques instants à chercher une réponse. S'il sait ce qu'il fait, alors quel est son plan?"
Yas cherche une réponse. Mais elle ne sait pas vraiment répondre. Elle est probablement la personne la plus proche de Giuse-P, de ses idées, de ce qui pourrait être un plan, mais il ne lui en parle jamais.
"Et si tu ne sais pas répondre à cette question, relance Simena, ça veut juste dire qu'il ne te considère pas comme digne de confiance."
Yas ne répond pas. Elle sait bien que Giuse-P ne lui fait pas spécialement confiance, c'est normal, elle débute. Elle aimerait bien qu'ielles parlent stratégie politique ensemble, qu'il détaille ses plans et ses projets. Mais il est toujours trop occupé. Alors elle prend des notes, compile ce qu'il se dit et se passe dans et autour du mouvement, et essaye de comprendre quel est le plan. Elle essaye de se convaincre qu'il y a un plan.
"Possible qu'il ne me fasse pas confiance, oui. Et je vois bien que sa stratégie te fais du mal, mais tu sais comment il est…"
"Non, et ce n'est pas une excuse. Il faudra qu'il change, parce que moi, je ne vais pas pouvoir supporter ça très longtemps." Simena est cramponnée à la rambarde, essayant de maintenir une apparence de calme. Changer de sujet de conversation, se dit Yas. Pour essayer de lea sortir de cette douleur.
"T'as écouté un peu les Bonecos de empurrar?" Yas lui a filé quelques recommandations musicales vu que Simena tourne en boucle sur la porn-pop de V.S.T.L, elle s'est dit qu'elle pourait écouter autre chose de relativement proche.
"Ouais. J'écoutais ça tout à l'heure, quand je me baladais dans l'anneau nadiral."
"Tu faisais quoi là-bas ?" demande Yas. Elle sait que quelques vocas vont régulièrement se promener là-bas, pour s'échapper, flirter, ou simplement explorer. Mais y aller seule ne lui a jamais semblé intéressant.
Simena lui parle de ses projets de sculptures dans cette partie de la station, un endroit où iel peut aller sans se faire insulter. Ça permet à Yas de garder l'esprit loin des histoires de Giuse-P et de Tik, et de ne pas se poser trop de questions sur son positionnement à elle.
Et, une fois qu'iel s'est faites un peu priée, Simena finit par lui montrer ses projections, ses idées. Iel lui parle des galeries de caryatides et de haut relief qu'iel a creusé dans les parois du Just a Big Dumb Spaceship. Iel lui explique pourquoi iel préfère la sculpture et les jeux de lumière à d'autres techniques créatives, au fait que les pilotes de long courriers sont nombreux à customiser leurs habitats, que cela les aide à supporter la routine de maintenance et de supervision constante.
La conversation tourne aussi autour de tout cet espace quasi-disponible dans cet anneau désert. Dans le fait qu'il serait théoriquement possible pour tout le monde d'avoir de quoi s'occuper et de quoi faire pour les prochaines années si quelqu'un se décidait à commencer ce projet.
Yas objecte que personne ne peut décider de se lancer seul dans un tel projet, ce à quoi Simena répond que c'est par cette mentalité que personne n'obtiens jamais rien. Que ce sont les limites des luttes institutionnalisées, prenant l'exemple des syndicats ouvriers dans les mines lunaires.
Yas en oublie les luttes du mouvement, ayant pour la première fois depuis longtemps une véritable discussion qui parle d'organisation et de politique, pas juste de logistique et d'engagement social. Simena semble pouvoir utiliser son expérience avec les différents mouvements de lutte contre le consortium comme source d'argument pour appuyer sa dissertation.
Mais au final, Yas bute sur les ressentiments de Simena. Et chaque fois qu'elle essaye d'amener la conversation sur les raisons pour lesquelles Simena a décidé de quitter les puits de gravité, cellui-ci élude et contourne la réponse en restant évasive, ou en changeant de sujet.