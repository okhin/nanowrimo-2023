title:          Simena
ID:             243
type:           md
summaryFull:    Simena questionne les croyances du groupe
POV:            0
notes:          {C:0:Simena Von Guyen the Third}
                {C:4:Giuse-P}
                {P:2:Le Monster Bash} Midpoint
                {W:63:Rythme rouge blanc}
                {C:17:Yas}
                {W:25:Vocas}
                {C:12:Tik}
                {C:2:Coelia}
                {W:105:le mouvement}
                {W:11:Monster  Bash}
                {C:19:Thig}
                {C:20:Bay Lee}
                {W:96::-}
                {W:129:système fédératif}
                {C:5:Mark-V}
                {C:11:Octane}
                {C:8:Xicha}
                {W:84:hydrazine}
label:          4
status:         5
compile:        2
setGoal:        1500
charCount:      10757


Yas a raison, iel a besoin de temporiser un peu les chose,s de ne pas foncer tête baissée dans ses problèmes, et de prendre le temps de parler avec Giuse-P. Et iel aime bien passer du temps avec la vocas. C'est l'une des rares personnes avec qui iel peut parler franchement de sujets politiques, de ses inquiétudes vis à vis de la gouvernance du mouvement, ou de ses angoisse de devenir un symbole.
Puis Tik, et quelques autres, quittent le Monster Bash avec grand bruits, annonçant à qui veut bien l'entendre qu'ils en ont terminés avec les délires de Giuse-P, que celui-ci est un traître à la cause et que c'est de sa faute si le mouvement ne parviens à rien. Ça fait quelques temps que les membres du mouvement se disputent entre eux, et ce ne sont pas les premiers départs, mais Tik était le second de Giuse-P jusqu'à encore peu de temps.
Son excuse pour partir, le fait que Giuse-P manipule le mouvement à son propre profit, et les éloigne de leur combat est, ironiquement, exact. Simena le sait. Iel pense que Yas en a conscience aussi à un certain niveau. Mais cette excuse n'est qu'un prétexte, une façon pour Tik de dissimuler le fait que, la raison pour laquelle il part, c'est qu'il veut prendre le contrôle du mouvement. Le reportage des idols n'est qu'un prétexte, une opportunité pour lui de raviver ses propres velléités de contrôle.
Simena laisse tomber l'idée de parler à Giuse-P. Du moins pour le moment. Il n'y a aucune chance qu'il soit disposé à l'écouter. Ou à écouter qui que ce soit. Pas tellement qu'il soit, généralement, ouvert à la critique et à la discussion, mais Simena devine, en entrant dans la salle d'arcade, et en voyant Giuse-P descendre d'une traite le contenu d'une bouteille d'hydrazine, que ses faibles chances de se faire écouter son maintenant nulles.
Alors iel s'est installée à une borne d'arcade pour s'occuper l'esprit et les mains en guidant un vaisseau pirates entre les forces de police du consortium. Étrangement, un jeu absolument pas modifié par le Syndicat, venant directement du consortium. Yas lui a amené une bouteille d'hydrazine et lea laisse tranquille, pendant qu'elle passe d'une table à l'autre pour faire le boulot que personne ne veut faire.
Simena garde une oreille attentive distraite sur les conversations qui se tiennent entre les tables. Le départ de Tik a servi de carburant à de nombreuses conversations alors qu'iel arrive sur un de ces boss qui remplissent l'écran d'attaque et qui nécessite de mémoriser les motifs des projectiles, pour rester hors d'atteinte. Ce à quoi iel n'est, définitivement, pas suffisamment entraînée.
Mais le temps qu'iel aille attraper quelque chose à grignoter dans les réserves, et s'installer au comptoir, les conversations ont changé. Elles ont repris leur tournure habituelle. Essentiellement une plainte constante de l'oppression de la cabale dont ils sont victimes.
Et iel en a eu marre. Peut-être que si iel ne subissait pas toutes cette merde, et qu'iel n'était pas au centre de campagnes de harcèlement, peut-être que s'ils se comportaient mieux avec Yas et cessaient de l'ignorer, peut-être alors qu'iel se serait contenter de hausser les épaules au lieu d'intervenir.
Mais les entendre se plaindre des conséquences de leur choix, de leur vision étriquée du monde, se penser victime d'une oppression de la cabale, alors qu'ils n'ont jamais souffert de ce type de discrimination, les entendre, encore et encore, reprendre des concepts politiques qui ne peuvent pas s'appliquer à cette situation, ce soir c'est trop pour ellui.
"Franchement les gars," dit-iel, se retournant pour faire face à la salle, s'apercevant que Giuse-P lea fixe dès qu'iel a pris la parole, "Arrêtez de parler de ce que vous ne connaissez pas."
"Qu'est ce que tu veux dire par là ?" demande Thig, interrompant sa conversation avec Bay-Lee.
"Que vous n'êtes pas victime d'oppression. Personne ne vient défoncer les portes de vos containers au milieu de la nuit, personne ne vous restreint l'accès à des services vitaux, ou même à des divertissements, personne ne vous empêche même de vous exprimer. Vous ne reconnaîtriez pas l'oppression si elle vous éclatait le nez à coup de botte coquée." répond Simena avec la subtilité d'une truie se vautrant dans une bauge.
Le temps qu'iel termine sa phrase, et les autres se sont replacés dans la salle. Les conversation se sont tuent, et seule sa voix. Iel se retrouve face à une demi douzaine de personne, touts prêtes à en découdre verbalement, prêtes à défendre leur vertu. Le tout sous le regard satisfait d'un Giuse-P qui semble prendre un plaisir particulier à voir la scène se dérouler.
"Vous n'avez aucune idées de ce que c'est," Reprend-iel. "Vous vous êtes montés la êtes touts seuls avec vos histoire de cabale, parce que vous vous ennuyiez. Mais vous ne manquez de rien. Vous ne manquez même pas de moyens de communication et d'organisation. Et…" iel s'arrête avant de parler de Yas, mais iel n'aurait pas pu continuer longtemps sans être interrompu par Thig.
"Et donc, tu fais quoi de tous les mensonges autour de :- ?" dit celui-ci.
"Le fait qu'on te mente n'est pas…" essaye de répondre Simena.
"Ou même que, aussitôt que l'on a essayé d'exposer cette cabale, que l'on a commencé à poser des questions, alors ils ont commencés à nous empêcher de communiquer, à couper les liens de syndications pour nous empêcher de nous coordonner ? Qu'est ce que tu as à répondre à ça ?" attaque ensuite Bay Lee.
Les arguments pleuvent comme les coups d'un boxer sur la garde de Simena, lea poussant dans les cordes et ne lui laissant pas de temps pour répondre. La mauvaise fois et les exemples extrêmes, sortis de tout contexte remplacent les arguments qu'énonçait Yas quelques heures auparavant.
Et Simena sait qu'il ne sert à rien de vouloir répondre à tout ça. Iel sait qu'il lui suffit de parler de la manière dont ils traitent Yas pour, peut-être, les amener à voir qu'ils ne sont pas aussi vertueux qu'ils ne le pensent. Mais ce n'est pas à ellui de le faire, surtout que Yas ne lui as pas demandé d'aide.
Alors iel encaisse et essaye de répondre comme iel peut. jusqu'au moment où l'arbitre vient siffler la fin du match. Giuse-P s'avançant tranquillement, écarte Thig et Bay-Lee, et vient se positionner à côté de Simena, lui passant une main autour de l'épaule tout en lui jetant un sourire condescendant.
"Et tu fais quoi des messages de MV ?" demande-t-il. Simena hésite avant de répondre, iel se demande si c'est une de ces quetsions rhétoriques dont il abuse, ou s'il attend réellement une réponse de sa part.
"J'en fait rien. Je ne dis pas qu'il n'y a pas quelque chose de suspicieux dans RAFT. Je dis juste que ce n'est pas une oppression."
"Et donc, parce que l'on est pas oppressé, on ne doit rien faire pour les autres, pour celles et ceux qui souffrent des actions de la cabale, de leurs mensonges ?"
Giuse-P est positionné de façon à l'isoler complètement. Certes, il a cesser la bagarre en ajoutant de la distance entre Simena et ses interlocuteurs, mais il s'est aussi placer de façon à mettre Yas dans son ombre, de façon à positionner Simena au centre de toute l'attention.
"Si, probablement. Mais tu sais que…" iel ne peut pas terminer sa phrase. Giuse-P ne l'écoute plus, il lea met au pilori, mais iel n'a plus la possibilité de parler. Iel ne pourrait de toutes façon rien dire pour se défendre.
"Si même toi, toi qui vient des puits des gravité, toi dont l'aspect à été conçu pour être un outil, un animal de bat, si même toi tu te fais avoir par le langage de la cabale, qui cherche à nous isoler, à couper toute la solidarité des uns avec les autres, alors tu comprends bien, vous comprenez bien," dit-il, concentrant maintenant son attention sur son public, "Vous comprenez bien à quel point notre combat est d'autant plus nécessaire, non ? La cabale nous manipule tous. Ils utilisent leurs armes mémétiques pour nous obliger à nous entre déchirer, pour nous empêcher de parler un langage commun, de donner un sens clair et précis à chacun de nos mots.
"MV a raison. Il faut tordre le sens des mots de la cabale, il faut déconstruire toute forme de discours. Il faut aller au-delà de la syntaxe et de la rythmique pour trouver le sens réel de leur message.
"Et que dit ce message ? Obéissez. Taisez-vous. Faites nous confiance. Mais moi je veux leur répondre que non. Qu'il y a autre chose dans la vie que l'obéissance. Même si l'on mange à notre faim. Même si notre air est filtré. Même si nous disposons de plateforme de communications évoluées. Tant que la cabale contrôlera tout ça, nous ne pourrons jamais nous reposer."
Giuse-P ne prête déjà plus attention à Simena, trop occupé à haranguer ses ouailles, à tenir des discours sans queue ni tête. La lassitude se saisit d'ellui. La tension et les conflits de ces derniers jours l'épuisent, et iel n'arrive qu'à peine à s'accrocher au fait que, ici, on l'accepte. On la laisse aller et venir.
Et puis, et iel s'en rend compte en regardant Giuse-P déblatérer, s'accorcher à sa propre importance pour essayer de surnager au-dessus de la crise qui menace son mouvement, iel ne peut s'empêcher de se demander combien de temps ce mouvement va-t-il continuer à exister, avant de n'être balayé par l'inévitable chute de leur meneur. Ce n'est pas la première fois qu'iel voit ce genre de mouvement péricliter, iel en a vu d'autre dans différentes mines. La seule question importante à ce moment là c'est de savoir combien de personne Giuse-P va entraîner dans sa chute.
Iel cherche Yas du regard. Celle-ci se tiens juste à côté de Giuse-P, passive, engloutie par son charisme. Iel essaye d'intercepter le regard de la voca, de l'inviter à venir s'asseoir avec ellui pendant que Giuse-P entraîne la conversation vers une des tables de la salle commune. Mais c'est vain. Toute personne prise dans l'aura de Giuse-P devient pour lui un accessoire avec lequel il peut exercer encore une plus grosse emprise, et il ne laisse personne partir s'il ne l'a pas décidé.
Et son audience est volontaire. Ils se sont tellement convaincus qu'ils méritent mieux que ça, ils veulent tellement participer à quelque chose qui a du sens, qu'ils sont prêt à accepter de se laisser guider par la première personne qui leur dira qu'il a un plan.
Et, même si iel a du mal à l'admettre, iel se laisse aussi avoir par son charisme. Iel n'est pas d'accord avec lui, mais iel n'a pas vraiment d'autre endroit où aller. Octane a disparu de son radar, Xicha n'a pas de temps pour elle, Coelia est… Coelia. Iel n'a pas vraiment grand monde pour lea soutenir, pour lui parler.
Alors iel accepte les excès de grandeur de Giuse-P. Iel accepte que Yas fasse des choix, même s'ils ne sont pas bon, et qu'elle n'a pas besoin de son aide. Et iel avale une hydrazine et une partie de sa fierté. Au moins, ici, on lui fout la paix.