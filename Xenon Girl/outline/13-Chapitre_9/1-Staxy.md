title:          Staxy
ID:             388
type:           md
summaryFull:    Staxy essaye d'embrasser Coda qui recule, interloquée et sous cara-1k, et part s'isoler pour se concentrer sur le boulot.
POV:            14
notes:          
                {C:14:Staxy}
                {P:6:Coda et Staxy} Pinch
                {C:1:Coda}
                {W:8:Lollipop Chainsaw}
                {W:128:gauffres}
                {W:129:système fédératif}
                {W:97:volontaires de la modération}
                {W:126:collectifs d'hébergement}
                {W:64:bain collectif}
                {W:66:moniteur}
                {C:0:Simena Von Guyen the Third}
                {W:74:Zone de transfert}
                {W:130:agent de recherche}
                {W:3:construct artificiel de réminiscence autosuggestive}
label:          4
status:         5
compile:        2
setGoal:        1500
charCount:      7390


Satxy finit par reprendre le contrôle, et laisse le fou-rire s'éloigner d'elles. La tension entre elle et Coda se dissipe, cette dernière souriant encore, quelque chose que Staxy n'avait pas vu depuis qu'elles se sont mise à bosser sur ce dossier. Elle préférerait que Coda réponde un peu plus à son flirt, mais celle-ci lutte sous l'influence de plusieurs constructs de personnalité, et ce n'est clairement pas la partie de Coda que préfère Staxy.
En attendant, elles n'avancent plus, leurs verres sont vides, et la ronde rouge vient juste de commencer.
"Bon, j'en peux plus on a bien avancé, mais là, il faut que je fasse quelque chose d'autre." dit Staxy, se levant et s'étirant, essayant de chasser la lassitude dans son corps. "Et en plus, ce soir, les vocas font une soirée au Lollipop, il faut qu'on vide les lieux, avant d'être prise d'assaut par une horde d'adolescents et d'adolescentes."
Une de ces soirées organisées par les collectifs vocas, généralement les plus âgés, pour passer du temps ensemble, se rencontrer, se faire des amis ou des amants l'espace d'une soirée, de quelques mois ou plus. C'est dans une de ces soirées qu'elle a rencontré une partie des Cuddle the Urshins, et qu'ielles ont commencés à écrire ensemble.
"D'accord," répond Coda, fermant ses entoptiques, et se déconnectant des archives hors ligne de Staxy. "Tu, as envie de faire un truc précis ? "
*Oui, plusieurs, et pour lesquels tu es beaucoup, beaucoup moins habillée,* se dit Staxy, avant de prendre Coda par la main, et de sortir du box.
"Il faut que je mange quelque chose, et il y a justement un stand fusion qu'il faut que je teste," répond Staxy, pensant aux plats d'inspiration philipino-martiens servit dans le bazar du bras trois, pas si loin du Lollipop Chainsaw. "Mais d'abord, il fuat que je me décrasse un peu, donc sauna. Tu viens ?" Staxy mène Coda à travers les vocas qui commencent à se rassembler dans le Lollipop Chainsaw, de la japlastique-pop relativement classique, servant de bruit de fond aux discussions.
Le sauna, même s'il la nettoie, les distancient l'une de l'autre, recréant une tension entre elles. La vapeur étouffe leurs voix, rend coûteuse les conversations et Coda se replie sur elle-même, laissant Staxy seule. Elle essaye de relancer une conversation, mais à chaque foi sa voix, écrasée par la chaleur et la vapeur, reste coincée dans sa gorge.
Après s'être séchées et rhabillées, elles marchent côte à côte, en silence, laissant la fatigue prendre le relais. Elles se promènent un peu, profitant des jardins au centre du boulevard radial en se dirigeant vers la bazar du bras trois, et vers le stand auquel elles se rendent.
Elles récupèrent leurs plats et s'installent sur la plate bande herbacée qui sépare la zone de connexion entre le bras, qui soutient l'anneau et le bazar qui s'y est développé, de l'architecture plus linéaire des sections toriques, au centre duquel s'étend le boulevard radial.
"Dis moi, demande Staxy une fois rassasiée, ça s'est passé comment ton entretien avec Simena ?" Staxy s'est allongée, la tête posée sur les cuisses de Coda, profitant de la fraîcheur des plantes rases.
"Je sais pas… Très conflictuel, ça m'a donné une étrange sensation…"
"En même temps… Qu'iel soit en colère, c'est normal non ? Surtout s'iel n'a fait que se défendre."
"Je dis pas le contraire. Mais reste que ça n'a pas été agréable. Je vais essayer de reprendre contact, pour voir s'iel est intéressée par un suivi. J'ai surtout l'impression que c'est la solitude qui lui pèse le plus, je peux peut-être l'aider à ce niveau là."
Elles restent allongée dans l'herbe, à profiter du calme relatif de l'espace physique par rapport aux tempêtes sociales et aux prises de positions tranchées qui déchirent leurs espaces sociaux.
"Tu entends ?" demande Coda soudainement.
"Quoi ?" répond Staxy, tendant l'oreille, cherchant ce qui a attiré l'attention de Coda.
"Le silence, justement. Il devrait y avoir du monde ici, une bande son, quelque chose… Non ?"
Il devrait en effet y avoir du monde. Enfin, un peu plus de monde. Le bazar était effectivement calme, et elles ont essentiellement croisés des vocas.
"C'est juste un temps mort," répond Staxy. Ces temps morts arrivent de temps en temps. Pas de volontaires pour animer l'espace, trop de choses ailleurs, de la fatigue générale. Ce n'est pas un phénomène courant, mais cela se produit quand même de temps en temps.
"Non," répond Coda, se redressant et signant de sa main les commandes de démarrage d'une entoptique. "C'est autre chose. Des instances entières de réseau sociaux coupent leur liens d'abonnement mutuels. Regarde," dit-elle, faisant glisser vers Staxy un lien, "Des collectifs d'administrateurices de ces instances publient des communiqués pour annoncer le repli de ces instances vers leurs membres. Et tiens, regarde, le Naked Brocoli mets en place un système de karma, soit-disant pour limiter les troll…"
Staxy se redresse à son tour, et parcoure les liens que lui transmets Coda. C'est plus qu'un simple temps mort en effet. Son flux social est particulièrement calme. Les centaines de notifications journalière qu'elle reçoit habituellement sont réduites à moins de vingt.
Et le retour à ces méthodes archaïques de score, de mesure de popularité pour déterminer qui peut publier ou utiliser les infrastructures de communication est inquiétante. Ce sont des mécanismes de repli culturel, de soumission à la volonté des plus anciens, de celleux qui parlent le plus, et qui accumulent ainsi le pouvoir.
"Il va falloir qu'on creuse ça aussi," finit par dire Staxy, après avoir épluché une dizaine de communiqué de rupture. Ajoutant encore à la liste des problèmes associés au mouvement, à ces conspirationnistes. Ou peut-être que ce n'est pas lié, mais il va falloir qu'elles creusent ça. Et qu'elles trouvent aussi une sortie de crise, un diagnostique ne sera jamais suffisant.
Mais elle sait qu'elle peut le faire. Qu'elles peuvent le faire. Toutes les deux. Même si Coda est dans un drôle d'état émotionnel, et que Staxy s'inquiète un peu de son recours aux CARA-1K. Coda laisse échapper un bâillement. Ça fait plus de vingt heures qu'elles sont éveillées, la journée s'est bien trop étirée, il va falloir qu'elles aillent dormir.
Staxy se relève rapidement, et aide Coda à se mettre sur ses jambes. La tirant vers elle, la prenant presque dans ses bras.
"Avant qu'on ne se sépare, dit Staxy, il y a quelque chose que je veux faire depuis deux jours."
Staxy hésite encore. Elle sent ses joues virer au rouge sombre sous l'afflux sanguin, ses mains tenant toujours celles de Coda. Peut-être que c'est une erreur, mais Staxy préfère clarifier les choses, surtout si elle doit leur faire confiance.
"Tu penses à quoi ?" demande Coda, cherchant les yeux de Staxy du regard.
"À ça…" murmure cette dernière avant de poser ses lèvres sur celles de Coda. Juste un instant, qui s'étire, elles restent enlacée l'une à l'autre, lèvres contre lèvres, leurs langues se cherchant l'une l'autre, puis, soudainement, Coda recule d'un coup, mettant fin à leur lien.
"Non partenaire. C'est contraires aux régulations," dit-elle, de l'intonation que Staxy reconnaît comme étant celle de la CARA-1K, celle qu'elle n'aime vraiment pas, qui n'est obsédé que par l'enquête.
Coda tourne les talons et détale avant que Staxy n'ait pu dire quoi que ce soit, la laissant seule, au milieu d'un parterre de trèfle.
*Au moins t'es fixée maintenant, ma grande.*