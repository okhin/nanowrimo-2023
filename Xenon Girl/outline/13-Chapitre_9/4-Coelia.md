title:          Coelia
ID:             2329
type:           md
summaryFull:    Elle obtiens une interview de Mark V
POV:            2
notes:          {C:5:Mark-V}
                {C:2:Coelia}{W:5:Xenon Girl}{W:7:Sc.nd.l}{W:10:Consortium}{W:132:Things-Speak}{W:105:le mouvement}{W:95:espaces intersticiel}{W:133:nootropes}{W:96::-}{C:16:Sanchez}
label:          4
status:         5
compile:        2
setGoal:        1500
charCount:      10406


Coelia s'est faites un peu plus discrète. Ryu a été clair à ce sujet, il accepte de la voir, mais uniquement en off. Pas d'enregistrement, pas de choses de ce genre. Étrangement, le fait qu'elle soit une idol, une personnalité remarquable n'est pas ce qui le gène. Elle a donc choisit le Xenon Girl comme lieu de rendez-vous, plus calme que le Lollipop Chainsaw, et elle ne risque pas d'y croiser Staxy. Moins officiel que le bauhaus aussi.
Elle l'attend donc, assise à une table, dans le lieu presque déserté, baigné de lumière cyanide et fuschias, une lente mélodie arythmique aux tonalité sucrée évoquant la pop lunaire des bar à idol de Luna. Ces bars sont plus des clubs de strip tease que les bars à karaoké de RAFT, du moins d'après les expériences synthétiques qui lui serve de référent émotionnel.
Elle a caché sa tunique blanche et bleu sous un hoodie trop grand, en néoprène mauve, doté d'une large capuche servant à dissimuler sa silhouette et son visage, histoire de ne pas trop affolé les afficionados du sc.nd.l et les autres fans qui pourraient passer dans le secteur.
Elle attends Ryu depuis plus de trente minutes. Il a montré de nombreuses hésitations, et elle a du insister fortement pour qu'il vienne lui parler. Il est prudent. Excessivement prudent. Mais Coelia est patiente, et elle l'attend en essayant de consulter son flux personnel, de garder une œil sur le discours autour de ces images de Simena. Elle note qu'il faut vraiment qu'elle reprenne contact avec elle.
Elle est sortie de ses rêverie par la silhouette élancée, habillée d'une combinaison spatiale très classique, dépourvue de patchs ou de flocage revendiquant l'appartenance à une clade sociale ou à une autre. En haut de la longue silhouette, un visage aux traits fins : une peau de jais tendu sur une musculature travaillée, des yeux mauves trahissant l'implantation de systèmes entoptique et des cheveux crépus très court et gris, l'établissant comme étant un des vieux de la vieille, un vétéran. Certains penseraient à une relique d'un passé dont il faudrait tourner la page, mais ces personnes sont actuellement en train de se déchirer dans les espaces interstitiels.
"Salut, lui dit Coelia. Contente que tu soit venu."
"Salut," répond-il d'une voix calme, presque dépassionnée. Il ne doit pas partager l'excitation de la plupart des personnes quand ielles sont confrontées à une idol. "Je ne suis pas certains de comprendre pourquoi, ou même de quoi tu veux me parler."
"Tu te rappelle de Things Speaks ?" demande-t-elle. Il hoche de la tête d'approbation, l'incitant à continuer. "À l'époque, sous le pseudo de Markus Vitus, tu as posté quelques messages qui m'interpellent."
Elle lui transmet le lien vers les messages. un lien issue de la copie d'archive qui est hébergé sur la peluche qui gigote sur la banquette à côté d'elle.
"Et j'ai l'impression que ces messages sont au cœur de ce mouvement qui s'en prend à l'infosphère en ce moment. Du coup, je voulais en savoir plus."
Ryu s'est reculé sur la banquette les bras croisés.
"Je n'ai rien à voir avec eux," dit-il, sa voix restant toujours aussi calme, aussi économe de son énergie.
"Je sait. Mais j'essaye de comprendre. Quelque chose dans ces messages semble avoir résonner bizarrement avec eux. Du coup, est-ce que tu peux m'en dire un peu plus ?"
Il relit les messages. les rides sur son front s'accentue à mesure de sa lecture, trahissant un effort de concentration, de remémoration de ce qu'il faisait il y a cinq ans, voire plus.
"Bon, mes souvenirs ne sont pas parfait. Je ne me rappelle plus du contexte précis, mais je bloquais sur une série de micro-code un peu récalcitrant. Des clefs de déchiffrement incomplète, des données binaires en guise de messages texte. Ce genre de chose. Je n'étais pas le seul à avoir des problèmes de ce type là, il y avait toute une nouvelle génération d'injecteur de Futech qu'on essayait de faire sauter."
"D'accord. Mais… Sur ces messages là, ceux qui sont repris dans les espaces interstitiels, ils ont une syntaxe différentes de simple message. On dirait presque de la poésie.
"Franchement, je ne me rappelle plus vraiment. J'étais blindés de nootropes à l'époque. Fix, Browse, ce genre de chose. C'est pratique pour trouver les motifs dans les choses, et rester éveiller une centaine d'heures d'affilée. Mais la mémoire à long terme déguste."
Il relit les messages que Coelia accentue.
"Je peux te répondre pour la syntaxe par contre. C'est du short-hand. Un pseudo-langage utilisé pour analyser les micro-codes. J'ai du perdre le contrôle de mes centres linguistiques et j'avais probablement besoin d'une pause. Mais du coup, voilà. C'est pas des hallucinations apocalyptique, juste un dump d'un codeur un peu fatigué par les mesures anti-copie de Futech."
Ryu explique les choses de manières fluides, essayant de se mettre à la hauteur de son interlocutrice, pour qu'elle puisse comprendre ce dont il parle.
Il doit passer du temps à expliquer les choses, probablement quelqu'un qui participent à former les vocas aux joies de l'analyse binaire et du contournement des mesures de protections propriétaires.
Ou juste quelqu'un qui apprécie pouvoir parler de ce qu'il fait. Coelia sent qu'elle pourrait encore y être demain, à ne lui poser que de courtes questions, le poussant à parler encore et encore.
"Et donc, pour :-, tu peux m'en dire plus ?"
"De ce que je lis, j'étais frustré par cette succession de caractères, qui semblaient apparaître un peu partout. Trop souvent pour être juste quelque chose d'aléatoire. Mais je n'ai aucune idée de ce que c'était, regarde."
Il pointe un fil de discussion assez bref, se terminant abruptement, et clôt. Un des derniers à parler de :- dans son archive de Things Speak.
"Tu sais m'expliquer ce que vois," demande Coelia.
"J'ai résolu le problème. Mais je n'ai jamais mis la solution ou quel était le problème au final. Je suis passé à autre chose. Puis j'ai commencé à bosser avec les vocas, et j'ai un peu laissé ça derrière moi."
"Bon, donc est d'accord… Selon toi, il n'y a pas d'IA cachée quelque part dans l'infrastructure de RAFT ?"
Ryu laisse échapper un rire étrangement sonore, contrastant avec le faible niveau sonore de leur discussion. Il lui faut quelques instants pour répondre.
"Non. Aucune chance. Je peux même t'expliquer pourquoi, c'est assez simple."
Il démarre un rapide exposé de théorie de réseau. Rappelant le fait que tout ce qui ressemble à l'intelligence d'un réseau est, en fait, situé en bordure. Les équipements sur la route, l'infrastructure du réseau, ne fait que transporter les données. Tout ce qui donne l'impression que le réseau est intelligent est, en fait, situé en bordure de celui-ci. La capacité de contourner un obstacle, de trouver une route entre deux points inconnus l'un de l'autre, tout ça est rendu possible par l'absence de point central.
Ce qui implique aussi, d'après lui qu'il n'y a pas vraiment de possibilité d'avoir au cœur du réseau, suffisamment de puissance de calcul pour y planquer quoi que ce soit. Quelqu'un pourrait avoir une IA fonctionnelle connectée quelque part, mais une IA qui serait capable de prendre le contrôle d'une infrastructure tant décentralisé lui semble être une impossibilité.
"C'est comme le Flux", dit-il, estimant que l'analogie est évidente pour Coelia.
Elle compile leur interview au fil de l'eau, notant les références à recouper, essayant de ne pas oublier de choses qui semblent être des détails, mais qui pourraient se révéler être plus tard important. Le Kang s'agite sur la banquette alors que les données vont et viennent entre lui et leurs terminaux.
"Je pense que j'y vois un peu plus clair. Un dernier truc. Ton sentiment là-dessus. Sur ce groupe qui prend tant d'importance, qui est persuadé qu'il y a une cabale, qui contrôlerait en sous-main la station."
"C'est pas ton boulot d'idol ça ?" répond-il.
"Peut-être. J'aimerais ton point de vue cependant."
"D'accord," il réfléchit quelques instants, rassemblant des pensées en propos cohérent prêt à être énoncés. "On est dans les villes spéculations et les analyses à l'emporte pièce là. Mais je pense que ça a à voir avec l'effet de réseau appliqué a l'infrastructure. Au Flux."
"Comment ça ?"
"Ben, vu de l'extérieur, et si tu n'es pas, par exemple, branché sur quelques canaux d'informations technique, la station fonctionne toute seule. Depuis vingt cinq ans, sans système de contrôle, sans rien ou presque. Évidemment, il y a, de temps, une équipe qui se pointe chez toi pour régler, je sais pas… le débit d'une pompe à air, ou que sait-je. Ils sont sympas, et tout, mais tu ne les a pas appelé. Tu n'as pas posté de message de demande d'aide.
"Et ça peut ressembler, de ce point de vu de la bordure du réseau, à une forme d'intelligence qui est au courant de tout et est capable de prendre des décisions. Ça implique effectivement de pas avoir été formé à ça, ou que quelque chose se soit passé pour que tu penses qu'il y ait autre chose que juste la confiance et la bonne volonté qui soit à l'œuvre."
"Quelque chose du genre, je sais pas, quelqu'un qui pousserait cette idée de cabale pour un but quelconque ?"
"Par exemple."
Coelia n'est pas complètement satisfaite de cette explication. Quelque chose ne lui plaît pas. L'idée que des personnes puissent avoir oublié ou ignorer le fonctionnement d'un élément aussi essentiel lui paraît peu probable.
"J'imagine, reprend Ryu, qu'une personne utilisant les même techniques et biais que toi, en ciblant un public spécifique, en utilisant un complexe mémétique minutieusement choisit, pourrais y parvenir. Une sorte d'idol quoi."
Coelia essaye de pondérer tout ça, mais elle est plus un systême émotionnel qu'une capacité d'analyse. Peut-être qu'il pourrait y avoir une sorte d'idol qui aurait démarré tout ça, dans le but de se créer une audience. Mais elle n'aime pas vraiment l'idée. Elle n'aime même pas du tout cette idée.
Il faut qu'elle laisse ces dées faire leur chemin dans l'esprit de Coda. Et, aussi probablement, de Sanchez. même si elle aime encore moins cette idée.
Elle laisse partir Ryu, qui lui demande encore une fois de tenir son nom en dehors de tout ça, et elle laisse Coda reprendre lentement le contrôle. Elle sent qu'elles ont une bonne cohérence mnémorithmique. Et ça faisait longtemps que ça ne leur était pas arrivé.
À mesure qu'elle s'estompe et se replie dans la bille de plastique abritant ses circuits mnémorithmique, une vague de dopamine inonde les récepteurs synaptique de Coda.