title:          Sanchez
ID:             2179
type:           md
summaryFull:    Retrouver la trace de Mark-V s'avère difficile, et, avec la fermeture de plus en plus de nœuds du réseau, elles semblent se trouver dans une impasse.
POV:            16
notes:          {P:7:Enquête de Coda (et Staxy) sur la conspiration} Pinch
                {C:1:Coda-6}
                {C:16:Sanchez}
                {C:2:Coelia}
                {C:5:Mark-V}
                {W:105:le mouvement}
                {W:102:vitres polarisées}
                {W:85:hotline}
                {W:131:infosphère}
                {W:132:Things-Speak}
                {W:104:résonance mnémonique}
                {W:101:chambre blanche}
label:          4
status:         5
compile:        2
setGoal:        1500
charCount:      9533


Sanchez ressent un sentiment d'échec. Ce qui n'est pas normal pour un programme mnémorithique d'investigation. Il n'est pas censé avoir d'émotions propre, il doit normalement faire avec celles de son hôte. Mais depuis qu'il a commencé cette enquête, les choses étranges s'accumulent.
D'abord, il n'a jamais réussi à entrer en contact avec sa responsable à Conquistadores. Mais ça ne l'as pas inquiété outre mesure, ça arrive les infiltrations sans pouvoir contacter la base. Il a été codé pour ça, pour rester discret, se fondre dans l'identité de son hôte, et prendre petit à petit le contrôle de ses processus de pensée, infiltrant ses ressources cognitives pour les reprogrammer à son avantage.
Ce qui l'amène au second problème, celui qui l'inquiètes bien plus, celui qui le ferait paniquer s'il n'était pas un simple programme. Son hôte est déjà habité par un autre. Ou un autre, s'il se base sur son ressenti. Celle-ci empêche et limite son infiltration, le force à lutter pour accéder aux fonctions conscientes du substrat sur lequel il s'exécute.
Et puis il y a Staxy, sa partenaire. Il sait qu'il ne devrait pas attendre de professionnalisme de la part d'une amateure, d'une de ces autonomistes qui cherchent à se libérer du consortium. Ça fait partie de son boulot de logiciel d'infiltration. Mais justement, il aurait du répondre positivement à ses avances. Une relation sentimentale permet généralement d'accéder à de plus haut niveaux d'influence.
Mais quelque chose en lui l'a fait paniqué quand elle a embrassé son hôte. Il s'est forcé un passage vers la conscience, sans passer par la chambre blanche et à cherché à … se protéger ? Mais se protéger de quoi ?
*Ce n'est pas toi qui l'as protégée, c'est moi.* dit l'agaçante voix de l'autre construct.
Peu importe ce qu'il s'est passé, il s'est planté. Ce qui le perturbe le plus, au final, c'est qu'il ressent de l'affection pour Staxy. Encore quelque chose qui n;est pas censé se passer. Il a de plus en plus l'impression que c'est lui qui se fait infecter par l'autre parasite de son hôte, qu'il développe ses propres émotions.
La seule chose qu'il peut faire, c'est reprendre le contrôle de leur train de pensée, l'orienter sur l'enquête, et manipuler les intuitions de son hôte depuis sa position. Clore le dossier et l'enquête et pouvoir passer à la suite. Quelle que soit la suite.
Une entoptique habille rapidement les murs triste d'un compartiment anonyme. Quelque chose dans ses tripes l'a amené ici. L'autre construct l'a amenée ici. Ce n'est pas le bauhaus, c'est juste un petit compartiment que Coda se garde et occupe de temps en temps, pour être seule et faire le point. Mais pour un espace privé, il n'y a vraiment pas de traces d'occupations de cet espace par une personne. Pas de bibelots sur les étagères, pas d'artefacts de réalité augmenté, pas d'imagerie projetée sur une pseudo fenêtre, aps de musique.
Sur les murs se dessinent une carte bien ordonnée de cette conspiration. Quelques noms sont mis en évidence, mais Sanchez sait qu'il faut aller à la source de tout ça. Remonter la trace de ces messages de MV qui structurent les discours des espaces interstitiels.
Il a commencé, avec Staxy, a reconstituer les entêtes binaires associés aux messages. Certains sont simple à retrouver — une date après tout n'est qu'un entier, et les 27 premiers bits sont tous identiques et nuls. C'est quelque chose de facile à extraire.
D'autres, à l'inverse, sont des champs libres. Des suites d'octets formants des combinaisons hexadécimales devant être combiné d'une manière précise pour déterminer les caractères associés et former les mots étranges utilisés pour étiqueter les messages.
Et, quelque part entre les deux, les données vraiment importantes. Les données binaires, stocké dans des structures finies, contenant des choses tels que l'adresse matérielle du périphérique utilisé pour publier le message, la route entre le message original et l'endroit ou celui-ci a été republié, un identifiant binaire de l'auteur, ce genre de choses.
Mais, pour l'instant, il s'agît d'une salade binaire d'en-tête non discriminés. La bonne nouvelle, c'est que ces en-têtes semblent partager le même format entre tous les messages de MV. Ils viennent donc tous de la même plateforme. 
Son discriminateur binaire, qu'il alimente avec les entêtes des messages, finit par lui cracher quelques détails. Des adresses intelligibles, des numéros d'utilisateurs sur deux cent cinquante six bits. Quelque chose d'utilisable pour parcourir une base de données, mais, pour cela, il lui faudrait avoir une copie de celle-ci.
Heureusement pour lui, les autonomistes sont des personnes qui ont tendances à publier tout type de données. Et plus celles-ci sont critiques, plus ils ont tendance à les diffuser largement. Soit disant pour garantir que personne ne puisse pendre le contrôle des éléments critiques. Sauf que les fermetures au public des instances de ces derniers temps viennent se mettre en travers de leurs belles théories.
Sanchez récupère donc sur la hotline un annuaire des instances sociales. Chacune d'entre elle dispose d'un identifiant unique. Il lui suffit juste de parcourir les milliers de noms de communauté disparues ou encore active, et de voir laquelle correspond à l'identifiant de cette instance.
Quelques secondes plus tard, principalement passée à attendre la récupération des données, et il trouve une instance candidate. Things Speak. D'après sa fiche sur la hotline, il s'agît du serveur d'une communauté "dédiée à faire parler les objets et à les libérer des absurdités de la propriété privée." Une bande de hacker donc. L'instance est marquée comme inactive depuis trois ans, et aucune archive n'est maintenu en ligne.
La page "Comment retrouver un compte ?" sur la hotline contient des dizaines de réponses, mais aucune ne lui permettant d'établir un lien vers un compte courant depuis les données dont il dispose.
Il va falloir qu'il accède à une archive de cette base de donnée. Et la hotline ne donne pas vraiment de procédure pour ça. Les génies qui ne jurent que par la théorie des flux pensent que faire confiance aux gens pour sauvegarder ce qui est important est suffisant.
Au moins, dans les puits de gravité, il y a en général un nombre réduits d'acteurs qui agissent plus ou moins de concert pour fournir des archives aux services de police. Il sait que le but, ici, est de s'assurer que la police ne puisse pas faire son travail. Et c'est efficace. Il va falloir se débrouiller pour demander à quelqu'un qui a accès à cette archive.
À part, évidemment, le mouvement. Ils ont accès à cette archive. Ou au moins à une copie de partielle de celle-ci. Mais il ne voit pas bien comment il pourrait faire pour aller leur demander.
*Je sais à qui demander. Je peux te récupérer ces données,* lui insinue l'autre.
Si elle sait, il sait aussi. Ils ont la même mémoire après tout. Mais elle le confine avec un barrage d'émotion et il risquerait d'effondrer sa cohérence et de ne redevenir qu'un ensemble inerte d'expériences non acquise. Il va falloir faire autrement, et maintenir le contrôle.
*Ce serait plus simple de me laisser faire.*
Plus simple, probablement. Mais il n'aurait aucune garantie de revenir. Et il sait à qui demander. S'il a compris comment fonctionnent les choses dans le coin, sa partenaire, étant une sorte de célébrité locale, pourra certainement lui fournir un contact. Reste que la façon dont ils se sont séparés le fait hésiter. Staxy pourrait l'envoyer balader. Mais elle pourrait également vouloir résoudre cette enquête et prendre sur elle et accepter de continuer à bosser.
*Peut-être, mais tu risques de démolir encore plus Coda.*
*Tu le sais aussi bien que moi, il n'y a pas grand chose à démolir. Si elle a besoin de toi pour savoir quoi ressentir, sa personnalité est déjà foutue. Et je sais ce que je fais,* répond-il, adressant pour la première fois depuis l'incident cette voix dans leur tête commune.
*Oui, mais on risque de la perdre. De perdre Staxy.*
L'enquête doit avancer, les fauteurs de troubles doivent être contenus, même s'il ne peut pas faire de rapport à Conquistadores. Perdre un partenaire, ou y laisser des plumes, ça fait partie du métier
*Mais moi, je n'ai pas envie de la perdre,* dit l'autre voix. Il sent son espace logique se réduire, les émotions prenant le pas sur la raison. Sa cohérence chute, son temps de conscience se réduit à vue d'œil.
Il commence à rédiger un message à destination de Staxy. Il n'as pas le choix, il doit agir vite et ne pas prêter attention à ces émotions.
"Salut,
"Essaye de voir si tu peux récupérer une image de la base de donnée de Things-Speaks. C'est de là que viennent les messages de MV, et ça pourrait nous aider pour enfin avoir le contexte."
*Excuses-toi au moins.*
Il hésite. Soupire intérieurement, et ajoute à la fin de son message.
"Et puisqu'elle me le demande, elle voudrait te parler. À propos de l'autre chose."
*Contente ?*
*Pas vraiment. Tu veux vraiment pas me laisser faire ?* répond-elle.
Il envoie le message. Et une vague de rage, de tristesse et d'abattement se précipite sur ses parois cognitives, érode ses scripts mnémorithmiques, et, inonde ses centres de réflexion. Il essaye de lutter avec sa rationalité, de maintenir debout les cloisons qui préservent son identité, de se concentrer sur le fait que s'il ne fait rien, les méchants finiront par gagner et qu'il ne peut pas gagner.
Mais sa détermination ne suffit pas à résister à la dépression qui balaye tout, qui arrache ses dernières barrières et le force à se replier dans l'inexistence de la salle blanche.