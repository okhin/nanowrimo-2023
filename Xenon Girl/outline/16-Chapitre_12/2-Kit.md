title:          Kit
ID:             238
type:           md
summaryFull:    Coda questionne sa carrière d’Idol, qui l’intéresse moins, et se demande si elle n’essayerait pas d’être journaliste
                Kat et Coda se croisent au Xenon Girl. Un lieu un peu en perte de vitesse, branché sur l’approche neo-néon, mais servant de base syndicale pour les icones.
                
POV:            13
notes:          {C:1:Coda-6}
                {C:13:Kit}
                {W:5:Xenon Girl}
                {C:6:Sarah}
                {W:63:Rythme rouge blanc}
                {W:137:néon-chic}
                {C:3:Gab}
                {W:15:V.S.T.L}
                {W:4:bauhaus}
                {W:14:Idols}
                {C:2:Coelia}
                {W:23:Cuddle the Urchins}
                {W:24:Syndicat}
                {W:65:scripts}
                {C:25:Moon}
                {W:74:Zone de transfert}
                {W:27:Contrôle}
                {P:10:Kit}
label:          4
status:         5
compile:        2
setGoal:        1500
charCount:      12283


Encore une ronde nocturne calme au Xenon Girl. Sarah et Audrey sont passée voir Kit, mais elles sont rentrées dormir depuis un bail, le laissant couvrir le service du club, du moins jusqu'à l'un des volontaires ne vienne le relever.
Il est parti du Naked Brocoli après le vote. Il trouve cette idée stupide, de voter pour l'exclusion d'une personne. Comme si il n'était pas possible de trouver un consensus autrement. Surtout pour ce genre de décision. Personne n'a laissé une chance à Simena. Lui non plus, s'il veut vraiment être honnête. Il aurais pu… Probablement faire quelque chose. Adresser l'hostilité plus tôt au lieu de demander à Simena de prendre sur elle ? Essayer d'isoler les quelques patrons du lieu qui ont déclenché ce vote ? Quelque chose en tout cas. Il aurais du essayer.
Au lieu de ça, il est parti, et il est venu donner un coup de main au Xenon Girl, un coyote-space. Un lieux qui célèbre la culture idol, centrée autour d'elles et de leurs performance. Ou, du moins, célébrait la culture idol. L'esthétique néon-chic du lieu, baignées de lumière fushia et cyanoïde, rappelle à Kit l'époque où Gab et lui venaient danser plusieurs rondes d'affilées, défoncé aux mnémorithme, bougeant sur les chorégraphies détaillées des V.S.T.L, des Cuddle the Urshins ou d'autres stars aux célébrités aussi fugaces qu'intense qui habitaient leurs nuits.
Le Xenon Girl était un des lieux où les vocas et les autres pouvaient se mélanger, partageant des fragments de culture et de socianilisation. Un de ces lieux où leurs brassards ne comptaient plus forcément autant que leur capacité à bouger. Un de ces lieux oú les volontaires gardaient un œil pro-actif sur les comportements de dragues des unes et des autres, pour s'assurer que tout le monde puisse s'amuser, sans se soucier des comportements limite des autres. Un de ces lieux où les gogo danseurs et gogo danseuses continuaient de perdurer, alors qu'ils avaient alors disparu de la surface des comptoirs de la plupart des autres lieux.
Mais maintenant, le lieu est passé de mode. Les idols se sont de plus en plus institutionnalisées, et ont délaissées leurs soirées de lâcher prise pour se ranger dans le bauhaus du Syndicat, ou autres établissement coopératifs du genre. Les envies des habituées ont aussi changé et s'orientent vers des centres communautaires moins spécialisés. Et le Xenon Girl est en perte de vitesse. Les dernières crises de la station n'aide probablement pas non plus, se dit Kit, alors qu'il glisse sur ses rollers entre les tables vides, servant les quelques personnes qui sont venues.
Un groupe s'est installé sur une des grandes tables, portant leurs blousons floqués du logo de leur équipe de poursuite. Une course en équipe dont le but est de rattraper l'équipe adverse, et qui se court sur la dorsale de l'anneau. Elles ont encore leurs brassard de vocas, et elles espéraient peut-être trouver plus de monde, une ambiance plus festive.
Il essaye de ne pas trop écouter leurs conversations, mais il n'a vraiment pas grand chose d'autre à faire, et son esprit gamberge. Se concentrer sur les discussions des autres lui permet de ne pas trop réfléchir à sa vie. Alors il écoute les récits d'exploits de DDR, de soirées passées à essayer de battre un score ou du fait que le lieu où elles jouent habituellement a beaucoup changé.
Il refait un tour des tables et des boxes, séparés par des cloisons écrans, sur lesquels sont affichés les poses des idols qui ont fait la renommée du lieu. Il repasse devant cette personne qui dorlote un hydrazine. Le même que celui qu'il lui a machinalement servi il y a deux heures. Son visage est familier, mais il n'arrive pas à remettre le doigt sur le contexte dans lequel il connaît cette personne. Son oracle reste muet, indiquant que cette personne cherche à ne pas être reconnue, identifiée. Un peu comme si elle portait un brassard. Une façon assez radicale de vouloir être laissée seule.
Puis soudain, alors que ses synapses s'activent sous les couches de reste de mnémorithme qui, parfois, lui file la migraine, ses routines neurales de reconnaissance de visage s'active. C'est Coelia. Enfin, son interprète, dépourvue des atours de l'idol. Vêtue d'une veste à capuche large, ses mèches de cheveux bleues glissant entre les lunettes noires et la capuche, elle fixe son verre dont le contenu est devenu tiède. Sous l'épaisse couche de coton tissé, portant les couleurs de Cuddle the Urshin, Kit sent une tension dans les épaules de l'interprète. Son langage corporel est celui de quelqu'un qui est désespérément en recherche de quelqu'un à qui parler. Elle veut garder les fans à l'écart, mais elle se retrouve de fait seule.
"Salut, je suis Kit," lui dit-il, glissant vers elle, se tenant à l'opposé du box, prenant la posture droite et confortable du serveur venu prendre sa commande, "je suis le volontaire du Xenon Girl pour le moment. Est-ce que je peux faire quelque chose pour toi ?"
"Je sais pas…" répond-elle.
"Tu veux parler ?" demande-t-il, avant de commencer à s'installer sur la banquette en mousse à mémoire de forme qui ceint la table métallique sur laquelle les idols venaient parfois danser.
"Tu n'as pas des gens à servir ?" demande-t-elle.
Scrutant la salle d'un geste de la tête exagéré, Kit répond simplement en haussant les épaules. La salle est essentiellement vide.
"Tu veux parler de quoi ?" reprend-elle.
"Je sais pas. De ce qui ne va pas dans ta vie pour commencer ?"
"Tu… Tu ne sais vraiment pas qui je suis ?" demande-t-elle.
"Tu es l'interprète de Coelia. Probablement la personne qui a organisé cette dernière session de photos, manifestement en dehors des accords syndicaux. Celle qui a exposé une conspiration avec sa rivale et qui, depuis, est resté silencieuse. Mais ça ne me dit pas qui tu es," répond Kit.
Elle se redresse sur la banquette. Posant ses mains sous la table, se distançant quelque peu de Kit. Elle le jauge, essayant de déterminer si c'est un fan qui veut juste se rapprocher d'elle, ou si sa proposition de l'écouter est authentique.
"Coda, dit-elle. Je suis Coda. Mais ça ne me dit pas vraiment qui je suis. Et c'est bien le problème en ce moment." Elle attrape son verre et avale une gorgée d'hydrazine bleue et sucrée.
"Et je ne peux même plus être Coelia en ce moment. Le Syndicat me laisse tomber et refuse de me couvrir."
Kit attend la suite. Il a envie de la tenir dans ses bras, de lui poser des centaines de questions, mais il les laisse tourner dans sa tête pour le moment.
"Ils n'ont probablement pas complètement tord, j'aurais peut-être du … je sais pas. Faire différemment? Mais je m'ennuie, j'en ai marre de ne rien ressentir qui ne vienne de moi, de devoir me poser tout le temps la question de comment vont les autres, de comment est-ce que le public va réagir, de devoir suivre les scripts tout le temps à moins d'être attaquée par une boucle émotionnelle de dépression quand je sort trop des scripts.
"À moins que ce ne soit juste moi au final qui soit déprimée, et que Coelia est la chose qui m'as permis de tenir jusque là. Tu savais que les CARA ont d'abord été développé comme outil thérapeutique ?"
"Je pensais que ça avait été mis au point pour former des travailleurs spécialisés à bas coût ?"
"C'est venu après. Mais les premiers mnémorithmes ont été développé pour essayer de soigner les cas de dépressions résistants aux autres thérapies. Puis on s'est aperçu qu'on pouvait implanter des bases de compétences avec les mnémorithmes, et que ces compétences étaient volatiles. Et bon, après, on a réussi à en faire des drogues. Mais ça a commencé comme ça."
"Et donc ?"
"Ben voilà, je doit être dépressive. Comme beaucoup de monde ici, non ? Je veux dire, vu la quantité de monde qui prend des mnémorithmes sur une base régulière, on doit pouvoir supposer qu'une part importante de personnes souffrent de dépression chronique ?"
"Je voulais dire, et donc, pour toi, ça veut dire quoi tout ça. Ok, tu es dépressive, mais s'il suffit que tu prenne des mnémorithme pour que tu aille bien… qu'est ce que ça implique pour toi ?"
Elle marque une pause.
"Que je n'ai jamais vraiment ressenti mes propres émotions. Que je ne sais pas si c'est moi qui ai le béguin pour," elle suspend sa phrase avant de la terminé, pesant le pour et le contre de continuer son histoire.
"Pour ?" demande Kit.
"Staxy. Oui, de CTU. Je sais, c'est contre la bible des scripts, mais c'est comme ça. Et j'arrive pas à savoir si c'est moi, ou si ce sont les mnémorithmes qui parlent là."
"Et c'est important de savoir ?"
"Je… ne sais pas." Elle baisse les yeux, et s'enfonce dans la banquette, son sweat shirt la recouvrant de plus en plus, la faisant disparaître au yeux de Kit. Derrière ses yeux clos, il devine une lutte interne, similaire aux siennes. Essayer de savoir qui il est, ce qu'il veut, faire le tri entre les émotions synthétiques et les siennes, essayer d'exister en dehors du regard des autres. Il comprend les craintes de Sarah, de le perdre dans une crise d'identité permanente, de ne plus savoir si c'est avec lui qu'elle a une relation ou si c'est avec des scripts de personnalités.
"Et elle en pense quoi ?" Il relance la discussion plutôt que de se perdre dans ses propres pensées circulaires.
"Staxy ? Elle… Elle m'a embrassée. Et je suis partie. Enfin, Sanchez est parti. Moi e sais pas. J'ai abdiqué je pense."
Kit n'a aucune idée de qui est ce Sanchez qui débarque dans la conversation, mais ce n'est pas ce qu'il faut creuser. Il veut lui demander comment faire pour reprendre le flambeau, comment devenir Coelia, lui enlever le poids de ses épaules, mais ce n'est pas le moment. Pour le moment il faut lui changer les idées.
"Viens, il faut pas que tu reste là toute seule dans ton jus. On va danser," dit-il. Et, joignant le geste à la parole, il attrape le bras de l'interprète alors qu'il change la liste de lecture du juke-box du lieu. Quelques commandes mentales lui suffisent, et *Embrasser le vide* surgit des hauts-parleurs, disposés un peu partout dans le bar. Le dernier album d'Idalgo est un monument de gravel-rock, et c'est aussi quelque chose de musicalement très éloigné de la pop de Coelia, ce qui ne devrait pas donner à Coda l'impression de travailler.
Celle-ci résiste un peu au début, et se fait un peu prier, mais finit par se laisser poter par les rythmes énervés. Pas de scripts, pas de chorégraphie ou de show travaillé, juste Coda, Kit, et les poursuiveuses qui les ont rejointes. Pas de caméras ni d'enregistrement, juste l'énergie des corps se jetant les uns contre les autres, laissant leurs émotions se dissoudre dans une euphorie collective faite de sueur et d'un cocktail d'hormone synthétisé par leurs hypothalamus.
Et une fois la tension émotionnelle évacuée, une fois que le dioxyde de carbone accumulé dans leur muscle se transforme en acide, ielles se posentt dans le box et se remettent à parler. Mais pas de leur situation personnelles, ou de leurs problèmes d'identités. Ielles parlent des envies de Kit d'incarner une idol, de l'état du Xenon Girl qui, quand même, devrait probablement changer un peu sa ligne éditoriale, ou des difficultés de bosser pour un syndicat et de devoir vivre sa vie selon des scripts validés par un comité.
Ielles finissent par se séparer, Coda accusant le coup de la fatigue mais se sentant manifestement un peu mieux qu'à son arrivée au Xenon Girl. Kit se prend quelques minutes pour laisser son état d'excitation passer, et raccroches ensuite son tablier, laissant à Moon le soin de faire la continuité. Il est fatigué, mais il veut d'abord aller voir Sarah. Il prend donc la direction de l'axe central de la station, là où les membres de contrôle ont établi leur capitainerie, et d'où ils s'occupent d'aiguiller les esquifs dans le voisinage de Vesta.
Il faut qu'il parle de sa soirée à Sarah. Et il ne veut pas en parler en ligne. D'autant que contrôle est en tension en ce moment, ils subissent de plein fouet le ralentissement des communications qui se propage un peu partout dans la station, les mettant dans des conditions dégradés pour gérer les trajectoires de tout le monde et éviter les collisions. Des soucis de circulation de l'information qui créent des galères pour les pilotes des esquifs qui gravitent autour de la station.
Alors il se dit qu'il peut au moins essayer de remonter le moral de sa copine, qui ne va pas tarder à prendre son service.