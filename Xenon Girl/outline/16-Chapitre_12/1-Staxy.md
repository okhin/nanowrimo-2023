title:          Staxy
ID:             2478
type:           md
summaryFull:    La conspiration sur les écrits de Mark V et la protection de :- est exposée à tout le monde, et se dissout sous les conflits internes.
POV:            14
notes:          {P:7:Enquête de Coda (et Staxy) sur la conspiration} Resolution
                {C:14:Staxy}
                {C:11:Octane}
                {W:50:coursiers de maintenance}
                {W:87:ersatz de café}
                {C:2:Coelia}
                {W:105:le mouvement}
                {W:95:espaces intersticiel}
                {W:71:Bauer}
                {C:5:Mark-V}
                {W:96::-}
label:          4
status:         5
compile:        2
setGoal:        1500
charCount:      10448


Staxy est assise à une table assemblée à partir de reste de diverses pièces de vaisseaux spatiaux, au milieu de la cour cernée d'ateliers. Les bruits de découpes du métal, se succèdent aux impacts des marteaux cherchant à assembler deux éléments ensemble faisant fît des côtes d'ajustement trop précises.
Octane s'assied en face d'elle, sa combinaison de travail nouée à la taille, ses épaules scintillantes de sueur et de graisse mécanique sont cernée des sangles d'un harnais porteur, auquel était suspendu une découpeuse plasma quelques instants auparavant.
Staxy, elle, avec son blouson en plastique et sa nano-peau fluo détonne dans le décor. Elle range ses entoptiques et se concentre sur octane. Elle est venue pour lui parler, pour faire un suivi psychologique. La nomade de maintenance s'est retrouvée en première ligne face aux premières attaques du mouvement, et Staxy passe régulièrement en parler avec elle depuis.
"Alors, comment vont les choses de ton côté ?" demande l'idol.
"Ça va. On trouve des façons de travailler, de s'occuper." répond Octane, laissant les bruits d'atelier emplir l'espace sonore comme preuve que les choses se passent bien. "On trouve comment bosser sans les accès aux instances sociales. Il reste des angles morts, mais on va y arriver. J'ai confiance."
"D'accord. Et, en dehors de la technique, tu vas comment ?" demande Staxy, essayant de faire en sorte qu'Octane ne puisse s'échapper de la conversation qu'elle est venue pour avoir.
Cette dernière ne répond pas immédiatement. Elle fait tourner la tasse de céramique qui contiens un fond de café froid dans ses mains, comme pour attirer sa concentration hors de cet endroit.
Staxy attend, elle n'est pas pressée, et le silence va, inévitablement finir par être rompu.
"Ça va pas terrible," commence doucement Octane, fixant toujours le fon de café dans sa tasse. "Tant que je reste le nez dans le guidon, à m'occuper la tête et les mains, ça va. Mais si j'essaye de prendre un peu de recul, de regarder l'état de la station… Je panique."
Elle termine son café froid et reprend la tasse entre ses mains.
"J'ai l'impression qu'on a régressé. Que tout le monde se repose tellement sur les autres, sur nos acquis que personne n'était prêt pour un truc nouveau de ce type. Certaines essayent hein, je veux dire, toi et Coelia, vous avez essayé de secouer les choses avec votre enquête, avec ce papier qui explique bien que tout ce truc là, cette conspiration, n'est basé sur rien. Mais…"
Les pensées d'Octane restent en suspend.
"Mais ?" relance Staxy, posant sa main sur celle d'Octane pour l'accompagner, pour l'aider à dire ce qu'elle a besoin de dire.
"Mais j'ai toujours peur d'apprendre par la CiBi que l'une d'entre nous s'est faites agresser parce qu'on a voulu réparer un système, et que la confiance est rompue. J'aimerai pouvoir leur faire confiance, mais je ne peux pas vraiment. Cette confiance a été rompue.
"Alors on reste cramponnée à ce fil rouge de la CiBi. Audrey n'arrêtes pas de maintenir les canaux ouverts, ne prenant qu'à peine le temps de s'effondrer à côté de moi dans notre couchette, ou à côté de je ne sais lequel de ses amants ou amantes. On est en mode urgence, et on ne peut pas durer longtemps."
Les vannes sont ouvertes et les mots s'enchaînent, les pensées d'Octane essayant d'évacuer les sources de stress.
"Et puis on prend deux minutes pour essayer de voir s'il y a une chance que les choses s'améliorent prochainement. D'essayer de se dire que, peut-être il n'y a qu'un jour ou deux à tenir. Et, à ton avis, on vois quoi ?"
"Je sais pas, des gens qui s'engueulent ?"
"Si seulement. Ça on sait gérer, non ? Enfin, vous savez gérer. Non, on voit des gens qui se moquent des autres. Qui se prennent pour plus intelligents que les autres pour ne pas avoir cru dans ces histoires de cabales. On voit les instances qui arrêtent de se parler. On voit des modules qui veulent ne plus dépendre d'autres nodules pour leurs adjonctions d'air. Et toute le monde, y compris nous, ici en maintenant, on perds de vue l'état global de la station. On perds de vue le fait qu'on est tous censé être là, ensemble. Que sans cette entraide et cette confiance qu'on avait il y a encore peu, on est dans un cercueil collectif de béton, avec en tout et pour tout, vingt mètres de béton entre le vide radioactif et une atmosphère encore respirable.
"Et ça, ça m'empêche de dormir. Quand les premiers modules vont perdre leurs système de survie, ils risquent de déclencher une réaction en cascade et de provoquer un effondrement complet de tout ça, et nos belles idées ne vaudront alors plus rien. Vous ne pourrez pas maintenir les gens au calme, vous ne pourrez pas empêcher la panique, et ce sera un désastre."
Elle s'arrête. Staxy la sent trembler sous sa main. Elle ne peut rien faire d'autre qu'écouter. Proposer une solution, chercher des idées pour sortir du problème, maintenant, n'est pas la chose à faire. I faut que la crise passe.
Alors elle laisse Octane pleurer, restant à côté d'elle, juste au bout des doigts sur lesquels Octane se cramponne.
"J'ai juste l'impression, reprend Octane, que je suis la seule à me rappeler qu'on dérive dans l'espace, avec rien d'autres que notre volonté de vivre ensemble et une coque de béton de vingt mètres d'épaisseur comme seule protection."
Elle sèche ses larmes. Le plus gros est passé, il faut maintenant avancer.
"Et donc, tu penses qu'on a plus cette volonté ?" demande Staxy.
"T'en penses quoi toi ?" répond Octane pour ne pas avoir à formuler encore ses idées. Mais ce n'est pas comme cela que les choses sont censée fonctionner. Donc Staxy, attend qu'Octane réponde à la question.
"Bon," reprend elle "vu comme je vois les choses, et peut-être que je me trompes hein, mais ce que je vois sur les quelques instances sociales sur lesquelles je traîne, j'ai plus l'impression que le discours s'est centré sur le fait de déterminer une culpabilité, de trouver qui à tort ou raison plutôt que de trouver comment faire avec ce qui n'est, au final, qu'un sujet sans importance, quelque chose qui n'aurais jamais du prendre autant d'importance."
"Tu penses que toute cette crise est sans importance ?"
"C'est pas… " Octane hésite avant de continuer "Ça n'aurais pas du prendre autant de place. C'est pas la première fois que quelqu'un démarre un groupe de personne avec des idées, disons, discutable. Ça arrive. Et normalement, on est censé l'ignorer doucement et passer à autre chose. Ou, à minima, discuter avec les personnes concernées. Non ? Je sais pas, c'est pas tellement ma branche tout ça.
"Demande moi comment établir un système de survie en respectant une atmosphère compatible avec la culture de végétaux autotrophes, je suis dans mon élément. Faire en sorte que les choses se passe bien entre les groupes, en dépit des conflits, et je suis perdue. J'ai jamais voulu me retrouver à gérer ça, c'est plutôt à ça que vous êtes censées servir vous, les idols. Non ?"
"Et justement, je suis là, non ? On discute, je t'écoute. Et je peux te rassurer, tu n'es pas la seule à être inquiète de ce qu'il se passe. Et même autour de toi, je suis certaine que… comment s'appelle ton crush, là ? Audrey ? Bref, je suis certaine qu'Audrey aussi partage tes inquiétudes, sinon elle ne passerait pas autant de temps à essayer de maintenir les canaux de discussion ouvert, non ?"
Staxy essaye de garder un ton réconfortant, même si elle interprète la dernière remarque d'Octane comme une attaque. Elle n'est pas là pour se défendre ou se justifier, elle est là pour aider Octane à se sortir de cettephase dépressive. Et elle peut encaisser quelques coups.
Ça lui fait moins mal que d'être rejeté par Coda. Même si celle-ci veut parler de leur relation, et qu'elles ont malgré tout réussi à bosser ensemble, elle a mal pris la fuite de Coda.
"Et toi, tu vas comment ?" lui demande Octane, qui s'est redressée sur son tabouret.
"Pff… Pas terrible. Des soucis amoureux, mais rien de grave." répond Staxy. "Et puis, ce n'est pas avec toi que je dois en parler. C'est avec elle."
Octane refait les niveaux de café dans leur tasses, et elles s'éloignent de leurs situations personnelles, ou de l'état social de la station. Staxy parle un peu des tentatives de Cuddle the Urshins de préparer un nouvel album et Octane l'écoute se plaindre de la difficulté de se réunir pour enregistrer une démo ou deux ou du fait que ses camarades de jeu semblent plus motivées par leurs histoires de sexe que par le fait de sortir de nouveaux sons.
Elles finissent pas se séparer, Octane étant manifestement dans un meilleur espace mental. Il y aura encore de nombreuses sessions de ce genre, et il faudra aussi que RAFT se mette en tête de cesser de se comporter de cette façon est résorbe les problèmes.
Elle attrape le trottoir roulant et, se laissant porter par la chaussée de caoutchouc, elle reprend son entoptique et ses analyses des répercussions de son papier. Staxy se concentre sur les effets de leurs révélations sur le mouvement.
Assez rapidement, elle perçoit des lignes de fractures. Leur mèmeplexe n'était pas très cohérent avant, une conséquence de n'exister que dans des espaces sans mémoire, mais elle distingue très clairement maintenant des blocs qui s'écharpe autour de la référence dogmatique que Coda et elle leur ont jetées dessus. Il ne leur est plus possible d'oublier les incohérences, elles sont exposées et repartagées un peu partout dans les recoins de l'infosphère, circulant même au delà des liens de syndications suspendus.
Certains ignorent ce texte et érigent des contre feux, en usant des mèmes conspirationnistes classiques : fabrication de preuve par l'ennemi, attaque de la cabale sur le peuple, tout cet attirail classique des narratifs autoritaires. Une partie de ce groupe s'est aussi mis en tête de convaincre les autres de marc Vitus est un imposteur, un traître à la cause, et que ses écrits n'existent que pour semer la confusion et détourner les messages de :-.
Chacun des deux groupes, ainsi que de nombreuses sous-factions, sont cependant d'accord sur le fait qu'il faille de libérer :- des mains de la cabale, et que les autres doivent rentrer dans le rang. Leur nombre réduit, note Staxy. Du moins, les plus zélés d'entre eux se font plus rare, même si de nouveaux comptes semblent apparaître dans ce mouvement mémétique.
Reste à savoir si Octane a raison d'être pessimiste sur la capacité de la station à passer au-delà de tout ça. Et aussi, et ce d'une manière plus immédiate et personnelle, savoir ce dont Coda veut parler.