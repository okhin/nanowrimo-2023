title:          Simena
ID:             246
type:           md
summaryFull:    Coda prend contact avec Simena, sans son icône, et lui dit qu'elle est disponible pour parler si elle en ressent le besoin, sans la gêne des scripts et des icône si elle préfère.
POV:            0
notes:          {C:1:Coda-6}
                {C:0:Simena Von Guyen the Third}
                {P:5:Coda et Simena} Midpoint
                {C:2:Coelia}
                {W:65:scripts}
                {W:11:Monster  Bash}
                {C:17:Yas}
                {C:12:Tik}
                {W:105:le mouvement}
                {C:4:Giuse-P}
                {W:5:Xenon Girl}
                {W:85:hotline}
                {W:25:Vocas}
                {W:6:Naked Broccoli}
                {W:112:interface entoptique}
                {W:105:le mouvement}
                {W:10:Consortium}
                {C:8:Xicha}
label:          4
status:         5
compile:        2
setGoal:        1500
charCount:      8602


Simena a failli rater le message de Coda dans le flux de notifications constants qui ne cesse de lui tomber dessus. Mais iel a faini par tomber dessus dans sa messagerie privée, là où personne, à part quelques membres du Tabula Rasa qui continuent d'étendre son protocole, ne vient s'adresser à ellui. Ni Xicha, ni Octane, ni Sarah, ni Yas.
Iel a mis ensuite un peu de temps à comprendre que Coda et Coelia sont la même personne. Enfin, pas exactement la même personne, l'une étant l'interprète de l'autre, mais l'idée est là. Mais donc, l'interprète de Coelia veut lea voir. Lea revoir pour être exact, iels se sont déjà vus et Simena n'en garde pas un très bon souvenir. En partie à cause d'iel d'ailleurs.
Simena a hésité avant de répondre au message, mais iel se sent un peu trop à l'étroit socialement au Monster Bash. Meme si iel descend de temps en temps bosser sur son projet de sculpture, iel passe l'essentiel de ses journées mis à l'écart.
D'autant que, depuis le départ de Tik, l'ambiance au Monster Bash dégénère. Les ex-communications se succèdent, Giuse-P ne cessant d'expulser toute parole vaguement dissonante de son credo, à chaque fois convoquant une réunion dont les effectifs ne font que réduire.
Iel a essayé de parler avec Giuse-P de l'utilisation de son image, mais celui-ci me parle plus à personne. Il passe de plus en plus de son temps à enfiler les hydrazine, jusqu'au moment où l'ivresse l'empêche de se tenir debout seul. Il n'y a guère que Yas qui soit maintenant tolérée dans le bureau qu'il s'est aménagé dans l'espace VIP, à l'étage du Monster Bash.
Simena finit donc par répondre au message de Coda, et iels se donnent rendez-vous au Xenon Girl. Un lieu en perte de vitesse et pour lequel Simena ne parvient pas vraiment à trouver une adresse précise, et les explications de Coda ne réussissent qu'à l'amener à proximité. Iel finit par trouver l'entrée du bar, au détour de coursives.
Les néons de la façade luttent avec l'éclairage de la placette, grésillant et parvenant encore à maintenir une présence dans le spectre visuel. Le reste du lieu semble a l'abandon. Juste le son, familier pour Simena maintenant, de la CiBi, et le fait que les lumières soient éclairées lui signalent que ce lieu est occupé.
Étrangement, elle trouver l'endroit où elle sculpte dans l'anneau fantôme, presque plus vivant que ce bar oublié de toustes. Pas de volontaires pour tenir le comptoir. Pas de vocas qui réquisitionnent l'espace une fois les adultes partis. Rien d'autre que des tables vides qui prennent la poussières et, affalée sur le bar, Coelia.
Ou, du moins, une version non maquillée de Coelia, qui n'est pas non plus habillée de ses costumes de scènes. Et qui n'a pas non plus cette lueur pétillante dans les yeux. Juste une forme d'ennui, de banalité.
"Salut," lui dit Coda en l'accueillant. Elle se redresse et réajuste un peu sa robe de dentelle qui tranche avec le style pour lequel Coelia est connue.
"Désolée pour le retard, je me suis perdue en route," lui dit Simena, s'asseyant en face de Coda.
"C'est pas grave. J'ai pas grand chose d'autre à faire, et je voulais te parler."
"Justement, rond Simena, c'est ce que je n'ai pas bien compris. Pourquoi tu veux me parler ? J'ai pas été super sympa la dernière fois, alors pourquoi tu veux me revoir ?"
Coda prend quelques instants pour répondre.
"Écoute, tu avais tes raisons pour me dire ce que tu m'as dit l'autre fois. Et elles sont probablement toujours valable. Mais je me demandais comment tu allais, si tu avais quelqu'un avec qui parler, surtout que j'ai vu que tu traînais avec ce Giuse-P…"
"Et donc, tu t'es dit que je pourrais te balancer des détails croustillants, c'est ça ? Non, je suis désolée, mais non."
"Écoutes, reprend Coda, je suis psa là pour ça. Je n'ai pas vraiment d'autres moyens de te convaincre de ça, donc si tu ne veux pas en parler, n'en parlons pas.
"Mais je pense que tu as besoin de parler à quelqu'un. Et c'est tout ce que je te propose. Je ne suis peut-être pas la meilleure idol, ni la meilleure journaliste, mais je sais me mettre à l'écoute. Et tu as l'air d'avoir besoin de quelqu'un à qui parler."
Simena prend quelques instants pour réfléchir. Iel se lève et commence à faire les cents pas, à essayer de mettre les choses en ordre. De déterminer si iel peut faire confiance à Coda, si iel peut accepter cette main tendue.
Iel avise une caisse contenant des bouteilles d'un liquide rose et en sors deux. Iel se rassied en face de Coda, posant les deux bouteilles de soda sur le comptoir et les ouvrants en faisant levier avec le coin du comptoir.
"D'accord," dit-iel. "Tu veux m'écouter ? D'accord. Faisons-ça." Iel prend une inspiration.
"Tu voulais savoir comment j'allais ? Ben ça va pas. Genre, je suis même pas certain que tu puisse envisager à quel point ça ne va pas. Globalement, au mieux, dans les coursives les gens s'écartent de mon chemin. Au pire ils m'insultent et me crache dessus, et je m'attends à ce que certaines d'entre elleux finissent par se dire qu'iels peuvent me sauter dessus. Juste comme ça.
"Le seul endroit où je me sentais bien, là où je commençait à me sentir bien, et même à développer des relations, ils ont décidés de me jeter dehors. Et je ne sais même pas si elle est d'accord avec eux, si elle aussi à peur de moi, ou si elle n'a réellement pas le temps de me parler, parce qu'il y a des choses plus importantes.
"Et déjà, rien que là, toutes les options sont merdiques non ? Mais tu sais c'est quoi le pire ?" demande Simena, reprenant sa respiration et avalant une gorgée du liquide rose qui a le goût synthétique de la cerise.
"Le pire c'est, au final, de réaliser que j'étais presque mieux dans puits de gravité. Là au moins j'avait un peu de solidarité. Quelques amis. Rien de transcendant, mais au moins un endroit où écouter de la musique avec des camarades de galère.
"Là, les seuls qui pensent être mes camarades de galère, pensent que tout leur est dû et confondent oppression et ennui. Tout en reconstruction tous les systèmes d'oppression en un temps record."
Simena termine sa bouteille de soda rose, et attends une réaction de Coda. Iel est passé d'un état de tension, d'anxiété à celui de colère et, maintenant qu'iel a dit ce qu'iel avait besoin de dire, de tristesse.
Coda boit tranquillement sa boisson avant de poser al bouteille sur le comptoir métallique, le liquide scintillant dans les lumières cyanosées du lieu.
"D'accord. Mais tu n'es pas toute seule. Même si ça peut donner cette impression," dit-elle.
Simena veut répondre que si, iel est seul. Mais le fait qu'iel soit là en face de Coda qui semble sincèrement vouloir l'écouter l'empêche de s'apitoyer sur ellui même. Reste la fuite, mais iel en a marre de fuir.
Alors iel essaye de changer son fusil d'épaule, de ne pas se contenter de juste faire une litanie de plaintes. Coda, seule, ne pourrais de toutes façon pas y faire grand chose, alors il ne reste qu'à parler de ses émotions, de ses sentiments. De ses attentes qui ont étés réduites en pièces par la dure réalité de l'aliénation qu'iel subit. Coda essaye d'abord de donner du contexte, d'expliquer que cette aliénation est le produit d'une méfiance de tout ce qui vient du consortium, mais le regard que lui jette Simena l'arrête dans son argumentation.
L'heure n'est pas aux justifications. Ni aux explications, pas encore du moins. Pour le moment, tout ce dont veut parler Simena c'est ce dont iel n'a pas pu parler depuis son arrivée sur la station, ou presque : du fait que Xicha lui manque, du fait qu'iel a envie de pouvoir contribuer à maintenir RAFT mais que personne ne veuille son aide, ou n'en est besoin, du fait qu'iel est complètement paumée.
Coda l'écoute et semble prendre quelques notes. Iels enchaînent les bouteilles de ce soda rose beaucoup trop sucré et la conversation, après revenir régulièrement autour du Naked Brocoli et de Xicha, finit par dériver sur les systèmes de communication, et le petit protocole sur lequel Simena bosse avec Tabula Rasa.
Ce qui semble intéresser particulièrement Coda, qui pose rapidement tout un tas de question, qui essaye de comprendre comment marcherait un système social basé sur ce genre de protocole distribué, ne dépendant pas de la bonne volonté des administrateurices des plateformes sociales et de leur capacité à couper les liens de fédération de manière unilatérale.
Iels finissent par se séparer, après que Coda se soit assurée de pouvoir communiquer sur ce nouveau protocole. Elle promet de garder le contact avec Simena, alors qu'iel se prépare à redescendre bosser sur son projet, seule, à réfléchir à ce qu'iels se sont dit pendant une bonne partie de l'après midi.
