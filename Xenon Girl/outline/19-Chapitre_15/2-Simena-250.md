title:          Simena
ID:             250
type:           md
summaryFull:    Elle repasse au Naked Brocoli, pour voir Xicha. Les habitués l'empêchent de rentrer, lui disant que Xicha ne veut pas la voir
                Simena essaye de recontacter Xicha, mais elle n'y parviens pas, et se retrouve seule et isolée.
                
POV:            0
notes:          {C:0:Simena Von Guyen the Third}
                {C:8:Xicha}
                {P:3:Simena et Xicha} Pinch
                {P:5:Coda et Simena} Pinch
                {W:6:Naked Broccoli}
                {W:87:ersatz de café}
                {W:89:beignets}
                {W:79:communicateur}
                {W:129:système fédératif}
                {W:140:protocoles texte}
                {W:134:anneau nadiral}
label:          4
status:         3
compile:        2
setGoal:        1500
charCount:      9062


Simena couve un café entre ses mans, adossée à un comptoir ouvert, un de ces endroits maintenu actif par la bonne volonté globale et permettant à tout un chacun d'attraper quelque chose à boire ou grignoter pendant ses trajets le long de l'anneau de la station. Un des endroits où il n'y a généralement personne qui reste plus longtemps que nécessaire, ce qui lui convient parfaitement.
Iel essaye de trouver en ellui la force d'aller affronter le Naked Brocolli, juste de l'autre côté du boulevard. Cent mètre de coursives et de jardin la sépare du lieu qui l'a, initialement, accueillie, avant de lea rejetter. Cent mètres à faire sous le regard de, et c'est une nouveauté, la personne qui semble garder la porte. Cent mètres qui la sépare d'une conversation avec Xicha. Celle-ci ne lui répond pas, et iel n'aime pas ça. Iel n'a plus réellement de média social, la plateforme qui hébergeait son compte s'étant effondrée sous la charge des messages lea mentionnant, mais iel aimerait quand même pouvoir parler avec Xicha.
Iel termine son café, rince la tasse et la pose sur le séchoir, à côté des autres, et se mets en route vers la porte du Naked Brocoli, passant la sangle de son sac de voyage au-dessus son épaule. Depuis l'autre bout de la pelouse la videuse l'a repéré et la suit du regard, alors qu'iel finit par atteindre le seuil du Naked Brocoli. Simena ne la reconnaît pas, mais elle aurait pu être une habituée du lieu.
"Désolée Simena, mais tu ne vas pas pouvoir entrer," lui dit la videuse relativement poliment lorsque Simena n'est plus qu'à un mètre d'elle.
"C'est nouveau ça," dit Simena, cessant sa progression. "Depuis quand vous avez besoin de videurs ?"
"Depuis que des personnes comme toi menacent la sécurité du lieu. C'est pas ma décision, mais…"
"Laisse moi deviner, c'est encore un vote stupide ?"
Simena toise la videuse, celle-ci semble faire preuve d'un peu de compassion, mais elle reste en travers de sa route.
"Je veux juste parler avec Xicha," dit Simena.
"Personne ici ne veut te parler," lui répond la videuse, maintenant le ton faussement sympathique de la condescendance.
"Je pense qu'elle est assez grande pour me le dire elle-même, non ?" Simena puise dans ses dernières réserves d'énergies mentale pour maintenir l'anxiété sous contrôle, et réussir à tenir tête à la videuse.
"Écoute," dit la vigile, lui posant la main sur l'épaule, pour la tenir à distance et prenant une inspiration. "Je vais être claire. Personne ne veut de toi ou de ta sale tête. Xicha ne veut pas te voir. _Je_ ne veux pas te voir. Alors retourne d'où tu viens, ça sera mieux pour tout le monde, et ne me fais pas perdre patience." Elle sourit en disant ça, en gardant cette voix neutre de refus qu'utilise tous les agents administratifs qui sont vraiment désolé, mais qui ne peuvent rien faire. Ce ton dépourvu d'émotion, de compassion, qui a pour bit de faire craquer, d'une manière ou d'une autre, l'interlocuteur.
Simena comprend qu'il n'y a pas de marge de négociations, qu'aucun effort ne sera fait pour essayer de l'aider. La délégation de la responsabilité vers le collectif par le vote empêchera de toutes façon la videuse de changer d'avis. Elle ne fait qu'obéir à la volonté collective, elle leur a abandonné son sens du jugement.
Mais Simena ne veut pas en rester là.
"Et il se passe quoi quand tu perds patience ?" demande-t-iel, essayant de capitaliser sur la peur qu'iel instille dans les autres. peur basée sur des faits concrets, dont la démonstration inonde l'infosphère à ses dépend. Iel ne veut cependant pas en arriver là, alors iel relance une tentative de négociations. "Je veux juste parler avec Xicha, je n'ai pas d'autres moyens de la joindre. J'ai pas besoin que ça se passe au Naked Brocoli, on peut aller ailleurs. Je veux juste que tu lui passes le message.
"Et une fois que j'aurai pu discuter avec elle, je me casse. Promis. Tu ne me verras plus."
"Elle n'est pas là de toutes façon, donc bouges de là," répond la videuse.
"Elle n'est pas là ou elle ne veut pas me parler ? Faudrait savoir."
Simena jette un œil par dessus l'épaule de la videuse et essaye de se faire une idée de l'état du Naked Brocoli. Iel n'a pas eu le temps de le connaître aussi bien que certains, mais iel voit bien que la cantine collective a connu des jours meilleurs.
La salle principale est plongée dans la pénombre, l'éclairage, qu'elle s'est pourtant éreintée à retapper, est en mode minimal. Pas de projecteurs qui habillent les murs de leurs motifs coloré, ni des reflets des boules miroir immobiles. Le comptoir est encombré de vaisselle à faire, la scène qui sert aux spectacles est remplie de caisses entassée, témoignant de l'inutilisation de celle-ci.
Même depuis l'extérieur, Simena peut constater que les choses ne se passent pas bien. Les odeurs d'épices chaudes, s'échappant normalement des fours emplies de différentes pâtisseries, sont remplacées par celle de l'air qui transite paresseusement dans les circuits de filtration.
Les affiches sur la porte datent un peu. Elles n'ont pas encore été recouvertes par le nouveau programme, ou l'annonce du prochain concert de Bonecos de Empurar ou de Cuddle the Urshins, comme si le temps avait cessé de s'écouler dans cette partie de la station.
Iel sait que Xicha n'aurait jamais acceptée que le lieu péréclite à ce point. Même Kit aurait mis les bouchées doubles pour tenir la cantine dans un état de propreté acceptable.
La seule trace de nouveauté c'est cette banderole suspendue au-dessus du comptoir, que Simena parvient à déchiffrer : "Équipe en grève ! Démerdez-vous ou arrêtez-vos âneries."
"T'aurais pu me dire que Xicha était en grève, je t'aurais foutu la paix tout de suite," répond Simena se dégageant de la videuse et commençant à s'éloigner du Naked Brocoli.
Inutile d'insister. Si même Xicha a jeté l'éponge avec elleux, alors Simena n'as pas à leur consacrer d'énergie. Iel parvient à s'éloigner, à plonger dans un des jardins encadré de haies avant que l'anxiété ne finisse par avoir gain de cause et ne prenne le contrôle.
Iel s'effondre au sol en tremblant et en laissant couler quelques larmes silencieuse. Iel est incapable de penser et reste prostrée là, à attendre que l'anxiété ne se résorbe, au contact de la pelouse humide, à l'abri des haies d'argousiers, déconnecté de toutes formes de sociabilisation dans une station qui lui est de plus en plus hostile.
Puis, les émotions finissent par remplacer l'anxiété. Iel redevient lentement fonctionnel. Si Xicha claqué la porte du Naked Brocoli, c'est qu'elle n'est pas d'accord avec le collectif, et qu'elle veut lea revoir. Ou qu'elle ne serait pas contre la revoir. Il y a probablement pleins de raisons qui font qu'elle n'a pas repris contact, au rang desquels, la désyndicalisation des plateformes qui les as déconnectées l'une de l'autre.
Mais justement, Simena a un moyen de communication qui ne dépend pas de ces plateformes sociale. Le petit projet de Tabula Rasa a pris un peu peu d'ampleur, surtout depuis que Contrôle s'est mis à utiliser le protocole, et de nombreux patchs ont été apportés au système de communication ad-hoc.
Iel doit juste trouver un annuaire, et vérifier si Xicha fait partie des utilisatrices. Elle envoie une requête simple sur une des messageries automatiques qui lui renvoie par salve les tables de conversation entre pseudonyme et adresse de destination, les unes après les autres. Lentement. Ce protocole n'est pas fait pour envoyer tant de donnée à la fois et ne permet que des connexions directe, mais tabula Rasa a commencé à peupler le protocole de petit système de ce type.
Plus d'une demi heure de téléchargement pour récupérer le petit millier d'enregistrement, l'essentiel étant passé à établir des connexions à travers les différents capteurs de la station jusqu'à arriver dans l'armoire abritant le service d'annuaire, et iel peut enfin effectuer une recherche.
Octane et Sarah ont, bien évidemment des comptes, mais elle a déjà enregistré leurs adresse. Elles n'ont pas le temps de parler avec ellui, même si iel répondent généralement à ses messages. Et à chaque fois, iel s'en veut de leur prendre du temps, alors qu'elles sont occupée à essayer de maintenir RAFT en état et à éviter les collisions dans le voisinage.
Mais enfin, elle finit par trouver le compte de Xicha.
"Hey, Xicha, écris-t-iel, j'aimerais vraiment qu'on se parle. J'ai essayé de passer au Naked Brocoli, mais visiblement tu n'étais plus là bas.
"Ça ne va pas bien du tout," ajoute-t-iel dans un second message. Le premier ayant déjà épuisé la quantité de mémoire disponible. "Et si tu ne veux plus me parler, s'il te plaît, dis le moi. Et je te laisserai tranquille."
Les messages s'empilent dans une boîte d'envoi. Le réseau semble saturé ou limité et il lui faut plusieurs secondes pour essayer de délivrer les quelques octets de texte bruts.
En attendant, iel doit s'occuper et ne pas trop attendre à réfléchir. Alors iel se mets en route vers la zone de transfert d'abord, puis vers son sanctuaire dans l'anneau fantôme, là où iel pourra travailler sur ses sculptures, en attendant que Xicha ne lui donne des nouvelles.