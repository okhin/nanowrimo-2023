title:          Simena
ID:             247
type:           md
summaryFull:    Elle refuse d’être un symbole sur lequel elle n’a aucun contrôle, d’être un genre d’Idol, de méta-icone.
POV:            0
notes:          {C:0:Simena Von Guyen the Third}
                {P:2:Le Monster Bash} Plot turn
                {P:8:Émancipation de Yas}
                {C:4:Giuse-P}
                {C:17:Yas}
                {W:5:Xenon Girl}
                {C:1:Coda}
                {W:11:Monster  Bash}
                {C:20:Bay Lee}
                {C:12:Tik}
                {W:84:hydrazine}
                {W:118:nano-peau}
label:          4
status:         8
compile:        2
setGoal:        1500
charCount:      9978


Une nouvelle vague de mème déboule dans les rares flux sociaux que Simena garde a l'œil. Iel n'a plus beaucoup d'accès aux instances les plus groesses, mais iel essaye de se tenir au courant des différentes phases mémétiques mise en place par le mouvement, surtout pour les parties qui la concerne. Le flot d'aller retour haineux se basant sur ces quelques images statiques, mal cadrées, mal compressées, devient aussi prévisible que les orbites planétaires du système solaire. Iel pourrait probablement en tirer une certaine forme de zen, si chaque rotation mémétique n'ajoutais pas autant à son niveau de stress.
Rentrer depuis l'anneau inférieur, avec un partie de son matériel stocké dans le grand container qu'iel porte en bandoulière, ses mouvements restreint par le scaphandre qu'iel doit porter à chaque fois qu'iel retourne dans sa galerie, devant se faufiler à travers la foule de personne qui vont et viennent à leur occupation, n'est vraiment pas une expérience plaisante. Iel note cependant que les personnes qu'iel croisent sont plus absorbés par leur reste de sociabilisation en ligne, que par sa propre présence. Les quelques regards qui lea reconnaissent semblent plus s'étonner de sa présence, ou de réaliser qu'iel est "cette personne là" dont le groin éclaboussé du sang de quelqu'un d'autre est utilisé pour défendre tout et son contraire, que de s'entêter à lui montrer haine et rejet.
Coda a raison. Les choses se tassent, la station va probablement passer à autre chose, Giuse-P et son monde vont retourner dans les oubliettes et angles morts sociaux de l'habitat dont il fait parti. Mais en attendant, chaque nouvelle journée ou presque ramène sa vague de harcèlement en ligne. Et, iel le sait, il y a probablement suffisamment de personnes à bord de cet habitat qui sont prêt à vouloir lui sauter dessus si l'occasion se présente, pour se mesurer à ellui. Iel en a développé la conviction, même si cela ne se base que sur son stress et ses peurs. Même si cela est probablement irrationnel. Cela ne l'empêche pas d'y penser et d'être sur ses gardes en permanence.
Et cette vigilance de tous les instants sape son énergie émotionnelle. La dépression lea guette. Pas exactement la même que celle à laquelle iel a du faire face aux pires moments de son isolement à bord du Just a big dumb spaceship. Ça iel a appris à gérer, et il lui suffisait en général d'aller prendre le vide dehors, harnachée à la coque de béton qui dérivait alors à pleine vitesse dans l'immensité spatiale, et iel se sentirait mieux.
Cette forme de dépression est plus insidieuse, inscrite plus profondément dans sa personnalité, se nourrissant de ses insécurités, de son aliénation, de sa mise à l'écart. Et la seule chose qu'iel à pour y faire face, ce sont une liste de personne qui, iel en est persuadée, n'ont pas le temps de s'occuper de l'écouter, et un lieu qui ne lea tolère que parce qu'iel est devenu contre son gré le symbole de leur lutte inepte, basée sur des oppressions inexistante.
Perdu dans ses pensée, Simena ne réalise qu'à peine qu'iel est arrivée au Monster Bash. La terrasse de la salle d'arcade est vide, et iel n'entend pas de bruit franchir les portes battantes. La salle principale est vide. Il est encore trop tôt pour que le mouvement ne se soit assemblé aujourd'hui. À moins que leurs chiffres ne se soient encore considérablement réduit. Pas de trace de Yas non plus, ce qui soulage un peu Simena, iel n'est pas vraiment d'humeur à avoir une conversation amicale avec qui que ce soit en ce moment.
"Hey, Simena, ça va?" lui demande Bay Lee, l'interceptant alors qu'iel grimpe dans la mezzanine, vers sa minuscule couchette. Les vestiges des collations passées occupent l'espace entre les banquettes et les tables, personne ne se décidant à nettoyer. Et Bay Lee, le nouveau Tik, se tiens là, assis, à attendre bêtement, au lieu de s'occuper et de ranger l'espace.
"Giuse-P est dans le coin ?" demande Simena.
"Il a demandé à ne pas être dérangé," répond Bay Lee, se plaçant ostensiblement entre ellui et la cloison assemblée à la va-vite et qui coupe l'espace VIP en deux, offrant à Giuse-P l'espace privé qu'il juge nécessaire pour accomplir sa mission.
Simena remarque seulement maintenant les éclats de voix, étouffés par l'épaisseur de cette cloison qui sépare. Deux voix bien distinctes s'affrontent dans un match de cris. Celle de Giuse-P, un peu trop haut perchée par rapport à la voix dont Simena a l'habitude, preuve d'une perte de contrôle de Giuse-P sur ses émotions; et celle de Yas, quelque part entre la colère et les larmes, en rupture émotionnelle. mais au-delà des intonations et des éclats, Simena ne parvient pas à déchiffrer ce qui se dit.
"De quoi tu voulais lui parler ?" Bay Lee essaye de recentrer l'attention de Simena, et la sienne, sur autre chose que l'engueulade.
"Hein ?" répond Simena, prise au dépourvu par la présence de Bay Lee, qui essaye de se donner de la consistance.
"Giuse-P, tu voulais lui parler. Tu veux lui parler de quoi ?" reprend Bay Lee.
"Du fait que j'aimerai qu'on me demande mon avis avant de lancer une énième campagne de mème sur mon dos. Mais peut-être que tu peux y faire quelque chose ?" demande-t-iel, convaincu de connaître déjà la réponse de Bay Lee.
"Giuse-P veut qu'on maximise l'engagement sur nos publications, tu le sais. Et c'est pas ma faute, mais c'est ta tête qui déclenche le plus de réaction, peu importe le message qu'on y adosse. Et puis en vrai, tu n'étais pas obligée de te battre hein. C'est un prix à payer plutôt léger, tu ne trouves pas ?" Il parle comme un algorithme d'optimisation de moteur de recherche, lui donnant un ton encore plus paternaliste que ne le lui donne ses remarques. Il rouvre la bouche pour relancer une tirade sur la cause, et les sacrifices personnels à faire, avant que Simena ne l'interrompe.
"Je demande encore gentiment," dit-iel, serrant les poings et prenant une posture bien plus agressive, ignorant le fait que Bay Lee justifie leur harcèlement par son agression.
"Hey, dit-il, mettant les mains légèrement en l'air, Calmes-toi. Moi je veux bien arrêter ce genre de campagne, mais c'est pas moi qui décides de ça. je ne fais qu'exécuter les décisions de Giuse-P. Il faut donc que tu vois ça avec lui, mais pas maintenant. Tu entends bien qu'il est occupé ?"
"Et moi qui croyais que tu étais responsable de ça, que tu te battais pour l'autonomie, la capacité a agir, tout ça. Et tu ne peux pas prendre une décision sans demander une autorisation avant ?"
"C'est pas ça c'est …"
"Aies au moins l'honnêteté de dire que tu n'as pas envie d'arrêter, plutôt que de blâmer les ordres. Et pousse toi de mon chemin, je vais aller lui parler," le coupe Simena. "Maintenant." ajoute-t-iel, avançant vers la porte qui la sépare de Giuse-P.
Bay Lee recule d'un pas, mais se maintiens entre Simena et la porte. Porte à travers laquelle les sons de l'engueulade continuent de filtrer, même si son intensité baisse.
"Il ne veut pas me parler," répond Simena, essayant de garder son calme, de ne pas donner raison à tout ce qui est dit à son sujet. "Il est toujours occupé, surtout quand _je_ veux lui parler. Et j'ai pas signé pour être le symbole de votre cause moi." Iel maintiens la rage concentrée quelques part dans ses poings et ses phalanges.
"De *notre* cause," lea corrige Bay Lee.
"OK, si tu veux. Je veux juste que vous changiez un peu votre fusil d'épaule."
"Tu ne peux pas nous dicter tes conditions comme ça, Simena. On t'accueille, on t'accepte parmi nous et tu veux nous forcer la main comme ça ? On fait tous des sacrifices, tu sais ?"
Le peu de répit émotionnel qu'iel a tiré de ses sessions de sculpture dans l'anneau nadiral est arrive à sa fin et un mélange de colère et de dépression se saisit de Simena. Quel genre de sacrifice cette personne qui n'a jamais manqué de quoi que ce soit peu bien avoir eu à faire ?
"Écarte-toi, ou soit prêt à sacrifier un œil."
Bay Lee s'écrase, il a poussé Simena aussi loin qu'il a pu, il a essayé de lea retenir, mais il voit maintenant ce qu'iel est.
"Tu ne veux pas rentrer dans cette salle," dit-il, essayant, une dernière fois, de détourner Simena de ce qu'il se passe de l'autre côté de la cloison.
Simena le bouscule de l'épaule et se dirige vers la porte qui lea sépare de Giuse-P et de Yas. Et cette porte était fixée rapidement, sans vraiment se soucier de solidité. Ou alors Simena est particulièrement énervée, et iel ne se rend pas compte de la force qu'elle applique à la structure d'un coup d'épaule. La porte s'arrach de ses gonds et tombe en travers des banquettes de l'ancien espace collectif, de l'autre côté de la cloison.
Yas est roulée en boule au pied de Giuse-P, des larmes coulent sur ses joues alors qu'elle garde sa tête coincée entre ses bras, en position fœtale. Au-dessus d'elle, Giuse-P est encore en train d'hurler, les poings serrés, sa faible musculature en tension, prêt à laisser pleuvoir les coups. Son propos est inintelligible, à moins que Simena n'ai, quelque part, décidé que cela n'avait aucune importantce. Ce qui est important, c'est d'agir. Maintenant. De mettre Yas à l'abri. Ses instincts prennent le dessus, et elle comble l'espace entre ellui et Giuse-P avant qu'elle n'ait le temps d'en formuler la pensée.
Iel attrape Giuse-P par l'épaule et le tire vers l'arrière, se positionnant entre lui et la voca roulée en boule.
"Qu'est-ce que tu fous là?" lui demande-t-il, surpris, sa voix redescendant de plusieurs gammes.
"Ça va, Yas ?" demande Simena, ne quittant pas Giuse-P des yeux. Iel entend Yas répondre que oui, ça ira, avant de se remettre doucement sur ses pieds et de quitter la pièce.
"Je t'ai posé une question !" dit Giuse P, "Qui t'as laissé entrer chez moi ?"
"Personne ne m'a laissé faire quoi que ce soit. J'en ai eu marre que tu ne me parle pas, alors je suis venue te voir directement. Je voulais te demander d'arrêter d'utiliser mon image sans me demander mon avis. Mais étant donné la situation présente, je suis tentée de régler les problèmes de fond de manière plus percutante."