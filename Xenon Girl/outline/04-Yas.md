title:          Yas
ID:             3363
type:           md
POV:            17
notes:          {P:8:Émancipation de Yas}
label:          4
status:         5
compile:        2
setGoal:        1500
charCount:      6058



Yas lâche le joystick de la borne d'arcade de _Fuir Tenochtitlan_. Elle viens de résoudre, une fois de plus, la crise politique qui secouait le Mexique, en s'effuyant avec la leader de la rébellion post-marxiste. Quelques séquences lui permettent d'inscrire, une fois de plus, son nom en haut du tableau de score.
Il lui faut quelques instants pour réhabituer ses yeux à la lumière noire du Monster Bash. Les personnages de jeux vidéos, mêlent leurs couleurs phosphorescentes aux monstres de films d'horreur, alors que la voca à la peau basanée s'étire pour évacuer les tensions dues a sa mauvaise posture pendant sa partie. L'écran reflête son visage fin, ses yeux bleus contrastant avec les lettres dorées qui écrivent son nom encore et encore.
Elle fait une queue de cheval avec sa touffe de cheveux noire et verts, réajuste la frange qui lui tombe toujours dans les yeux et s'éloigne de la borne d'arcade, en tirant un peu sur les sangles de son harnais. Elle a besoin de ne pas sentir de tension physique quand elle joue.
Les rythmes bubblegum-japlastic-techno des consoles de Dansudansureboryūshon, se mêle aux bruits des pas de danses des joueuses, s'affrontant dans des duels frénétiques. Toutes portes leurs brassards de vocas, comme elle. Mais leurs brassards à elles portent des dessins, des broderies, des bijoux ajoutés par leurs amis, célébrant des souvenirs collectifs, des accomplissements personnels ou des trophées pris à leur conquête. Alors que celui de Yas reste définitivement vierge de toute altération.
Elle décide d'ignorer pour le moment le sentiment de manque qui s'installe en elle à chaque fois qu'elle se retrouve confronté aux autres groupe de vocas, et se dirige vers la zone centrale de la salle d'arcade. Elle attrape une paire de donut et une limonade dans un des frigidaires avant de s'asseoir à une des tables hautes ornée de reproduction de vieilleries pulp. Un peu à l'écart, entre le sas d'entrée et la porte qui mène aux couchettes à l'étage.
Pour valider ses basiques, elle se documente sur la micropolitique et la psychologie sociale. Et elle fait ça seule, à l'ancienne. En lisant des essais et en explosant les scores des roms-sims politiques du Syndicat. Elle publie régulièrement des essais sur les plateformes vocas et elle contribue aussi aux articles de la hotline quand elle le peut. Mais elle n'a que peu de retour. Des émoticônes diverses sont régulièrement associés à ses contenus, mais c'est le cas de n'importe quel bout de texte écrit par quiconque. Et elle aimerait que quelqu'un, pour une fois, remarque son travail. La remarque. Prenne le temps de la connaître.
Elle est sortie de ses pensées par un mec qui lui fait signe de venir s'asseoir près de lui. Sa tête ne lui dit rien, et trois de ses amis à lui sont pris dans une conversation passionnée, dont le sujet lui échappe. Face à ses hésitations, il se lève et vient s'installer en face d'elle.
"Tu es seule?" demande-t-il.
"Plus maintenant." répond-elle.
"Tu connais bien le lieu?" Quelque chose dans son attitude l'intrigue. Il n'est pas grand, pas spécialement beau. Il n'essaye pas de dissimuler une calvitie naissante mais semble porter une attention particulière à sa peau tellement celle-ci à l'air douce. Ses mains sont impeccables, pas de coupures, de bleus, de traces de brûlures. Trois phalanges par doigts, cinq doigts par mains, deux mains.
"Oui, pourquoi?"
"On se demandait s'il serait possible que tu nous aide moi et mes potes. On cherche un endroit où tenir nos réunions et, ont se demandait si on pouvait aller là-haut?"
Il la prend pour une des vocas volontaires du lieu. Elle lui explique qu'elle n'est pas une des gardiennes d'ici, mais qu'ils doivent pouvoir rester en bas, si ils peuvent supporter les bruits des consoles de DDR. Et, alors qu'il repart, elle lui demande quel est le sujet de leur réunion. Il marque un temps d'arrêt, cherchant ses mots avant de répondre.
"Est-ce que tu penses qu'il y a plus dans la façon de fonctionner de cette station que ce que nous dit le discours officiel?"
Bien sûr que oui se dit-elle. Comme dans toute organisation en fait. Elle hésite, ils sont six à leur table maintenant. Aucun vocas n'est présent, mais le sujet l'intéresse. La façon qu'il a eu de le formuler est étrange, certes, mais ce n'est pas pour ça que c'est inintéressant.
"Si je te répond oui, il se passe quoi ensuite?"
"On t'accueille à notre table, on discute, on échange nos points de vue et on mets en place un plan d'action. Enfin, peut-être pas ce soir, mais c'est l'idée. Se rencontrer entre personnes partageant les mêmes inquiétudes, et essayer d'apporter des réponses constructives."
Il donne à Yas l'impression de réciter des articles de la hotline tels que "Comment recruter pour un club politique?" ou "Comment démarrer une révolution par l'intelligentsia?". Mais il s'intéresse à elle. Il s'est arrêté et semble prêt à lancer une discussion, un léger sourire sur ses lèvres fines se dessine. Et quelque chose dans son regard, comme s'il avait un plan, une certitude que tout se passe comme il l'a prévu.
"Je peux peut-être vous aider pour le lieu, finit-elle par lui dire, si toi tu peux m'aider. J'ai besoin d'un tuteur pour mes vocas, et préparer mon mémoire. Tu pourrais m'aider?"
Il est revenu à sa table à elle, mais il s'est mis juste à côté d'elle, comme pour partager le même point de vue. Il est vraiment _très_ proche d'elle. Il passe son bras autour de ses épaules, sa main se promenant près de sa poitrine.
"J'ai justement besoin d'une assistante. Et de l'anonymat d'une voca. Bienvenue à bord, je suis Giuse-P et les gars que tu vois là, c'est ma bande. Et avec eux, et toi, on va changer le monde."
Un grand discours, de grandes ambitions. Et sa main très proche de sa poitrine. Mais elle se sent vue. Et il y a quelque chose en lui qui l'attire. Probablement autant de chose qui devrait la maintenir à distance, mais elle en a marre d'être seule et ignorée.
Alors elle se laisse mener à la table qu'il s'est appropriée, et elle écoute les conversations. Et jamais il ne lui demande comment elle s'appelle.