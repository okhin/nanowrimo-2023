title:          Coda
ID:             3804
type:           md
summarySentence:Kat récupère l'icône de Coda
POV:            1
notes:          {C:13:Kit}
                {C:1:Coda}
                {C:2:Coelia}
                {P:10:Kit}
                {P:4:Une nouvelle voix pour Coda} Plot turn
label:          4
status:         5
compile:        2
setGoal:        1500
charCount:      6481


L'ambiance dans le bauhaus a changé. La vie est revenue, il règne de nouveau cette ambiance un peu électrique, d'agitation. Une ambiance qui peut tenir éveillé Coda plusieurs rondes d'affilée, l'esprit stimulé par les projets de créations qui fusent à travers les synapses des syndiqués qui essayent de donner du sens à tout ce qu'il se passe ailleurs dans Rien à Foutre de ta thune.
À moins que ce ne soit juste l'humeur de Coda qui se soit améliorée et qui lui permette de nouveau de profiter de cette excitation. Elle reste au centre de l'espace, écoutant les discussions vives qui agitent plusieurs groupes de travail, les pieds solidement ancrés sur la dalle de béton.
Elle essaye de profiter au maximum de cette légère ivresse, qui peut-être addictive. Une dernière fois, avant de passer le relais officiellement. Elle a revu Kit au Xenon Girl, et ielles ont discutées longuement. Il a récupéré des détails précis de ce qu'il se passe dans le mouvement, des témoignages des abus commis par certains, des minutes des réunions montrant que tout cela est organisé juste pour satisfaire les besoins de Giuse-P.
Et il veut publier tout ça avec Coelia et Staxy, une suite à leur article incendiaire, dans le but d'achever ce qu'il reste de ce mouvement, de permettre à RAFT de passer à autre chose. Il n'y a pas beaucoup de travail de rédaction, mais recouper tout ce qui a déjà été écris et l'intégré au reste leur a pris un peu de temps et s'est avéré frustrant et fastidieux.
C'est en travaillant sur cet énorme cache de donnée, avec l'aide de Staxy, de Kit et d'amie à lui, qu'elle a réalisé que le journalisme d'investigation n'est pas pour elle. Qu'elle veut définitivement faire autre chose. D'autant que son compte est toujours bloqué sur l'infosphère publique. Elle a alors pris Kit à part pour faire un point avec lui sur son projet d'interprète, de devenir une idol. D'incarner Coelia à sa place, alors qu'elle se concentrerait sur le Xenon Girl, sur son projet d'archive culturelle, sur quelque chose de moins sur le devant de la scène, qu'elle peut faire tout en ayant une vie privée.
Et c'est ainsi qu'elle se retrouve au bauhaus, probablement pour la dernière fois. Du moins en tant qu'interprète. Elle doit retrouver Bey et Kit qui discutent des conditions de la passation dans un des bureaux. Discussion qui doit être animé, Kit ayant exprimé des limites qu'elle n'avait pas quand elle a commencé l'incarnation de Coelia. En particulier sur le rôle du Syndicat dans la gestion de sa vie. Il est clair sur le fait qu'il ne veut pas que sa vie soit gérée par des scripts.
Coda lui a pourtant expliqué que ces scripts servent à le protéger lui, à permettre de maintenir une illusion satisfaisant les relations para-sociales des fans, tout en en limitant les abus. Mais il semble être plus de cette nouvelle école, comme Staxy. À vouloir mettre en place leurs limite, à réduire les relations parasociales, quitte à faire le travail émotionnel et sexuel en personne. À vouloir n'utiliser les CARA que pour se sentir bien en faisant le job.
"Il est dur en affaire ton remplaçant," dit Bey, la sortant de ses pensées. La scripte se dirige vers l'un des distributeurs de nootropiques et se prend une cannette étiquettée en vert et rose, quelque chose pour l'aider à garder la tête hors de l'eau suppose Coda. Bey ne prend pas de CARA, mais elle descend beaucoup de ces boissons.
"Au moins il sait ce qu'il veut. Non ?"
"Je sais pas. On va trouver un terrain d'entente hein, mais c'est compliqué." La petite rouquine avale d'une traite ou presque le liquide qui dégage un parfum de cerise que Coda peut ressentir de là où elle est.
"Il a raison sur pas mal de points cela dit, reprend Bey. Peut-être qu'effectivement, il faut qu'on gère autrement les relations des idols. Et il connaît son sujet aussi. Y compris pour ton papier."
"Le papier qui a poussé le Syndicat à me couper du réseau, à m'isoler et à me laisser me débrouiller seule ?"
"On ne va pas refaire la bataille. Mais oui. Le reste du syndicat voudras jamais faire ça. Je veux dire, regarde-les."
Bey laisse à Coda le temps de contempler l'espace qui les entoure. Coda sait que même si tout le monde est occupé, ielles le sont sur des projets d'éducation, à réfléchir à comment transmettre des messages simples, de s'assurer que tout le monde ait un niveau d'information suffisant. Mais personne ne réfléchit vraiment à quoi faire quand ces messages échouent dans leur mission. À quoi faire pour que des choses comme le mouvement, et tout ce qu'il peut se passer ne se reproduise pas. Ni à comment y mettre fin.
Une forme de naïveté institutionnelle qui était importante quand les choses prenaient forme, et qu'il fallait mettre tout le monde au même niveau. Mais maintenant, cette naïveté est d'une autre époque, les nouvelles idols ne se cantonnent plus au simple domaine de la culture. C'est probablement ce que Kit et Bey essayent de résoudre.
"Il m'a soumis une idée. Et vu que je suis ta scripte pour encore un peu de temps… " reprend Bey
"Pour plus de temps que ça, je vais avoir besoin d'aide pour retomber sur mes pieds tu sais ?" dit Coda, l'interrompant.
"… Pour le temps nécessaire, corrige Bey, je veux t'en demander ton avis d'abord. Est-ce que ça te poserai un problème d'héberger Coelia au Xenon Girl ?"
"Comment ça ?"
"Tu veux faire… quelque chose, autour de la culture là-bas, c'est ça ? Un genre de réseau distribué, ou autre ? Je ne suis pas certaine de bien comprendre. Bref, on pourrais sortir Coelia du Syndicat, au moins au début, le temps que Kit prenne ses marques. Et la baser sur le Xenon Girl. En plus, ça t'attirerais de l'attention."
"Faut voir. Faut que je demande à Staxy, mais pourquoi pas. J'ai besoin d'y réfléchir, d'accord ?"
"Bien sûr. Tiens moi au courant d'accord ? Et… Même si je ne suis plus ta scripte, on reste amies tu sais ? On va continuer de se voir, même en dehors de ce projet."
Se retournant rapidement, Bey retourne à ses négociations avec Kit. Coda est certaine que la scripte cherche à cacher des larmes. Un mélange de stress, de nootropes, mais aussi une façon d'accuser le coup de la séparation entre elle et Coda.
Coda profite encore un peu de l'ambiance, mais elle sait qu'elle doit quitter le bauhaus avant de replonger dans l'énergie épuisante du lieu. Elle passe par sa cabine privée pour récupérer les quelques affaires qu'elle y a stocké, une boule se formant dans la gorge alors qu'elle fait ses au-revoir silencieux au lieu dans lequel elle a passé presque la moitié de sa vie.