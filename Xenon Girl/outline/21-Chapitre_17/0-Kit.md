title:          Kit
ID:             255
type:           md
summaryFull:    Coda, transmet l'icone de Vesta-L à Kat, après en avoir discuté avec Staxy.
POV:            13
notes:          {C:1:Coda-6}
                {C:13:Kit}
                {C:6:Sarah}
                {C:17:Yas}
                
                {W:25:Vocas}
                {W:27:Contrôle}
                {W:11:Monster  Bash}
                {W:26:Brassard}
                {W:18:bazar}
                {W:105:le mouvement}
                {W:14:Idols}
                {C:3:Gab}
                {W:36:Chernobyl}
                {C:10:Bey}
                {W:115:graveuse}
                {W:83:karaoké}
                {C:2:Coelia}
                {W:3:construct artificiel de réminiscence autosuggestive}
                {W:26:Brassard}
                {P:8:Émancipation de Yas}
                {P:10:Kit}
label:          4
status:         5
compile:        2
setGoal:        1500
charCount:      10348


Sarah a insisté pour le traîner dans ce lieu étrange, qui semble célébrer les monstres de films d'horreurs. Les vieux films d'horreurs. Ceux d'avant l'expansion spatiale du consortium. Ceux écrit par des personnes et non générés procéduralement. Ce genre de vieillerie géologique. C'est en lien avec un truc étrange qu'il a reçu, des transcriptions de discussions, des enregistrements audio déformés, des photos floutées, des vidéos brutes et tout un tas d'autres médias. le tout accompagnant ce qui ressemble à un journal intime, ainsi qu'un long message, un peu confus.
Il en a parlé avec Sarah, qui a mis Audrey sur le coup. Récupérer tous les segments de données a été bien plus complexe qu'il ne l'avait supposé au début. Quelque chose à voir avec la pagination des données s'il en comprend la jargon technique qu'utilise Audrey pour creuser dans ce genre de pile de données. Le plus long a été de passer par un relais de vocas, parce que tout transite par l'un de ces serveurs. Mais normalement, ceux-ci ne sont pas vraiment configurés pour envoyer autant de contenus vers des comptes de l'infosphère publique, Kit le sait bien, ça a été une de ses plus grande frustrations de voca.
Au bout de plusieurs heures, ou journées, Audrey a finit par les retrouver avant de disparaître avec Sarah, laissant à Kit une archive complète, indexée et utilisable, stockée dans la mémoire supplémentaire d'un vieux drone caméra. Laissant à ses partenaires du temps, il a commencé à parcourir les données avant de comprendre qu'il avait à faire à un ghost de brassard. Une copie binaire du contenu archivé dans un des systèmes personnels de voca. Une image entière, exhaustive, contenant même les étiquettes laissées par le système de fichier. Du moins, d'après la notice d'accompagnement que lui a laissé Audrey.
Ce n'est pas quelque chose qui est censé arriver. Il sait bien qu'il n'y a jamais vraiment moyen d'empêcher la copie de donnée, mais le principe des brassards de vocas est, justement, d'éviter de pouvoir publier tout ce qui se passe dans la vie des vocas en ligne. De leur laisser la possibilité de faire des erreurs qui ne leur seront pas reprochées dix ans plus tard. De pouvoir expérimenter des formes de socialisation et de sexualité, à l'abri des jugements que peuvent formuler les instances publiques de l'infosphère.
Mais ce n'est un problème que si la technique se répand à grande échelle. Au fur et à mesure de son exploration des données, il a compris qu'il avait sous les yeux un compte rendu détaillé, presque heure par heure, de ce qu'il se passe dans ce mouvement qui agite l'infosphère en ce moment. Beaucoup de choses sont anonymisés à cause des algos du brassard, mais un journal accompagne et détaille chaque élément, permettant de recréer le contexte. Les discours tenus par ce qui est définitivement leur chef, les tensions entre les membres, l'orchestration de campagne mémétique contre l'infosphère. Mais aussi les discussions privées, augmentées des notes et questionnement de la vocas qui semble questionner de plus en plus le bien fondé de l'action du groupe. Et puis la lettre qui accompagne le tout, qui lui est destiné. Qui mêle obsession pour lui et Sarah, appel à l'aide et volonté de s'affirmer, de sortir de ce mouvement.
Lorsqu'il en parle à Sarah, alors qu'elle le retrouve chez Li, accompagnée d'Audrey, elle a eu un sourire un peu étrange.
"Tu veux être une idol, pour aider les autres ? Ben voilà, tu as une occasion parfaite pour ça."  Lui a-t-elle alors dit, s'attaquant à son petit déjeuner. Étrangement, Kit n'avais alors pas encore réalisé les choses sous cet angle. Il lui a fallu quelques instants pour réaliser et décidé d'aller voir sur place ce qu'il en était. Il doit être sûr de lui avant de faire quelque chose. Avec l'infosphère, retrouver le Monster Bash n'a pas été difficile. Il n'y a plus tant de salles d'arcade sur RAFT, et seule l'une d'entre elle utilise autant de peinture phosphorescente comme décor.
Audrey les a accompagné une partie du trajet, mais a préféré laissé à Kit et Sarah aller voir à l'intérieur. Elle préfère réparer les machines aux gens, ça a a toujours été un peu le cas. Ielles se retrouvent donc tous les deux devant le Monster Bash. 
Et l'ambiance du lieu ne les rassure pas vraiment. Les nuances verdâtres de la phosphorescence n'aident pas, mais il y a quelque chose dans l'humeur ambiante qui titille l'empathie de Kit, qui lui communique une forme de désespoir et d'abattement. Il n'y a personne dans l'espace commun. Même le Xenon Girl a toujours une dou eux personnes trop habituée au lieu qui occupe le zones collectives. Mais là, il n'y a personne de vivant. Personne pour les accueillir, personne pour essayer de briser l'ennui qui suinte des cloisons délimitant le lieu. Kit est surpris de la conscience aïgue et soudaine qu'il a d'être enfermé, comme si les squelettes formant une danse macabre rococo sur les murs se rapprochent de lui.
La seule indication qu'il y a quelqu'un de vivant dans l'espace sont les beats étouffés d'un album de Staxy, venant du fond de l'allée de bornes d'arcades, et qui couvre à peine les bruits irréguliers d'un système de filtration en fin de vie. Audrey a bien fiat de rester dehors, elle aurait probablement mal vécu le fait d'entendre les cliquetis des ventilateurs, signes d'un manque flagrant de maintenance. Kit veut partir d'ici, trop de choses ne vont pas. Mais la voix d'alto de Staxy, égrenant les paroles de _Über Pop_ le convainc de rester, d'aller chercher la source de musique, de vie, au milieu de l'ambiance pesante.
"Hey !" lance Kit, d'une voix enrouée par l'angoisse. Il s'éclaircit la gorge avant de réitérer son appel d'une voix plus confiante. Suffisament confiante pour se redonner un peu d'assurance du moins, et laisser le sentiment d'oppression se dissiper. Il se dirige vers la seule borne d'arcade allumée, sur laquelle une voca est en train de dérouler automatiquement les combinaisons de touches lui permettant de traverser les aventures pixelisées qu'elle parcoure à peine, manifestement perdue dans un ennui que la machine qu'elle manipule ne parviens pas à vaincre. Sans enelver les mains du contrôleur encastré dans la console, elle décroche le regard du verre polarisé servant d'écran de jeu, cherchant du regard d'où viens la voix qui la sort de son apathie.
Elle replonge rapidement les yeux dans sa partie, mais Kit sent bien que son attitude à changé. Elle essaye d'être imperturbable, absorbée par ce qu'il se passe sur les quelques centimètres carré d'écran en face d'elle, mais il sait qu'elle l'a reconnu. Et qu'elle aurais préféré qu'il ne soit pas là. Il avance vers elle, Sarah le suivant dans son ombre. Puis, essayant de se rappeler les configurations de touche de Bang Bang, il s'approprie la place de second joueur et s'invite dans la partie.
"C'est toi qui nous as envoyé ce message, avec toutes les pièces jointes?" demande-t-il après quelques instants, essayant de compter sur sa mémoire musculaire pour essayer de suivre le rythme effréné de sa partenaire. Il n'a jamais passé beaucoup de temps sur les bornes d'arcade, mais il sait reconnaître quelqu'un qui y a passé trop de temps. Il n'a aucune chance.
Il meurt. Un des mécha-chien rebelle mastique la jambe de son avatar, avant de voler en éclat sous les impulsions des séquences de hacking de sa coéquipière. Qui n'as toujours pas décroché un mot.
Pause.
"T'es vraiment pas bon à ça hein ?" lui dit la voca, essayant d'esquisser un sourire pour essayer de fendre son armure de nervosité. Kit ne peut qu'acquiescer.
"Mais ouais. C'est moi," reprend-elle. "Vous n'auriez pas du venir ici. Vous allez me griller."
"Qui va te griller ?" demande Sarah, pointant la salle désertée derrière elles.
Reprise
Le jeu reprend, ainsi que la séquence d'infiltration de la station minière de Luna, rythmée par le pont énervé de Über Pop. Quelque chose dans la progression de la voca dans le niveau indique une charge cognitive. Ses mouvements sont plus saccadés, ses attaques moins précises, légèrement en décalage avec le rythme.
Pause.
"Giuse-P. Bay-Lee. Ou n'importe qui d'entre eux qui viendrait au Monster Bash pendant que vous êtes là tous les deux. Vous avez tout ce qu'il faut pour mettre fin à tout ça maintenant. Pourquoi vous êtes venus ?"
Reprise.
"Je veux juste savoir si tu es en sécurité ici. Si tu vas bien," demande Kit.
"Ça ira. Mais faites quelque chose, rapidement," répond elle, exécutant les manœuvres complexe lui permettant de passer sous la vigilance des briseurs de grèves, et de rentrer dans le club dans lequel se produisent les Cuddle the Urshins.
Mais son intonation de voix, la façon qu'elle a de se tenir extrêmement raide et à distance de contact physique de Kit, tout dans son attitude lui dit qu'elle ne va pas bien. Elle est persuadée qu'elle peut tenir le coup, et Kit n'as pas d'autre choix que d'accepter cette réponse. Il ne peut pas agir sans son consentement. Pas pour le moment du moins.
"D'accord, d'accord," dit Kit, se retrouvant recruté comme gogo dancer par les gestionnaires de la mine, le forçant à un game over et à dépenser un autre crédit pour rejoindre la partie. Heureusement que les consoles ne comptent pas vraiment les crédits, car à ce rythme, Kit devrait abandonner.
"On ne s'est pas présenté au fait. Je suis Kit et elle c'est…"
Pause.
"Sarah, je sais. C'est moi qui vous ai contacté. Tu te rappelles ? Moi c'est Yas," un léger sourire se dessine sur les lèvres de la gamine alors qu'elle reprend la partie. Elles entendent un peu de bruit venant de la zone commune. Quelqu'un descend de la mezzanine qui sert normalement de salon privé. Le sourire de la voca s'estompe instantanément.
"Vraiment les gars, ça ira. Tardez pas à agir, mais ça ira. je tiendrais le coup. Mais il faut que vous partiez. Maintenant. Avant qu'il ne nous trouve en train de parler."
Kit lâche le contrôleur, alors que Sarah lui attrape la main et l'emmène rapidement vers les coursives, hors du Monster Bash, vers une lumière plus douce que celle des bornes d'arcade. Ielles prennent leur souffle en arrivant sous la lumière rouge de la ronde de nuit. Kit sent l'inconfort du confinement totalement se dissiper.
"Bon, et maintenant, on fais quoi ?" demande-t-il à Sarah.
"C'est toi l'idol, non ? Tu es censé faire quoi dans ces cas là ?"
Il ne sait pas vraiment. Mais il a une idée de qui peut l'aider à répondre à cette question.