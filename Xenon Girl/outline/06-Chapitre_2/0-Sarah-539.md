title:          Sarah
ID:             539
type:           md
summarySentence:Sarah détecte une trajectoire étrange
POV:            6
notes:          {C:6:Sarah}
                {W:27:Contrôle}
                {W:29:baleiniers}
                {W:31:ferme de silicate}
                {W:33:transpondeurs}
                {W:34:4-Vesta}
                {C:7:Banji}
                {W:36:Chernobyl}
                {W:35:ampoules nucléaires}
                {W:37:anneau zénithal}
                {W:38:marine marchande Mexicaine}
                {W:39:expansion terrienne}
label:          4
status:         5
compile:        2
setGoal:        1500
charCount:      7879


Deux semaines. Ça fait à peine deux semaines que Sarah s'est trouvée une place au sein des équipes de Contrôle, et elle a déjà un cas foireux.
Contrôle est le collectif qui essaye d'éviter les collisions dans l'espace autour de 4-Vesta, et de limiter les dépenses inutiles de carburants en proposant des solutions de trajectoires plus économes. Et leur seul outil pour faire face a ça, c'est leur voix. Contrôle n'as pas de myen d'imposer physiquement ses recommandations, ni de dérouter quelqu'un. Les voix du Contrôle doivent donc composer avec tout un ensemble de personnalités plus ou moins conflictuelles.
Mais les voix sont aussi souvent le premier contact non différé avec les pilotes du pipeline, qu'ils passent simplement dans le voisinage avec leur porte container, ou qu'ils aient pour destination Rien à foutre de ta thune. C'est une des raisons pour lesquelles Sarah a bosser une bonne partie de ses vocas pour devenir une voix. Ça, et c'est le seul endroit où l'on peut faire sérieusement de la mécanique orbitale.
Engoncée dans un fauteuil en mousse à mémoire de forme, faisant face aux nombreux entoptique qu'elle utilise pour se tenir au courant des vélocités de chaque objet dans le voisinage proche de RAFT, elle essaye de déterminer ce qu'est cette objet qui l'inquiète.
Contrôle lui a associé une référence — 2340 RN — en attendant de savoir ce dont il s'agit. Cette référence transitoire signifie que cet objet n'a pas été observé par un autre télescope, et qu'il est dépourvu de transpondeur. Son albedo bas empêche toute identification précise par les télescopes optiques disponible. Mais ce qui perturbe Sarah, et les autres voix de Contrôle si elle fait confiance aux commentaires associés à la trajectoire, c'est que cet objet vient directement de Jupiter.
Pas de son voisinage, ni de l'une des lunes sur lesquelles il y a quelques postes avancés. De Jupiter *elle-même*. L'écho de l'objet est apparu sur le télescope d'un des autres habitats avec lesquels RAFT partage ses informations de trajectoires. Personne ne peut scanner l'ensemble de l'espace, et personne ne veut être surpris un peu tard par un astéroïde qui aurais une trajectoire menaçante pour son habitat.
Le partage de données des objets célestes est donc un des rares sujets sur lequel l'ensemble des acteurs présents dans le système solaire est d'accord, ce qui permet à tout le monde de bénéficier des yeux et des oreilles des autres, sans avoir besoin de développer un télescope global.
Mais donc, cet objet s'est échappé de l'atmosphère haute de Jupiter, et est sur une trajectoire d'interception avec 4-Vesta. Elle refait encore une fois les calculs, et l'objet devrait les éviter de peu, mais elle n'aime pas que quelque chose d'aussi gros passe si près d'eux. Et il devrait passer dans cinq jours avec une vitesse relative de … six mille deux cent mètres par seconde, avant de repartir orbiter quelque part.
Les abaques de trajectoires s'emballent soudainement. L'objet dévie de sa trajectoire calculée et sa vitesse commence à réduire. Sarah récupère un des télescope installée sur l'axe central de la station et l'oriente vers l'objet. Balayage du spectre complet. La silhouette de l'objet est encore un peu difficile à percevoir, mais ce qu'elle cherche ne se situe pas dans le spectre lumineux classique.
Elle récupère une signature thermique et électromagnétique. Chaque moteur est légèrement différent des autres, même s'ils sortent des même chaînes d'assemblage. Et cela se reflète dans l'analyse de leur éjectas. Ce qui permettrait à Sarah de savoir au moins à quel type de vaisseau ils ont à faire.
Une fois les données échantillonnées et converties en un format plus simple à comparer, elle lance les résultats dans la base de donnée de Contrôle, en espérant que ce soit un modèle connus. Car, contrairement aux trajectoires d'objets céleste, le Consortium préfère garder ses bases de signature pour lui.
Mais si c'était un vaisseau du consortium, ils auraient trouver au moins une trace d'un transpondeur dans le spectre électromagnétique, et elle aurait pu prévenir… quelqu'un, probablement. Elle n'est pas vraiment sûre de savoir ce qu'il faut faire si un vaisseau du consortium se pointe dans leur secteur.
Mais ici, pas de transpondeurs. Et, rien dans la base de donnée qui ait une signature similaire à ce qu'elle voit.
*Non…* S'empêche-t-elle de penser. *Ce ne peut pas être un cas de premier contact.* Elle sait qu'il y a un protocole en place pour un cas de premier contact avec une intelligence extra-humaine, mais elle a appris aussi que ce n'est **jamais** un cas de premier contact. Il y a forcément une autre explication.
Elle parcoure les forums d'ingénierie, à la recherche d'une spécialiste des moteurs. Elle finit par y poster la signature de l'objet, en espérant que quelqu'un lui donne une réponse rapide. Quelque soit ce qui leur fonce dessus, ils ont cinq jours pour réagir. Et s'il faut aller aborder et dévier l'objet, ça va être limite.
Quelques messages demandant plus d'informations qu'elle ne peut pas fournir tombe rapidement, avant que Banji ne réponde.
"On voit bien cinq points de combustions, mais là où je pense que mes camarades se plantent, c'est que nous avons affaire à des ampoules nucléaires.
"Des moteurs nucléaires en circuit ouvert et à cœur d'uranium 235 fondu. C'est la seule explication que je parvient à trouver pour justifier l'ensemble de ces signatures."
"Tu veux dire que quelqu'un a réussi vaporise de l'uranium 235 et s'en sert comme carburant ?" répond Sarah un peu incrédule.
Elle sait que, avant de passer à la fusion nucléaire et aux propulsions électromagnétique bien plus en vogue en ce moment, il y a eu une brève tentative d'utiliser la fission comme moyen de conquérir le vide, mais elle supposait, jusau'à quelques instants, que ces moteurs étaient tous tombés en désuétude.
"Oui. Et vu le nombre d'ampoule, ça ne peut être qu'un classe Chernobyl. Je ne pensais pas qu'ils étaient encore en état de fonctionnement, mais quelqu'un a été assez têtu pour en maintenir un.
"Tabula Rasa doit avoir les spécifications plus précise si tu veux. Mais c'est probablement un cargo lourd. Plus lourd que ce qu'utilise le pipeline. Tu dis qu'il sera dans le coin bientôt ?"
"Ouais, cinq jours. Peut-être plus si l'équipage continue de freiner comme ça."
Sarah n'a pas besoin de spécifications plus précise, l'introduction accolée aux fichiers de Tabula Rasa lui suffise. C'est un bus logistique, propulsé par des ampoules nucléaires, mais le consortium les a décomissioné il y a plus de vingt ans.
Et ce monstre freine à 1G. S'il maintient son freinage, la vélocité du cargo devrait l'amener sur l'orbite de RAFT, à une vitesse relative nulle ou presque. S'il maintient son freinage.
Et toujours pas de signal radio, ou de transpondeur visible. Mais vu le cône de plasma électromagnétique émis par le freinage, il ne serait de toutes façon pas possible d'envoyer un message vers le vaisseau.
Reste à attendre une fenêtre de communication. Personne ne peut soutenir une semaine de freinage à 1G. Il vont devoir faire des pauses.
En attendant, Sarah a besoin de se dégourdir les jambes et de se changer les idées. Alors elle déconnecte ses entoptiques, qui se réduisent à de simples icônes présente dans son champ de vision, et elle sort du café dans lequel elle s'installe pour travailler.
Ses flux audios basculent sur les fréquences Ci-Bi utilisées par les nomades de maintenance, et presque inconsciemment, elle glisse à travers les longueurs d'onde, traversant le spectre des roll-calls et des communications techniques pour s'arrêter sur 27.015, la radio collective des nomades.
Et aujourd'hui, c'est Audrey qui anime le canal. Ce qui permet à Sarah d'entendre sa voix, un peu écrêtée par les codecs audio à échantillonnement bas alors qu'elle passe les appels des auditeurices et les morceaux de sa playlist, occupant le canal de divertissement.