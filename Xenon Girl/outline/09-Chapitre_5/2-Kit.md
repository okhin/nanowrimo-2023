title:          Kit
ID:             228
type:           md
summaryFull:    Kat se mets à suivre Coda, pour essayer de l’approcher et parler avec elle de son boulot d’Idol.
POV:            13
notes:          {C:6:Sarah}
                {C:13:Kit}
                {W:83:karaoké}
                {W:6:Naked Broccoli}
                {W:69:groupes de travails}
                {W:14:Idols}
                {W:4:bauhaus}
                {W:24:Syndicat}
                {C:2:Coelia}
                {C:3:Gab}
                {W:27:Contrôle}
                {W:25:Vocas}
                {W:78:agent}
                {W:62:Compte social}
                {W:97:volontaires de la modération}
                {C:15:Paz-Tun}
                {W:10:Consortium}
                {W:7:Sc.nd.l}
                {W:89:beignets}
                {W:50:coursiers de maintenance}
                {P:10:Kit}
label:          4
status:         5
compile:        2
setGoal:        1500
charCount:      12381


La ronde blanche est passée. Puis la rouge. Kit n'as pas envie de se lever, mais il va falloir qu'il mange quelque chose. Sarah est encore endormie dans le lit qu'ielles ont partagées ces dernières heures. Dans la pénombre rouge qui l'entoure, il distingue les ombres des quelques meubles et objets occupant la vingtaine de mètre cubes que constitue le cube de Sarah.
Le léger bruit des systèmes de filtration d'air et des pompes à chaleur est à peine audible, mais Kit sait que, si l'un ou l'autre venait à disparaître, il le percevrait instantanément. Sa vie, est celle de chaque personne à bord de RAFT, en dépend.
Il se lève et s'étire rapidement. Dans son champ de vision, une icône attend sagement son activation, mais il n'a pas encore envie de plonger si vite dans le bain des interaction sociales. Pas sans avoir mangé quelque chose du moins. Ni sans avoir reparlé de leur conflit, de la façon dont Sarah évite le sujet de leur relation. À moins que ce ne soit juste lui qui se fasse peur tout seul. Dans tous les cas, il est temps pour lui de se lever.
Sarah se retourne dans le lit encastré dans la cloison, surplombés de rangements encore vide. Sarah passe beaucoup de temps à Contrôle, au centre de la station, et elle n'a pas vraiment eu le temps d'investir proprement son cube. Mais Kit sait qu'elle n'aura jamais vraiment le temps. À part quelques fringues suspendues dans un placard mural et un ensemble d'outil nécessaire à maintenir les systèmes de survie, le module de vie de Sarah reste très vide. Il sait qu'elle dirait que c'est fonctionnel, pratique, et qu'elle ne voit pas l'intérêt de s'encombrer de trop de choses.
Il sait aussi que son kit d'outil a été amélioré par Audrey, et que donc elle a bien plus que le nécessaire. Il doit être possible de démonter une navette avec ces outils "strictement nécessaire". Il sait aussi que la moitié, au moins, de la garde robe de Sarah est composé de fringues qu'ielles ont choisis ensemble. Plus pour lui faire plaisir à lui qu'à elle, d'ailleurs.
Il trouve enfin ce qu'il cherche. La radio qu'Audrey leur à donné. Le tuner électromagnétique est pré-réglé sur Dispatch. Il triture les potentiomètres qui orne la façade de l'appareil, scannant à travers les différentes fréquences d'appel des nomades, et finit par trouver une fréquence libre du blabla et des discussions un peu incompréhensible pour les gens qui ne parlent pas le techie. Il lui faut un peu de temps pour finir de filtrer les parasites électromagnétique, avant de reconnaître la voix de Staxy qui annonce le prochain morceau.
"Hey," dit Sarah derrière lui, luttant un peu contre le sommeil et essayant de se réveiller alors que les harmoniques de *Fix your filters!* emplissent doucement l'espace sonore.
Elle l'embrasse dans la nuque rapidement, avant de jaillir hors de son lit. Avant qu'il n'ait pu comprendre ce qu'il se passe, Sarah est déjà en train d'enfiler une combinaison, prise au hasard dans sa penderie.
"J'ai les crocs, dit-elle remontant la fermeture éclair, on va chez Li ?" Kit sait que ce n'est pas vraiment une question. Mais Li ou ailleurs, ça lui va très bien. Il attrape sa tunique qu'ielles ont jeté au sol la veille, et, après avoir retrouvé les uns après les autres les différents éléments de sa tenue, finit par s'habiller, alors que Sarah l'attend impatiemment.
Chez Li est un étal, installé en bordure du bazar du bras deux, à quelques minutes à peine de chez Sarah. L'étal ne paye pas de mine mais ce qui lui assure une affluence régulière, du moins quand Li se décide à ouvrir, est la qualité de ses pâtisseries et viennoiseries à extrudées à base de mycélium.
Kit et Sarah se sont installées derrière un assortiment de beignets qu'elles dégustent assise sur l'une des tables qui occupe l'espace entre les étals du bazar. Les échoppes, composés de containers logistiques colorés, accumulés les uns sur les autres, et grimpant la coque de la station, abrite l'une des places d'échanges de RAFT. L'un des bazar installés à la jonction de l'anneau avec les bras qui le connecte à la partie centrale et qui permet le transfert des personnes, de l'énergie, des marchandises et de toutes l'infrastructure de la station. L'un des lieux dans lequel l'inventivité est déployée dans le réemploi de ce qui est usé, abîmé, passé de mode et permet d'alimenter les modes culturelles foisonnantes de RAFT.
Depuis que Sarah est réveillée, elle n'a pas vraiment parlé à Kit. Leur dernière conversation n'était pas simple, et s'est terminé quand elle a décidé qu'elle préférait baiser que de poursuivre la conversation. Une variante de celles qu'ielles ont régulièrement en ce moment, et qui a prend beaucoup trop de place dans la pyschée de Kit.
"On peut parler deux minutes ?" demande-t-il à Sarah, après avoir terminé sa part de pancakes.
"De quoi tu veux parler ?", répond-elle, interrompant son œuvre d'engloutissement de pâtisserie.
"De nous. J'ai l'impression que tu évites le sujet. J'ai besoin de savoir où on en est. Surtout si j'envisage de rejoindre le bauhaus." Il se concentre sur sa respiration pour ne pas laisser ses pires angoisses s'exprimer. Angoisse dont il ne connaît pas la cause, ni l'origine. Ça fait quelques années que lui et Sarah se connaissent maintenant, il devrait être capable de lui faire confiance.
"J'ai besoin de savoir, " finit-il par ajouter.
"Je sais. Et je n'ai pas envie d'en parler" répond Sarah en soupirant, repoussant l'assiette de pâtisseries juste hors de sa portée. "Mais, reprend-elle, tu ne me lâcheras pas tant que je ne t'aurais pas répondu. Alors parlons-en. Mais j'aime pas ça."
Elle ferme les yeux, et cherche ses mots. Kit sait qu'il doit attendre, que le silence est le meilleur catalyseur dans une conversation, même si c'est frustrant. Il résiste à la tentation d'ouvrir les notifications qui ont commencés à peupler le bord de son champ de vision.
"C'est complètement égoïste, finis par dire Sarah, mais je ne veux pas que tu soit une idol en fait. Je ne veux pas que tu rentre dans un bauhaus, et je ne veux pas que tu deviennes quelqu'un que je devrais partager avec un public de fan."
Au moins, les choses sont dites se dit Kit. Il a envie de répondre, mais Sarah est encore en tension, elle ne lève pas les yeux et fixe  ses genoux à elle, refermant son espace personnel à une sphère qui se tiens entre ses cuisses et sa poitrine.
"Et en même temps, j'ai pas envie de t'empêcher de faire quelque chose dont tu as envie, de faire ce pour quoi tu as travaillé ces dix dernières années. Et je sais que je devrait pas être jalouse d'elleux, des fans, des autres. Mais j'ai quand même envie qu'on est des espaces rien qu'à nous, sans public, sans notoriété, sans syndicat, sans script… C'est juste… Nul. J'aime pas me sentir comme ça. Mais j'arrive pas à passer au-delà."
Sarah tremble sur sa chaise.
"Je peux te prendre dans mes bras ?" demande Kit, doucement. Sarah hoche de la tête en se penchant vers lui, se pressant contre sa poitrine et laissant couler ses larmes. Il la sert contre lui et attend. Il n'a pas forcément grand chose à lui dire qui la rassurerait là maintenant tout de suite, alors il attend qu'elle termine de pleurer et il l'accompagne en l'englobant de ses bras élancés.
Au bout d'un moment, il sent Sarah se détendre, et se redresser. Il la laisse prendre l'espace dont elle a besoin.
"Désolée," dit-elle. Comme si elle avait besoin de s'excuser. "Tu dois penser que…"
"Tu ne sais pas ce que je pense, répond Kit. Écoutes, j'entends tes peurs et tes craintes. Mais c'est vraiment quelque chose que j'ai besoin de faire." Il marque une pause. "Non, je n'ai pas *besoin* d'être une idol. Mais c'est quelque chose dont je rêve depuis petit. Et je n'aime pas te voir comme ça, dans cet état là."
"S'il te plaît, n'arrêtes pas juste pour moi…" dit Sarah.
"Je t'aime, tu le sais ? Toi, Audrey et Gab aussi, dit Kit, gardant son exaspération sous contrôle, mais tu ne me rends pas forcément la tâche simple. Je peux finir ce que j'allais dire ?"
Sarah opine de la tête, attrapant un pancake tiède et commençant à l'ingurgiter, pour s'empêcher de l'interrompre.
"Pardon," dit-elle, la bouche à moitié pleine de pate de mycélium.
"Donc… Je ne suis pas pressé, tu sais ? Et idol, c'est pas forcément quelque chose qui se planifie. Même avec le Syndicat, ou avec un label coopératif comme Cuddle the Urshins. Donc, avant que je perce, et il peut se passer un peu de temps, je suis là, et on est encore juste entre nous. Et je n'ai pas envie de t'échanger, toi ou Audrey ni même Gab, contre une notoriété. D'accord?"
Sans vraiment attendre qu'elle ne réponde, Kit ramène l'assiette de pancake devant eux et commence à en triturer un, sans forcément y faire attention.
"Et en attendant, on en parle d'accord ? Et si ça arrive, et bien on essayera de trouver une façon de continuer à avoir une relation."
Il n'a pas forcément grand chose à ajouter sur le sujet. Il est même légèrement surpris du fait qu'il ait eu autant à dire sur ce sujet qui lui occupe quand même pas mal d'espace mental.
Ils continuent leur dégustation de pâtisserie en silence, mais Sarah s'est rapproché de lui. Il parcoure maintenant les notifications que son oracle lui a sélectionné. Un petit agent qu'il se bricole, en collant ensemble des facettes de recherche et des boucles algorithmiques dans le but d'essayer de comprendre un peu ce qu'il se passe sur les nombreuses plateforme sociales.
Beaucoup de discussions sur les groupes de travail et leurs dysfonctionnement sont automatiquement archivées. Il n'a pas le temps de tout lire, et il ne peut pas y faire grand chose. L'instance du Naked Brocoli est relativement calme en comparaison. Et, un peu sotie de nulle part, en tout cas ne venant pas des canaux de communications habituels de Coelia, ni même du syndicat, une alerte de contenu surgit devant ses yeux.
Ça fait quelques jours que l'idol est assez discrète sur les plateformes sociales, et il a donc mis en place une routine pour essayer de la suivre, elle est forcément en train de travailler sur un projet. Il voit ça un peu comme un exercice, une manière pour lui de continuer de s'habituer à la navigation de contenu dans les instances prolifiques de la station. Quelque chose auquel ses basiques et son parcours de vocas ne l'ont pas vraiment préparé.
Une nouvelle publication, mais manifestement par des canaux non institutionnels. Un genre de rétrospective de ses couvertures par un certain Paz-Tun. Coelia se superpose à la foule du bazar, dans toute la gloire post-porn néo-classique de ses premiers albums. De l'époque où l'idol faisait encore parti des V.S.T.L, d'avant que le Syndicat ne la libère du contrôle du Consortium.
Quelque chose d'inattendu, de très gravure, suggestif. Quelque chose qu'il imagine plus destiné à finir affiché dans les vestiaires d'atelier d'une usine d'assemblage orbitale du Consortium, que quelque chose de destiné au public de RAFT.
Paz-Tun est un des habitué du SCNDL d'après son profil. Mais, toujours d'après son profil, il semble avoir quitté la station récemment, probablement l'un de ces chasseurs de tendances qui vont et viennent entre les stations et le consortium pour essayer de se faire quelques crédits sur le dos des créateurs et créatrices locaux.
"T'es parti oú?" lui demande Sarah, le tirant de ses réflexions.
"Coelia. Elle a un comportement étrange en ce moment. Tiens, regarde ce que je viens de trouver," il partage avec elle le lien vers les posters. "Je sais pas si ça a à voir avec le bordel des groupes de travail, où si il y a autre chose derrière."
"J'aime beaucoup," répond Sarah distraitement, d'une voix pleine d'excitation sexuelle, confirmant à Kit, s'il en avait besoin, la démographie ciblée par cette publication. "Mais ça tranche un peu avec ce que fait le Syndicat d'habitude, non ?"
"Ouais… Je vais essayer de la croiser. De trouver le compte de son interprète et de voir ce qu'il se passe."
"T'es pas censé faire ça, tu sais? Retrouver les interprètes des icônes."
Kit ne répond pas. Sarah a raison, il ne devrait pas faire ça. D'autant qu'il n'aimerait pas que des fans le fassent pour lui non plus. Mais quelque chose le titille dans l'activité récente de Coelia, et sa curiosité est purement professionnelle. Et maintenant, il pense savoir comment remonter la trace de Coelia. Il doit juste passer au Sc.nd.l et parler avec les habituées du lieu, déterminer s'ielles peuvent l'aider à identifier quelqu'un. Il a déjà une date approximative.