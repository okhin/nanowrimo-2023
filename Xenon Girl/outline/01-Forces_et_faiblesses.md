title:          Forces et faiblesses
ID:             210
type:           md
label:          2
compile:        0
charCount:      1219


Le setting marche bien, l'exploration de la station de son infrastructure. Simena a un arc intéressant, des motivations claires, à peu près.
L'intrigue n'est pas trop linéaire et le ping pong de points de vue Coda / Simena permet d'explorer les différents aspects des infrastructures (technique et culturelle respectivement).
L'utilisation de la culture comme système de protection contre les agressions physique, et d'expressions de conflits ets une bonne idée (mais il en faut plus)

Le personnage de Coda est bancal. Pas assez de focus sur ce que sont les Idols ou l'impact direct des CARA0K, il faut le montrer plus et en parler plus tôt.
Les coursiers de maintenances peuvent être un peu plus élaboré, et peut-être que Vick etSVal pourraient n'être qu'une personne, ou une personne et un agent numérique (mais est-ce que je veux des IA ?).
La progression émotionnelle des relations est pas forcément ouf, parfois un peu brutale. Notamment Coda/Choelia et Xicha/Simena.
La relation Coda/Simena est pas assez développée. Mais peut-être qu'elle n'a pas à l'être, et que c'est leurs cercles sociaux respectifs qui les font se fréquenter (Xicha - Vick - Choelia, par ex).
Faire attention à la chronologie et au temps qui passe.