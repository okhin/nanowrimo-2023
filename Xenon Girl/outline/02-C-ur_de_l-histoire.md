title:          Cœur de l'histoire
ID:             209
type:           md
label:          2
compile:        0
charCount:      206


Le cœur de l'histoire est l'exploration des imperfections sociales et des angles morts de groupes autonomes, et de voir comment leurs mécanismes de résistance se mettent parfois en travers de leurs valeurs.