title:          Coda
ID:             256
type:           md
summaryFull:    Coda et Simena sont amies, et travaillent 
                Coda se consacre au développement d'un réseau de partage d'archive culturelles distribuées.
                
POV:            1
notes:          {C:1:Coda-6}
                {C:0:Simena Von Guyen the Third}
                {P:4:Une nouvelle voix pour Coda} Resolution
                {C:11:Octane}
                {C:18:Alika}
                {W:17:Tabula Rasa}
                {W:127:stockage hors ligne}
                {C:14:Staxy}
                {W:79:communicateur}
                {W:129:système fédératif}
                {W:5:Xenon Girl}
                {C:2:Coelia}
                {C:10:Bey}
                {C:13:Kit}
                {C:5:Mark-V}
                {W:50:coursiers de maintenance}
label:          4
status:         3
compile:        2
setGoal:        1500
charCount:      3663


"Ça ne peut pas être juste une solution technique," oppose Octane à Coda à chaque fois qu'elles échange sur son idée d'archives culturelles. Cette réunion ne fais pas exception. Elle n'a pas nécessairement tort, mais Coda à l'impression que les freins mis par Octane au déploiement plus large de ce protocole cache d'autres choses.
Leur projet avance bien pour autant. Simena, avec l'aide d'Alika,  a intégré des bouts de protocoles que Tabula Rasa utilise pour ses sauvegardes. Son Kang et le système d'archivage de Staxy, ainsi que la quantité de système de stockage personnels éparpillés à gauche et à droite de la station, sont capables de synchroniser et partager des données entre eux.
Il s'avère que, à l'image de Staxy, un nombre élevé de personnes étaient déjà équipé d'archives personnelles. Plus que ce que ne leur fournissent en général leurs communicateurs. Et sans avoir un protocole de partage et de mise en réseau, il existe déjà un réseau informel d'échange de données.
"Mais ce n'est pas juste une solution technique," répond Coda. "Le but c'est de pouvoir contourner la gestion par syndicalisation des ressources. De fournir, je sais pas, un substrat permettant à tout le monde d'accéder aux éléments culturels importants pour chacune et chacun."
La réunion à lieu au Xenon Girl. Cela fait quelques jours maintenant qu'elle et d'autres viennent échanger sur ce réseau d'archive, amenant souvent, aussi, leurs propre éléments matériels et culturels. Simena prend en charge la partie technique, et les guide rapidement dans la configuration des protocoles qui font tenir le tout ensemble.
Et Coelia fait des apparences régulières. En attendant que Bey et Kit aient créées de nouveaux morceaux, il réinterprète certains classiques. Et chacune de ses apparences amène toujours plus de monde au Xenon Girl, d'autant que Coda insiste pour que personne n'enregistre quoi que ce soit qui se passe au Xenon Girl.
"Je sais," répond Octane. "Mais j'aimerai que… je sais pas… qu'on trouve un moyen de garantir qu'il ne serait pas possible à terme que quelqu'un centralise la gestion du protocole."
"On pourrait envisager de mettre des pénalités sur l'asymétrie de ressources?" suggère Ryu, avant que Coda ne puisse répondre. Il joue un peu au juge de paix avec Alika, pour arbitrer entre la prudence, parfois excessive de la coursière de maintenance, et l'enthousiasme parfois un peu infondée de Coda. "Mais toute limite technique peut être contournée à terme."
"Et une charte ou un manifeste peut aussi être ignorée", ajoute Alika.
"Certes, mais ça ne coûterait rien d'expliciter nos intentions, non?" soulève Staxy, qui sort de sa posture habituelle d'observatrice. "D'autant qu'on est toutes à peu près au clair sur pourquoi on veut faire les choses. On devrait pouvoir se mettre d'accord rapidement."
"D'accord, si tout le monde pense que c'est une bonne idée, faisons ça. Moi j'ai besoin d'air," conclue Coda, s'extrayant de la table de discussion.
Staxy la rattrape quelques instants après, alors qu'elle quitte le Xenon Girl.
"Tout va bien?" demande-t-elle.
"Je suis juste fatiguée," répond Coda.
"T'es certaine qu'il n'y a pas autre chose?"
Son silence suffit à répondre à Staxy que oui, il y a autre chose. Des émotions qu'elle doit apprendre à comprendre. De la frustration de devoir discuter de tout ce qu'elle fait. De la panique vis à vis du succès du Xenon Girl auquel elle ne s'attendait pas. De la joie de voir les gens venir ici, ré-établir les connexions sociales qui ont été brisées par la désyndicalisation. Mais elle n'arrive pas à en parler. Pas pour le moment.
"Ça va aller," finit-elle par répondre alors que Staxy la serre dans ses bras. "Promis."