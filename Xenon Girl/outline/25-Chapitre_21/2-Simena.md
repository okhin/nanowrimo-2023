title:          Simena
ID:             2765
type:           md
summaryFull:    sur le projet de Coda
                Simena est intégrée dans un groupe social de la station
POV:            0
notes:          {P:0:Intégration de Simena} Resolution
                {P:5:Coda et Simena} Resolution
                {C:0:Simena Von Guyen the Third}
                {C:1:Coda}
                {W:134:anneau nadiral}
                {C:8:Xicha}
                {W:6:Naked Broccoli}
                {C:17:Yas}
                {W:26:Brassard}
                {W:25:Vocas}
                {C:6:Sarah}
                {C:13:Kit}
                {C:2:Coelia}
                {W:24:Syndicat}
                {W:14:Idols}
                {W:74:Zone de transfert}
                {W:10:Consortium}
                {W:131:infosphère}
                {W:83:karaoké}
                {W:5:Xenon Girl}
                {C:11:Octane}
label:          4
status:         3
compile:        2
setGoal:        1500
charCount:      4671


Les heures passées à parler protocoles et gouvernances sont éreintantes pour Simena. Elle n'a pas l'habitude de discuter autant avant de faire des choses, mais c'est ainsi que les choses se font ici. Ou, plus précisément, comment Coda veut faire les choses.
Et Simena n'arrêtes pas de dire qu'elle va essayer, de donner une chance aux autres. Alors elle joue le jeu et participe autant qu'elle peut à ces réunions de travail. Ce qui lui permet aussi de ne pas passer trop de temps seule dans son atelier de l'anneau fantôme.
Même si elle n'y est que rarement seule maintenant. Xicha aime bien venir l'y retrouver quand cette dernière se décide à prendre une coupure du Naked Brocoli. Yas aussi s'incruste régulièrement, pour venir discuter.
Elle ne porte plus son brassard de voca, mais traîne beaucoup avec Sarah, la copine de Kit, qui a repris l'icône de Coelia. Sarah semble s'être mise en tête de s'assurer qu'elle aille bien. Ce qui rassure Simena. Elle a assez à faire de son côté, sans avoir en plus à devoir s'occuper de Yas.
Des débuts de graffitis ont commencé à apparaître entre les cloisons pressurisées qui délimitent ce qu'elle appelle son atelier. D'après Xicha ce sont juste des vocas qui s'ennuient et explorent l'espace de jeu que constitue cet anneau déserté.
Le lieu devient moins sont atelier, et plus un lieu d'expression collectif. Pas quelque chose de concertés, comme les syndicats d'idols. Mais quelque chose plus proche des fresques de la zone d'attente, ou des arts muraux qui envahissent les cités du consortium. Quelque chose qui lui correspond plus, de plus instinctifs, de moins réfléchit. Une collaboration artistique ad-hoc entre elle et les différentes personnes qui passent ici laisser une marque ou deux dans la structure du lieu.
Elle se refuse toujours à le partager dans l'infosphère, même si Coda insiste. Elle se ferait reconnaître différemment, ça pourrait lui faciliter la vie d'après l'ancienne idol. Mais justement, elle ne veut pas être reconnue, elle l'a suffisamment été et elle essaye encore de s'ajuster au fait que tout le monde la reconnaît, la remarque.
Alors elle garde la tête enfoncée dans sa capuche, et elle garde un profil bas en ligne. De toutes façon, les médias sociaux fédératifs ne sen sont pas encore remis de leur récente crise, et leur réseau de partage d'archive ne permet pas encore de gérer des espaces de discussions publiques. Tout passe par les conversations privées. Ce qui lui convient bien.
"Hey, ça avance comme tu veux?" demande Xicha, débarquant dans son espace.
"Moui… je pense. Difficile à dire," répond-elle, arrêtant la découpeuse à béton, offrant à ses épaule un répit bienvenu.
Elles s'enlancent et s'embrassent, avant de s'attaquer la nourriture que Xicha lui ramène directement des cuisines du Naked Brocoli.
"Ça se passe comment au Naked Brocoli?" demande-t-elle.
"Lentement. Mais les soirées karaokés ont repris. Je suppose que c'est une bonne chose."
Simena sait qu'il y a quelques personnes qui campent encore sur leurs positions au Naked Brocoli, et qui ont encore en travers de la gorge le fait que Xicha ait ignoré leur décision collective. Il doit y en avoir d'autres du même acabit un peu partout, mais elle entends surtout parler d'eux.
En attendant, Simena ne mets plus les pieds au Naked Brocoli que pour rentrer chez Xicha. Qui est aussi chez elle, pour le moment du moins. Elle aimerait récupérer un des cubes au dessus du Xenon Girl, mais pour le moment Octane s'en sert de zone de stockage le temps de reconfigurer le lieu. Elle partage donc tous les soirs le lit de Xicha, ce qui leur convient très bien pour le moment, mais elle aimerait bien avoir un espace rien qu'à elle.
"Tu fais un truc tout à l'heure?" demande Xicha
"Je crois que le gang veut qu'on se retrouve au Xenon Girl. Pour genre boire un verre."
"Le gang. Carrément. Je peux en être ce soir?"
"Mais oui. Et comme ça, je serai sûre que Coda essayera pas de, encore une fois, me demander pourquoi le protocole réseau ne converge pas plus rapidement."
"Elle te lâches pas avec ça?"
"Non, mais elle a raison hein, on devrait pouvoir enrôler plus de machines. Mais ça fait trois jours que Ryu et Alika bloquent dessus. On a besoin d'une pause je pense.
"Et de toutes façon, ça fonctionne quand même. C'est juste un peu lent, c'est tout. Et lent c'est bien aussi, parfois."
Elles terminent leur repas sous le regard de Coelia qui les surplombe de sa stature de béton. Les plis de la tunique commençant à être colonisé par les couleurs tranchés des tags.
Et pendant quelques instants de silence, et de paix, sa main posée sur celle de Xicha, dans les clair obscurs de cet endroit étrange de la station, elle se sent vraiment bien.