title:          Coda
ID:             880
type:           md
summarySentence:Découverte du bauhaus et de :-
summaryFull:    Coda découvre un nouveau memeplexe émergeant dans la station (:-)
POV:            1
notes:          {C:1:Coda-6}{C:10:Bey}{C:2:Coelia}
                {P:4:Une nouvelle voix pour Coda} Plot turn 1
                {W:4:bauhaus}
                {W:93:pancake}
                {W:94:chasseurs de tendance}
                {W:65:scripts}{W:14:Idols}
                {W:24:Syndicat}
                {W:69:groupes de travails}
                {C:11:Octane}
                {W:95:espaces intersticiel}
                {W:96::-}
                {W:72:riot porn pop}
                {W:42:néo-art déco-classique}
                {W:1:mnémorithmes}
                {W:97:volontaires de la modération}
label:          4
status:         3
compile:        2
setGoal:        1500
charCount:      9146


Cela fait dix minutes que Coda est roulée en boule sous la couverture qui l'isole du monde extérieur, définitivement réveillée, et cherchant à regrouper l'énergie nécessaire à sortir de son compartiment pour affronter le reste du monde.
Ses flux sociaux personnels sont calme. Déclencher mentalement des mise à jour ne ramène rien, pas de nouveaux messages, personnes qui ne pense à elle. Rien d'intéressant.
Encore cinq minutes à s'agiter sous la couverture sombre, dans la faible luminosité qui baigne les quelques mètres carrés de volume mis à sa disposition par le syndicat. Elle perçoit à travers les fibres synthétiques les mouvements de lumière créé par les images défilant sur la cloison. Des extraits des images qu'elle a étiquetées d'une manière ou d'une autre au fur et à mesure de ses interactions, la plupart des instances sociales n'archivant pas le contenu, le laissant disparaître avec le temps.
Déjà, parce que cela nécessiterait tellement de capacité de stockage que personne ne veut en assumer l'archivage historique. Ensuite parce qu'une émoire parfaite et exhaustive du moindre échange a été considéré comme une mauvais chose par les administrateurs de contenu.
Ce qui rend son travail d'idol d'autant plus important. Les reportages, interview, spectacles, et témoignages que les idols enregistrent et publient hors des plateformes sociales, constituent une forme d'archive mémétique des tendances.
Et, au final, cela rend chaque personne responsable de sauvegarder ce qu'ielle trouve important. De publier des analyses hors de l'immédiateté des plateformes sociales, de penser à mettre une note d'archivage sur un élément culturel important à un moment. D'être un minimum conscient de ce qu'il se passe dans leur environnement proche.
Elle finit par sortir de sa cabine, la faim se faisant plus forte que l'envie de rester ici. Vêtue d'un simple boxer, elle traverse son étage du bauhaus jusqu'au vestiaire collectif et farfouille dans les casiers à la recherche de quelque chose à se mettre. Pas des fringues de scène, juste quelque chose de confortable.
Elle jette son dévolu sur un survêtement floqué des logos et sponsors d'une écurie de Formule Sun et un Tshirt noir d'Obscene Tatoo Shop. Probablement un groupe de musique quelconque. Coda n'en a jamais entendu parler, elle aime juste le contraste des genres graphiques. Et elle n'est pas vraiment d'humeur à aller se renseigner maintenant. Il faut qu'elle mange un morceau d'abord.
Guidée par l'odeur de caramel et des pancake, elle descend deux étages, et rentre dans la salle de repas collectif. Elle se sert une tasse de chicorée qu'elle commence à boire tranquillement, assise à une table métallique, baignée dans la lumière d'un vitrail rose et vert, reprenant une des couvertures de Ymir, un des idols du Syndicat, qui a finit par arrêter de tourner il y a quelques années.
Alors que la boisson chaude rince sa dépression, et lui redonne un peu d'énergie, elle démarre ses analyseurs de tendance. Il doit y avoir quelque chose d'intéressant qui se passe, il lui faut juste trouver quoi.
"Comment tu te sens ce matin?" lui demande la petite rouquine qui s'installe à côté d'elle, lui amenant une assiette de pancake à base de farine de mycélium, et de la mélasse fumée, aromatisée à l'hibiscus.
"Redemande moi dans cinq minutes, Bey," répond Coda, commençant à s'attaquer à la pile de pâtisserie encore chaude. La scénariste se sert dans la pile, mangeant doucement les galettes dorée. Contrairement à Coda, elle ne mange pas parce qu'elle a faim, mais comme une excuse pour rester assise à la table, développant une technique précise pour mettre le plus de temps possible pour avaler la pancake baignant dans la mélasse.
Le sort des pancake est rapidement réglé, et la sensation de satiété calme Coda, lui permettant d'aborder son environnement dans un état plus ouvert sur les autres. Ses chasseurs de tendance lui ont remonté une liste de choses qui pourrait valoir le coup d'être creusée, mais rien de fantastique.
"Tu as quelque chose pour moi ?" demande Coda, permettant à la scénariste de faire son travail.
"Pas grand chose de neuf non. Mais je me demande si tu ne voudrais pas repasser en studio enregistrer quelque chose. On a des auteurices prêtes à t'épauler, et tu finis toujours par nous trouver des mélodies intéressante.
"Je n'ai pas envie de te rajouter des scripts en ce moment. Juste une ligne émotionnelle classique, pour t'aider."
"M'aider à quoi ? Je suis pas certaine de vouloir faire encore la même chose, je tourne en rond depuis un peu de temps. J'ai envie de refaire de l'exploration mémétique de l'analyse."
"Comme Cuddle… " soupire Bey, pas convaincue par l'idée de Coda.
"Oui, comme CTU. Pourquoi on ne fais pas ça ? Pourquoi est-ce que je fais juste chanter et danser pour alimenter les fantasmes des unes et des autres ?"
"Parce que c'est ce que tu aimes faire, normalement. Mais c'est pour ça que je te proposais un passage en studio plutôt. On peut ajouter des éléments extérieurs, on peut renouveler Coelia hein. Juste… Laisse moi faire mon taff, d'accord ?"
"J'ai juste l'impression de sombrer dans une confusion émotionnelle perpétuelle, de perdre pied. Je suis pas certaine d'avoir l'énergie en ce moment pour grand chose."
Bey ne dis rien. Elle écoute ce que Coda a à dire. Elle se content juste de prendre sa main dans la sienne, de lui donner un support émotionnel.
Coda essaye de formuler des choses, de nommer les émotions qu'elle ne comprend pas forcément mais qui l'assaillent de plus en plus souvent. Mais elle ne parvient qu'à garder le silence. Et à se cramponner à Bey.
Les chasseurs de tendances de Coda finisse par attirer son attention hors de cette spirale émotionnelle et lui notifie visuellement qu'ils ont trouvés quelque chose qui devrait l'intéresser.
Les groupes de travail de maintenance ont des difficultés à avancer dans leurs réunions. Rien d'anormal en soit, si ce n'est qu'au moins une dizaine d'entre eux semblent faire face aux mème perturbations. Des personnes qui demandent la publication de journaux cachés.
Une des premières personnes à avoir publié sur le sujet et une certaine Octane. Elle est devenue, de fait, la coordinatrice des efforts des groupes de travail de comprendre ce qu'il se passe.
Coda demande à son chasseur une corrélation avec de nouveaux mèmes. En quelques instants, celui-ci lui retrouve l'émergence d'un nouvel élément syntaxique, utilisé dans pas mal de discours des espaces interstitiels, ces espaces non sauvegardés par les instances sociales, à mémoire courte. Ces espaces non référencés et difficile à indexer.
Elle n'y aurait pas prêté attention si celui-ci n'était pas également présents dans une série de photos publiée par Staxy. Des photos de tags sur différentes coursives, au milieu des nombreux grafs, tags collages et sculpture qu'infligent les habitants aux panneaux des coursives de leur habitat, leur servant d'espace de libre expression artistique et politique.
"Libérez :-", "Où est :- ?" ou, simplement, ":-" sans rien d'autres. Ces messages ont fleuris un peu partout, mais leur fréquence d'apparition suit une tendance qu'elle connaît bien. C'est la même que celle qui précède les rumeurs parlant de Coelia, du moins quand les scénaristes font correctement leur boulot, et ont bien organisés la diffusion d'information.
C'est un peu maigre pour lier les deux phénomènes ensemble, mais les instincts de Coda lui hurle qu'ils sont liés. Elle ne sait pas encore comment, mais elle a envie de savoir.
"J'ai un truc à creuser. Une histoire sur les groupes de travail et, peut-être, un lien avec un nouveau complexe mémétique." dis Coda, pour tenir sa scénariste au courant. Elle partage avec elle quelques liens pour illustrer son propos.
C'est loin de ce que fait Coelia habituellement, elle le sait. Elle sait que Bey n'est pas enthousiasmé par l'idée. Elle n'a pas besoin de script d'analyse pour lire son langage corporel.
"Et je veux bien retourner en studio, bosser sur un album. Mais pas du néo classique s'il te plaît. J'ai bien aimé quand on avait fait *Kiss me*, cette période post porn pop, tu te rappelle ?"
Bey se rappelle bien. Les réactions avaient été violentes. Beaucoup de monde s'indignant d'un discours qui, d'après leurs critiques, se basait sur l'exploitation de la sexualité de Coelia, sans aucune critique politique. Mais aussi un des albums qui reste, à ce jour, le plus efficace de ce qu'elles ont fait.
Les critiques avaient raté le fait que tout morceau de musique n'a pas forcément pour but de soutenir un argumentaire politique d'argumentation, et la plupart de ces reproches ont maintenant été oubliés. Mais Bey n'est pas complètement certaines de vouloir retourner sur ce terrain.
Elle laisse échapper une longue expiration. Elle est censée aider Coda à naviguer les scripts et les mnémorithmes, s'assurer qu'elle aille le mieux possible. Et si elle veut se lancer dans un album de porn pop, c'est à elle de prévenir les volontaires de la modération.
"D'accord. D'accord, on va faire ça. Et on va faire ça bien. Et bosse sur ton sujet, tu as raison, il y a peut-être quelque chose à creuser. On Tu me tiens au courant de comment tu avances là-dessus, d'accord 