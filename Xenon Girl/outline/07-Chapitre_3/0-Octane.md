title:          Octane
ID:             219
type:           md
summaryFull:    Coda tombe sur un nouveau memeplexe autour de :-, qui prend de la place dans les discussions des groupes de travails
                
POV:            11
notes:          {C:11:Octane}
                {C:12:Tik}
                {P:7:Enquête de Coda (et Staxy) sur la conspiration} Plot turn 1
                {P:1:Le Naked Brocoli}
                {W:50:coursiers de maintenance}
                {W:66:moniteur}
                {W:67:capteurs environnementaux}
                {W:68:analyseurs comportementaux}
                {W:14:Idols}
                {W:69:groupes de travails}
                {W:71:Bauer}
                {W:19:gravel rock}
                {C:23:XT}
                {W:72:riot porn pop}
                {W:15:V.S.T.L}
label:          4
status:         5
compile:        2
setGoal:        1500
charCount:      10695


La journée d'Octane n'avait pourtant pas si mal démarré. Elle avait enfin pu trouver un peu de temps pour bosser sur son projet de transformer une pompe Bauer et a l'intégrer dans un système d'assainissement d'eau grise.
En plus Audrey la laisse un peu tranquille aujourd'hui. Elle aurait pu continuer à traîner dans cet atelier collectif, a écouter la ci-bi, à laisser la musique la guider tranquillement pendant qu'elle réassemble les carters en aluminium du système étalé sur les tables de travail qu'elle partage avec XT.
Mais elle a préférer aller animer le groupe de travail de maintenance du secteur dans lequel elle zone en ce moment. Elle aurait pu se mettre en quête de cette légère fuite de dioxyde de carbone, et ramper entre les coques de l'habitat à chercher à clore cet incident de faible priorité. Ou n'importe quoi d'autre de faible importance, ce ne sont pas les occupations qui manquent pour les nomades qui ont une patience particulière pour la mécatronique.
Mais non, elle se retrouve dans la salle commune occupée par le groupe de travail filtration et recirculation, a essayer d'animer une réunion dont le but est, théoriquement, de permettre aux nomades et habitantes du secteur de se tenir informé des principaux problèmes, qu'ils s'agissent de pannes ou de maintenance préventive.
Elle essaye de clamer son agacement, et de remettre sur les rails la discussion qui a dérapé.
"Écoutez, on peut reprendre cette discussion après le reste de l'ordre du jour", dit-elle, essayant de canaliser une audience particulièrement nombreuse.
"Mais ça ne prendra que deux minutes, répond Tik, on veut juste savoir ce qu'il se passe avec ces journaux d'erreur, c'est tout."
Octane sait que ça ne prendra pas deux minutes. Le sujet a déjà pris plusieurs heures de la réunion. Ainsi que de la vingtaine de personnes présente, un nombre anormalement élevé. Elle aurait du se douter que les choses n'allait pas se passer normalement lorsqu'elle a vu l'affluence. Elle se réjouit habituellement de voir de nouvelles têtes venir aider à faire le boulot tellement nécessaire de maintenir l'habitat en fonctionnement, mais plus de dix nouvelles têtes, c'est étrange. Elle aurait du se méfier, mais elle préfère penser que d'autres personnes se soucient vraiment du fonctionnement des infrastructures vitales de l'habitat.
Après tout, tant que suffisamment de monde participe à la maintenance des petits systèmes de survie, le gros de l'infrastructure se régule assez facilement. Donc Octane apprécie toujours voir ces nouvelles têtes participer aux groupes de travail, ça veut souvent dire qu'elle, et les autres nomades, vont pouvoir changer de secteur ou s'attaquer à des plus gros chantiers, ou à un afflux de ressources.
Mais elle sait aussi que ces groupes de travail servent essentiellement à faire circuler de l'information. À transformer les statistiques des différents réseaux de gestion d'incident en quelque chose de plus facile à assimiler et à comprendre pour les personnes qui ont des attentes parfois un peu trop spécifiques. Mais aussi à intégrer dans les communautés techniques les impacts de leur actions par les retours des habitantes.
"Il ne se passe rien avec les journaux d'erreurs. Rien d'inhabituel du moins, et ils sont stockés dans les archives. N'importe qui peut les consulter." Elle répond factuellement. Patiemment, même si sa fatigue commence se faire sentir. Mais ce Tik semble convaincu qu'elle lui ment, qu'il y a quelque chose d'anormal.
En général, c'est le cas. La station est une communauté de machines hétéroclites, et il se passe souvent quelque chose d'anormal. Une machine qui refuse de coopérer avec le reste, des réglages contradictoires qui envoient les boucles PID dans des luttes mathématiques infinie, des erreurs humaines aussi qui connectent une pompe à l'envers. C'est pour ça qu'il y a les coursiers de maintenance, qu'il n'y a pas de système unique, que personne ne dépend jamais que d'une seule machine, que le flux maintient cette structure dans un état précaire d'apparente stabilité.
Cette fausse stabilité qui est le domaine des nomades de maintenance, errant dans la station, à la recherche des trucs bizarres pour essayer de déterminer si il y a un problème et le régler. Se fixant sur un collectifs quelques jours ou semaines, le temps de s'assurer que leur travail résout les problèmes sans en créer d'autres.
"C'est ce que nous on dit les nomades oui. Mais…"
"Mais quoi ? Quel est, au juste, le problème sur ce circuit de filtration ?" lâche Octane, interrompant Tik.
"Le problème c'est que vous nous cachez les journaux d'erreurs ! Les vrais, pas les versions assainies. Ceux qui nous permettrait de voir réellement ce qu'il se passe !" Tik commence à hausser la voix. Uune demi douzaine de personnes parmi les nouvelles têtes acquiescent à ce qu'il dit, à ces exigences absurdes.
Personne n'a besoin de lire les formats binaires de journaux d'erreur. Pas tant qu'on est pas branché en direct dessus avec des interpréteurs de langage machine. C'est pour cela qu'ils ne sont jamais collectés et archivés. C'est une dépense de stockage et d'énergie absurde et inutile.
Octane jette un œil au moniteur environnemental attaché à son poignet. Les mesures environnementales ne relèvent pas un taux de dioxyde de carbone particulièrement élevé. Ses analyseurs comportementaux qui scannent la dialectique corporelle des personnes présentes, lui confirme que Tik n'est pas inquiet. Et vu qu'il n'est pas non plus intoxiqué au dioxyde de carbone, il choisit délibérément de prendre une posture d'agressivité.
Elle se surprend à regretter l'absence d'idol à cette réunion. Ielles sauraient désamorcer cette situation. À la circonscrire à quelque chose de gérable individuellement. Mais elle doit faire ça seule, car il n'y a pas besoin de plus de monde pour animer ces points d'information. Normalement.
"Pourquoi penses-tu qu'on vous cache des choses ? Encore une fois, toute l'infrastructure est accessible, tu peux aller récupérer tous les journaux d'erreurs que tu veux. Tu peux connecter ton propre collecteur si tu veux…"
"Parce que c'est comme ça que vous imposez votre façon de faire, votre contrôle sur l'infrastructure, sans laisser aux habitants des secteurs décider pour eux-mêmes." Octane manque s'étouffer après que Tik ai énoncer cette réplique. La raison pour laquelle les nomades ne sont pas fixés à un endroit, la raison pour laquelle il n'y a pas de système central de gestion des incidents, la raison pour laquelle il y a ces groupes de travail, c'est pour justement prouver qu'il n'y a pas un groupe qui aurait pris le contrôle de systèmes critiques de la station. C'est quelque chose qui est expliqué à tous les vocas, quelque que soient leur parcours. Qui est réexpliqué à chaque nomade, à chaque personne qui entreprend de modifier de manière significative.
Que Tik dise ça est la preuve qu'il n'est pas de bonne fois, et qu'Octane perds son temps. Mais elle perd aussi son calme.
"Et donc, tu parles au nom des habitants du secteur ? Ielles t'ont donnés un mandat pour ça ?" demande Octane, essayant de lui faire tomber dessus la responsabilité du déraillement complet de la réunion.
"Ce n'est pas le sujet, répond-il. On veut juste l'accès à ces journaux originaux, et après on vous laisse tranquille. C'est tout. Il n'y en a que pour deux minutes."
*Sauf que ces journaux n'existent pas.* Veut hurler Octane, qui, dans un dernier acte de contrôle personnel, garde cette remarque pour elle même.
"Bon, je propose d'ajourner cette réunion. On y a déjà passé beaucoup de temps," répond-elle à la place.
"Et nos questions alors?"
"Comme d'habitude. Postez les sur le groupe de travail. Si quelqu'un se sent d'y répondre, alors vous aurez une réponse. Sinon… ben tant pis."
Octane ne laisse pas le temps à Tik ou à quiconque de réagir, elle sort de la salle commune de l'atelier, contenant sa colère, son épuisement. Elle parcourt les coursives du petit bloc, et se pręcipite dans son atelier partagé, encore désert. Elle pousse un hurlement de frustration et contemple la pompe bauer sur son atelier.
Elle la laisse tranquille. Elle n'est pas dans un état lui permettant de continuer son réassemblage, elle a besoin d'un état d'esprit moins destructifs. La ci-bi diffuse du gros gravel rock. Elle attrape le micro de l'émetteur, bascule sur la fréquence de roll-call, et énonce son identifiant et le fait qu'elle décroche pour la journée. Elle ne prend pas le temps de suivre les réponses et parcoure le spectre à la recherche d'une station plus récréative.
Du riot porn pop finit par surgir dans les enceintes surdimensionnée de l'atelier, et elle laisse le rythme se saisir des ses hanches, alors qu'elle s'agite et cherche à évacuer la tension de la journée. Pas le meilleur album de Coelia, maisc'est parfait pour ce dont elle a besoin.
Après vingt minutes à se défouler seule dans son atelier, son humeur· s'est définitivement améliorée. La sueur lui colle son Tshirt gris à la peau, son odeur couvrant presque celles des différents dégrippants et alcools techniques qui embaume habituellement l'espace. Continuant à danser de manière plus calme, elle enlève son Tshirt et se rafraîchit avec l'évier qui sert habituellement à rincer la graisse des mécanismes. Deux mêches roses tombent de son crâne presque rasé devant ses yeux, coupant son visage aux traits finement découpés. Sa peau claire est parsemée de petites tâches blanches, des vestiges d'une explosion d'une canalisation de sodium en fusion il y a quelques années.
Elle se rince le visage et les aisselles, son reflets dans la vasque d'acier poli lui renvoyant sa silhouette menue, déformée par les courbes d'acier. Son corps est parsemé de petites cicatrices, accumulées au fil du temps, autant de souvenir d'incident technique, de défauts de documentation. Elle enlève le short de toile couvert de cambouis enfile la robe au motifs floraux qu'elle avait enfilé avant de se mettre à bosser ce matin.
Elle ajuste les sangles d'un harnais qu'elle enfile par-dessus le tissu léger, et convoque une interface sur les forums de coordination es nomades. Ou l'un d'entre eux. Une des nombreuses instances sociales qui servent de support aux interactions des habitants.
"La réunion du GT filtration et recirculation a déraillée, quelqu'un d'obsédé par des journaux que l'on dissimulerait, et qui a refusé de laisser le reste de la conversation avoir court. Suis-je la seule ?" poste-t-elle, avant de quitter son atelier. Elle a encore besoin de se changer les idées, et Audrey voulait lui faire rencontrer quelqu'un qui aurait besoin de s'occuper. Elle se mes donc en route vers le boulevard radial et vers le Naked Brocoli. Un des endroits dans lequel elle passe du temps quand elle n'est pas en 