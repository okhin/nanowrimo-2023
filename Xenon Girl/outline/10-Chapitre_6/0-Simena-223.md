title:          Simena
ID:             223
type:           md
summaryFull:    Simena participe au backstage du Naked Brocoli, et rencontre Kit
                
POV:            0
notes:          {C:0:Simena Von Guyen the Third}
                {C:13:Kit}
                {P:1:Le Naked Brocoli} Mid point
                {C:11:Octane}
                {W:83:karaoké}
                {W:6:Naked Broccoli}
                {W:23:Cuddle the Urchins}
                {W:15:V.S.T.L}
                {W:49:Recyclage}
                {W:90:serveur}
                {W:10:Consortium}
                {C:8:Xicha}
                {W:87:ersatz de café}
                {P:10:Kit}
label:          4
status:         5
compile:        2
setGoal:        1500
charCount:      9530


"Hey, comment vas-tu ?"
Le message d'Octane interromp Simena alors qu'iel est occupée à refaire toute la distribution électrique du Naked Brocoli. D'autant que pour une scène ouverte, avoir une installation de distribution électrique fonctionnel est critique. toute la chaîne est cependant un entremêlat de cables de cuivre, de contacteur et disjoncteur, d'interrupteurs et de transformateurs, assemblés itérativement, au fur et à mesure des interventions des volontaires qui n'ont pas forcément le temps de tout reprendre depuis le début.
Et hier soir, en pleine session de karaoké, la distribution principale à finit par rendre l'âme. Les volontaires ont essayés quelques heures de remttre les choses en l'état, mais ielles ont finit par isoler la scène et tous les appareils de divertissements, pour remettre en route l'éclairage principal et la cuisine, laissant la scène en carafe le temps que quelqu'une de motivée ne s'attaque au problème.
Rien de grave au final, un bobinage d'un transfo d'isolation qui a fondu. Simena iel a commencé par le change, mais, après qu'iel ait un peu creusé et trié la connectique, iel a constaté un déséquilibre de phase, source probable des problèmes à répétitions. Iel s'est donc mise en tête de tout reprendre depuis le début.
"Je me refait la distribution élec du Naked Brocoli. Et toi?" répond Simena, utilisant le clavier de son terminal de poignet pour taper sa réponse. Cela luiprend du temps pour répondre aux personnes, mais iel n'a pas encore de broche neurale implantée, comme beaucoup de monde ici, ce qui l'oblige à avoir un débit de texte limité à sa capacité à utiliser un clavier.
"C'est un peu la merde en ce moment, sur les groupes de travail." Répond Octane, toujours par l'interface texte. Simena n'est pas certain de comprendre exactement à quoi servent réllement les groupes de travail, mais iel sait que cela stresse Octane, et que cela lui prend tout son temps. Elle n'a pas réussi à mettre ces problèmes de côté pour partir en nomadisme, comme elle l'aurait voulu et c'est un sujet sur lequel elle tourne en rond et qui lui pourris le moral. Mais Simena ne sait pas vraiment ce qu'elle peut y faire, à part l'écouter. Quand elle prend contact, ce qui se raréfie en ce moment, même si Simena squatte chez elle, Octane n'y dors pas souvent. Ou pas en même temps qu'elle.
"Attends. reprend Octane, T'as dit au Naked Brocoli ? Le backlog élec est horrible, t'es sûre que ça va aller toute seule ?"
L'accumulation de petits incidents reportés par les volontaires du tiers lieu dépasse probablement les quotas imposés par n'importe quel gestionnaire d'un centre d'appel technique du consortium, plus d'une centaine de soucis sont catalogués sur l'infrastructure. Mais refaire la distribution de ce qui n'est, au final, qu'un bête régime de phase, est facile. Une fois qu'on accepte de tout refaire du moins, et de payer une fois pour toute la dette technique.
Plus simple, en tout cas, que d'essayer de faire fonctionner l'armoire électrique vomissant des câbles et d'essayer de la réparer. Simena a donc repris l'inventaire de tout ce qui est branché, catalogué les différents besoin de chaque appareil et s'est attaquée frontalement au problème.
"T'inquiètes, j'ai fait pire sur le Just a big Dumb Spaceship. Je te raconterais, la prochaine fois que tu passe boire un verre ici." Iel se rappelle ses quelques semaines d'angoisse, quand le système électrique du Chernobyl, traversant la magnétosphère de Jupiter, avait rendu l'âme, et qu'iel avait du refaire tout le circuit. Les radiations avaient, certes, grillé son transpondeur, lui donnant un peu de répit, mais la plupart de la distribution électrique était morte.
Et il y a quelques kilomètres de câbles, des centaines de convertisseurs et de transformateurs en tout genre dans ce genre de système. Iel avait finit par pouvoir récupérer la puissance des réacteurs nucléaires et, de là, en y passant plusieurs heures par jour, avait finit par remettre en route l'essentiel des fonctions du vaisseau, avant que les batteries des systèmes de survie ne lâchent définitivement.
Refaire un régime de pahse pour le Naked Brocoli est donc une promenade de santé pour Simena. Mais ça lui va bien les choses simples pour le moment.
Iel s'extrait de la jungle de câbles, qui comence à être un peu plus ordonnée et lisible, ferme l'armoire de distribution, souffle un peu en étirant son épaule qui lea lance, à force de travailler en hauteur, et lance à la cantonade: "Vas-y Kit, teste moi tout ça !".
Kit, derrière la console de gestion de la scène, bascule les interrupteurs les uns après les autres. Puis, émettant un bruit de disjonction caractéristique, l'installation saute et plonge la scène principale dans le noir.
C'est la troisième fois que Simena rebranche toute l'armoire. Mais au moins, cette fois, ce n'est pas le général qui a sauté, juste un différentiel. Il y a donc un court circuit quelque part et iel va devoir tester chaque appareil les uns après les autres, pour isoler le coupable.
Mais au moins, elle progresse plus vite maintenant. L'infrastructure est certes un tas de câble et d'appareil assemblés les uns sur les autres sans réelle anticipation, mais ce bazar est documenté minutieusement. Et elle a déjà extrait quelques dizaines de mêtres de câbles inutiles des tripes du Naked Brcoloi.
"Alors ?" lui demande Kit, lui amenant une tasse de café. Iel aime bien Kit, qui prend soin d'ellui quand iel est dans le secteur, ce qui la change des regards circonspects, au mieux, des habitants du coin, avc qui le courant ne passe pas.
"Alors ça progresse. Un souci sur un différentiel. Mais ça sera bientôt réparé, juste le temps de tester chaque appareil pour une fuite de masse."
"Bientôt ?" 
"Je sais que j'ai dit ça il y a plusieurs heures. Mais laisse moi finir, d'accord ?"
Simena sait qu'iel use un peu la patience de Kit, et des autres volontaires du lieu, mais iel arrive au bout du problème. Et surtout iel veut leur prouver qu'iel contribue aussi au lieu qu'iel peut faire sa part. Peut-être, ainsi, ils finiront par l'accepter.
S'inccruster dans la gestion du Naked Brocoli s'est avéré à la fois simple et complexe. Simple, parce qu'iel a juste eu à se créer un compte sur leur instance sociale, et à s'attribuer les taches du backlog pour les faire. Complexe, parce qu'iel ne cesse de butter sur les non-dits et les règles tacites de fonctionnement du lieu. Règles et coutumes qui doivent lui être expliquées, tantôt par Kit, tantôt par Octane.
Iel garde donc plutôt un profil bas. Bricolant ce qui peut l'être, tout en essayant de s'imprégner de l'ambiance du lieu et de sa communauté. Ambiance qui lui rappelle, par moment, celle que l'on peut retrouver dans les cantines ouvrières du consortium. Sauf qu'il n'y a pas d'horaires d'ouverture. Ou de numéros de strip tease.
Elle se replonge dans l'armoire électrique, affichant en surimpression sur sa visière le schéma théorique du régime de phase, et revérifiant point à point chacun des appareils. Il lui faut encore quelques minutes pour isoler le coupable : un ampli qui se retrouve branché dans la boucle lumière et qui avait échappé à son attention, et dont le brochage n'est pas compatible avec le régime de phase, générant des fuites de masse.
Le temps de le retirer du circuit et de le réintégrer dans un circuit respectant ses polarité, et iel fait confirmer à Kit que, cette fois, tout fonctionne sans disjoncter. Elle referme les cloisons ouvertes dans lesquels les chemins de câbles respectent une logique vérifiable à l'œil nus, entasse les bobines de cuivres qu'elle a récupéré et range ses outils, et enfile son blouson floqué du logo des V.S.T.L, chacune des idols y étant représentée par sa silhouette stylisée, chacune dans leur couleur. Une vieillerie qui fait partie des rares choses qu'elle a gardé avec elle dans sa fuite des puits de gravité.
Iel s'adosse au comptoir, prenant la place qu'iel s'est attribué. L'endroit où iel passe l'essentiel de son temps quand iel n'est pas occupée à réparer l'installation électrique du moins. Pas assez de réputation pour aller s'asseoir au fond, trop vieille pour suivre le rythme des plus jeunes et des vocas qui viennent danser, trop seule pour partager les grandes tables communautaire. Ici, iel est bien.
Iel en profite pour fermer l'ensemble des tickets du backlog, soldant d'un coup plusieurs années de dettes technologique, et libérant de l'espace dans le tableau de bord collectif. Iel remarque des changements dans certains regards à son adresse. Au moins une partie des habituées semblent faire un effort pour reconnaître qu'iel sait ce qu'iel fait. Mais d'autres, une majorité, restent distants, presque hostile. Ce sont celleux qui n'ont cessé de râler quand iel remettait en état l'installation, coupant le courant à de nombreuses reprise, les privant de musiques et de clips vidéos. les plongeant dans le noir ou les empêchant de prendre leur boissons à la température qu'iels préfèrent.
"Au fait, tu as eu le temps de voir Xicha ?" lui demande Kit, la rejoignant à côté du comptoir, après qu'il ait démarré une ambiance sonore dans le système de son remis à neuf.
Iel n'a pas eu le temps de voir Xicha. Enfin, si. Simena l'a vue. Elles se sont saluées, et croisées deux trois fois. Suffisamment pour qu'iel développe une forme d'engouement pour Xicha. Ce qui, du coup, la prive de ses moyens et l'empêche d'aller lui parler sérieusement. Parce qu'elle a eu le temps d'aller la voir.
"Elle prend sa ronde dans deux heures, tu devrais vraiment lui parler. Ça te faciliteras la vie ici," lui dit Kit.