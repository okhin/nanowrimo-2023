title:          Xicha
ID:             1380
type:           md
summaryFull:    Simena rencontre Xicha au Naked Brocoli, et l'aide à tenir le lieu en backstage. Elles passent un très bon moment ensemble.
POV:            8
notes:          {C:8:Xicha}{C:0:Simena Von Guyen the Third}
                {P:3:Simena et Xicha} Plot turn
                {W:6:Naked Broccoli}{W:91:cuves hydroponique}
                {W:92:discussions asynchrone}{W:14:Idols}{W:62:Compte social}{W:36:Chernobyl}{W:15:V.S.T.L}{W:98:chop suey}
label:          4
compile:        2
setGoal:        1500
charCount:      9712


Xicha sort de son compartiment, traverse la cour délimité par les containers abritant les compartiments des autres habitantes du bloc, franchit le boulevard radial et les jardins qui ornent le milieu de la chaussée, jauge d'un œil professionnel la terrasse du Naked brocoli, encore déserte, et pénètre dans l'établissement, son foyer, ses formes généreuses obstruant la lumière venant de l'extérieur, projetant une ombre large et allongée sur le sol en béton du lieu.
Elle réajuste le foulard rouge qui retiens ses cheveux noirs en arrière. Elle parviens encore à dissimuler les quelques mèches blanches, témoins silencieux mais extrêmement frustrant des situations de stress qu'elle a affronté il y a plus de vingt ans.
Xicha salue rapidement les habitués d'un geste ou d'une simple question, se forçant à tenir un état mental des réponses. Elle ne veut pas dépendre d'un système informatique pour savoir ce qu'il se passe dans la vie des personnes qui peuplent son lieu.
Parce que oui, le Naked Brocoli est son lieu. Elle est probablement la dernière parmi les premiers arrivés à bord de ce qui était alors la Station Minière 37 à être resté autour du Naked Brocoli, les autres ayant voulus essayer d'autres choses, ont finit par quitter la station pour aller porter leurs révolutions ailleurs, ou ont finis par mourir irradiés ou dans différents accidents.
Elle chasse rapidement ces pensées alors qu'elle se diriger vers les cuisines collectives, son quartier général. Simena est là, à discuter avec Kit. Xicha doit lui parler, au moins pour savoir ce qu'il se passe dans la vie de lea pilote. Iels se sont à peine croisées en deux semaines, et Xicha sent que Simena semble être mal à l'aise en sa présence. Elle sait que c'est probablement parce que Kit ou quelqu'un d'autres lui a parlé d'elle. Sa réputation qui la précède. Elle déteste ça, et il va falloir qu'elle en parle avec Kit. Mais d'abord, parler avec Simena.
"Simena, viens avec moi, faut que je te parle d'un truc ou deux," lui dit-elle, alors qu'elle rentre dans la cuisine. Simena finit par passer son groin dans l'embrasure de la porte. Xicha l'invite à rentrer dans la salle recouverte de paillasse et de machines destinée à la préparation des légumes, accumulé en un tas en face d'elle.
"Tu sais cuisiner?" demande Xicha.
"Pas vraiment, répond Simena. Non, ajoute-t-iel."
"Tu vas voir, rien de compliqué. On fait un chop suey pour ce soir. Du coup, toi tu trie et dégrossis les légumes. Je m'occupe du reste. Et on parle."
Simena est comme figée devant la tâche. Xicha y va peut-être un peu fort.
"Jamais vu de légume avant ?" demande-t-elle.
"Non. Enfin, si. En photo. Sur les emballages des trucs que je mangeait le weekend, quand je pouvais me les offrir. Mais sinon j'ai essentiellement mangé des barres de nutriments informes pendant..." Simena semble essayer de compter les années.
"Depuis que tu es née ?"
"Oui, mais… Ce n'est pas si simple," répond-iel.
"Et tu ne veux pas en parler ?" demande Xicha.
"Je sais pas… J'ai jamais mangé autre chose que les trucs servis par le Consortium à ses travailleurs et travailleuses. Donc non, je n'ai jamais vu de légume, je n'ai jamais cuisiné, et je ne sais pas ce qu'est un chop suey."
Simena est resté immobile, levant à peine les yeux. La fin de sa phrase est à peine articulée.
"Bon. On va reprendre, je vais t'expliquer, ne t'inquiètes pas. C'est surtout un prétexte pour faire connaissance, d'accord ?" Xicha s'est rapprochée de Simena, et s'est mise à sa hauteur. D'une main elle lui attrape doucement le menton pour lui relever le regard et la dévisager plus en détail. Ses yeux bruns, enfoncés dans son crâne allongé lui sont étrange, forme une expression que Xicha n'est pas certaine de pouvoir qualifier d'humaine. Une expression qui est censée évoquer l'empathie, mais qui ne fonctionne probablement pas chez les humains, trop étrange, trop différente, et pourtant tellement semblable. Comme les cochons dont elle tire son héritage génétique et dont elle a récupérer les traits faciaux.
Réalisant cette distance, Xicha comprend que Simena est seul. Terriblement et extrêmement seul. Elle lea prend dans ses bras, mais elle sent Simena se raidir à son contact.
"Hey, tout va bien. D'accord ?" lui murmure-t-elle dans l'une de ses oreilles tombantes. "Le chop suey peut attendre."
Iels restent ainsi quelques instants, avant que Simena ne se détende un peu et ne sorte de l'étreinte de Xicha, un afflux sanguin dans ses joues se faisant percevoir par un léger rosissement perceptible sous la fine fourrure qui couvre l'hybride.
Ce qui doit être un sourire s'est dessiné sur les lèvre de Simena, faisant apparaître ses coins, les incisives pointues dont elle a génétiquement héritée.
"Ça ira, merci, dit-iel, maintenant le contact physique avec Xicha. Tu voulais m'expliquer c'est quoi un chop suey ?"
"Ouais. Rien de bien complexe. C'est une recette de chez moi, mais on va faire avec ce qu'on a ici." Elle se place derrière Simena, et prend ses mains dans les siennes, lea guidant dans les gestes précis requis pour émincer les choux et tubercules variés issus des jardins et des hydroponiques orbitaux. Xicha sent la respiration saccadée de Simena.
Xicha lea guide quelques instants, lui explique les différences de cuisson entre les différents légume, puis, un peu à regret, prend ses distances pour démarrer la cuisson, sur l'une des tables à inductions. Elle réalise que la proximité avec le corps de l'hybride lui donne chaud, qu'elle a envie de mieux lea connaître, de lea sentir près d'elle. Elle défait discrètement les fermeture situées sous les aisselles de sa tunique à manche longue pour laisser sa peau de jais s'aérer et essayer de se rafraîchir.
"Tu disais que savoir quand tu étais née n'était pas si simple. Tu veux en parler ?" Xicha préfère re franche dans ses sujets de discussions, plutôt que de tourner autour du pot, à attendre que le sujet ne revienne naturellement. Elle n'a pas la patience des idols ou des thérapeute.
Simena prends quelques instants avant de répondre.
"Ouais… C'est le souci d'être née en laboratoire. Qu'est-ce qui compte pour déterminer mon anniversaire. La date d'éditions des cellules souches propriétaires qui formeront ensuite l'amalgame qui sera mon corps, avant qu'il ne subisse un développement accéléré en cuve ? Le moment ou ce corps, quelques deux ans après sa conception, sort de cuve, ayant atteint le stade de développement d'un adulte ? Ou alors le on compte le développement émotionnel et mental relatif pendant ces deux ans, qui, a grand coup de patch logiciels et de CARA m'ont fait atteindre l'équivalent de la trentaine dans ce même intervalle ? La date de ma sortie de laboratoire,qui correspond à celle de mon transfert à une corporation minière, la même date que les robots issus des chaînes d'assemblage ?
"J'ai le corps de quelqu'un qui a quarante ans, mon esprit ayant atteint un développement intellectuel de trente ans, mais je n'ai que quinze ans, à peu près, d'expérience personnelle set de développement émotionnel.
"Et on ne célèbre pas l'anniversaire des machines. Donc voilà. J'ai pas de meilleure réponse que : c'est compliqué. Pourquoi est-ce que tout le monde veut absolument que j'ai un âge ?"
Simena est passionné, sa voix s'eraille sur la fin, une colère qu'elle ne parvient pas à garder sous contrôle, juste à fleur de peau. Xicha se rapproche d'elle et lui pose une main sur l'épaule. Simena esquive son geste, et recule.
"Et pourquoi tu tiens tant à me toucher ?" demande-t-iel.
"Parce que personne ne t'as probablement suffisamment touché, par affection. Je… sais un peu ce que c'est de vivre dans les puits, et même si c'était il y a longtemps, je n'ai pas oublié à quel point le manque de proximité physique a eu un impact négatif sur le développement émotionnel de toute le monde." rępond Xicha, gardant la distance entre iel que Simena veut préserver.
"Tu ne sais pas ce que c'est de vivre dans les puits." répond Simena.
"Si. J'y suis née. Tu n'es pas la seule à t'en être extraite Et je ne prétend pas que mon expérience en tant que travailleuse a été la même que toi, j'ai moi aussi été baladée de chantier en chantier. Et on a atterri ici, avant que le consortium ne se décide que l'exploitation de cette station ne soit pas rentable et ne nous abandonne au milieu du chantier."
La voix de Xicha est montée d'un coup. Elle sent que sa colère lui échappe. Il faut qu'elle dise ce qu'elle a à dire, pour que cette colère s'épuise, et qu'elle puisse se calmer.
"Alors, certes, je ne sais pas ce que toi tu as vécu. Mais ne me dis pas ce que je sais ou pas de vivre dans les puits d'accord ? Et j'ai peut-être été maladroite, je m'en excuse. Mais je ne peux pas laisser quelqu'un être manifestement tant mal à l'aise."
Continue Xicha, chacun de ses mots emmenant avec lui une particule de colère, la privant de combustible, l'éteignant. Elle se calme enfin, Simena ne réagissant plus, sa propre colère, sa propre frustration ayant été comme balayée par celle de Xicha.
"Et j'ai envie d'être près de toi," dit Xicha reprenant sa position derrière les woks dans lesquels les légumes commencent à griller un peu trop, "mais si tu n'en as pas envie, c'est OK."
Simena reprend sa tâche, concentrée sur la découpe des légume en fine lamelle, en silence.
"C'est pas que je n'en ai pas envie…" finit-iel par dire. "Mais je cherche mes marques encore dans tout ce bazar."
"D'accord," répond simplement Xicha, surveillant la cuisson d'un œil.
"Il paraît que c'est toi qu'il faut remercier pour avoir remis à jour toute la distribution électrique ?" reprend-elle, ajoutant un saté aux douces odeurs de gingembre aux légumes, comme une façon de changer la conversation, de détendre l'atmosphère, de laisser à Simena l'occasion de parler de ce qu'elle aime 