title:          Notes
ID:             3172
type:           md
label:          2
status:         1
compile:        0
charCount:      2411


Les V.S.T.L sont un groupe d'Idols du consortium. Coelia est une version piratée de celles de V.S.T.L. Les V.S.T.L ne se sont jamais produites dans la station.

Une scène de gestion de conflit avec Xicha au Naked Brocoli

Expliquer les vocas, leurs groupements en écoles, et la gestion des isolées comme Yas.

Accueil de Simena dans une zone d'accueil aniimée. Présentation de certains système.

Prendre le temps de faire visiter la station. Parler de la politique interne, de la bouffe, du pipeline.

Prendre le temps de monter la solution de Coda à la fin.

Faire bosser Simena avec Octane dans la maintenance, un peu après etre arrivé au Naked Brocoli.

La relation Giuse-P Yas doit changer, plus sur un mentorat toxique, subit.

Les vocas. Détailler, expliquer.

Pourquoi Simena est si étrangère dans ce contexte. Pourquoi le Consortium ne viens pas la chercher.

Politique spatiale. Chaque gramme compte. We're in it together. Déclin du Consortium.

C'est quoi les idols au juste.

Revoir la conspiration. Détailler les espaces interstitiels, se reposer sur l'infrastructure du réseau.

Il y a un réseau de Radio à onde courte sur lesquelles les équipes techniques un peu éparpillées se connectent et se synchronise par Dispatch. Les Idols sont les animatrices de talk shows, concert, ou fiction scriptées sur ces fréquences.

Quel public Giuse-P recrute, et pourquoi. Il est persuadé qu'il y a une caballe capitaliste. Pourquoi il ne part pas?

Pourquoi les gens ne partent pas? Pourquoi ils restent et galère? Y-a-t-il d'autres statiosn comme celle-ci?

Un dispensaire/clinique/hopital.

Rajouter du monde, vraiment. Jouer sur le côté centre sociaux et culturels des bars, qu'ils sont grand. Détailler les bazars et la récupération. Ajouter des lieux de travails émotionnel, analyse, artistique, sexe.

C'est une station de logistique. Le montrer dans le bati, le brutalisme, l'efficacité, la dépendance aux sources externes d'approvisionnements.

Il y a des jardins productifs? Oú et ajouté comment et quand?

L'anneau fantôme comme lieu de villégiature des vocas.

Coda à développer. Sur ses doutes et l'hostilité contre les idols mainstream de syndicat. Son envie de changer/d'arrêter. De ne pas savoir qui elle est.

Les CARA, il y a en a d'autres, le montrer.

Pas de vie synthétique, mais prothèses et mnémorithmes très développés. Montrer les risques de ces derniers, et leur origine en tant que systéme thérapeutique.
 
Ajouter un prologue Yas.