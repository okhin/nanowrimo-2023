title:          Kit
ID:             222
type:           md
summarySentence:Kit prend son service au Naked Brocoli.
summaryFull:    Il arrive en roller, traverse la salle, manque de percuter Simena et est alpagué par Xicha.
                Celle-ci le recadre un peu et lui recolle les pieds sur terre avant de lui laisser la salle pendant qu'elle va prendre une pause.
                
POV:            13
notes:          {C:0:Simena Von Guyen the Third}
                {C:13:Kit}
                {C:8:Xicha}
                {W:6:Naked Broccoli}
                {C:6:Sarah}
                {W:25:Vocas}
                {W:86:oracle}
                {W:14:Idols}
                {W:71:Bauer}
                {W:3:construct artificiel de réminiscence autosuggestive}
                {C:11:Octane}
                {W:89:beignets}
                {P:10:Kit}
label:          4
status:         5
compile:        2
setGoal:        1500
charCount:      10166


Kit est en retard pour prendre sa shift au Naked Brocoli. Il déboule sur ses rollers au milieu des passants de la station, traversant les zones bordées de jardin du centre de l'anneau, loin des trottoirs roulants qui charrient celles et ceux qui, comme lui, ont besoin de rejoindre rapidement une section de la station diamétralement opposée à leur cube.
Il est en retard parce que Sarah l'a mis en retard. Il ne regrette pas vraiment le temps passé avec elle, à partager son cube, l'un et l'une contre l'autre. Et ses notifications qui s'affiche dans son champ de vision lui signale qu'elle pense encore à lui, et qu'elle n'a manifestement pas encore trouvé ses fringues.
Surgissant dans son champ de vision, juste au travers de la poitrine menue de Sarah, un drone d'entretien le pousse à enchaîner très rapidement une manœuvre d'esquive, passant par les patins de freins sur ses pointes, et enchaînant quelques petites pointes avant de récupérer suffisamment d'inertie pour se rééquilibrer et replonger vers les jardins dorsaux en marche arrière, avant de se retourner d'un rapide mouvement de hanche. Il entend quelqu'un applaudir derrière lui, alors que d'un tout petit geste de la main, il ferme le flux d'image que Sarah lui envoie, le marquant d'une étiquette d'archivage.
Un des avantages de pouvoir accéder sans aucune restrictions aux plateformes sociales, c'est qu'il est plus facile pour elleux de s'échanger photos et vidéos. Plus besoin de passer par des relais ou des protocoles hasardeux pour contourner les restrictions posés sur les vocas par leur brassards.
Leurs relations ont un peu évoluées aussi : Audrey a disparu plusieurs semaines avant de réapparaître soudainement, avec une nouvelle coupe de cheveux encadrant son visage et sa peau, déjà naturellement très brune, bronzée par les plasmas et UV du matériel de soudure. Elle et Sarah ont ensuite disparu dans le cube de cette dernière plusieurs jours, avant de donner des nouvelles ; Gab s'est un peu détaché de leur groupe, ne pas pouvoir piloter autant qu'il ne le souhaite lui pèse plus que ce qu'il ne l'avait imaginé, et il passe la majeure partie de son temps à errer dans la zone de transfert, à attendre qu'un créneau de pilote se libère ; Sarah et Kit, par contre, se sont rapprochés, un peu à sa surprise. Et donc, elle lui envoie des nudes.
Mais il n'a pas le temps de répondre ou de commenter, il est suffisamment en retard comme ça. Il s'engouffre sous l'arche qui marque une des entrées des anciennes galeries commerciales, qui ont été réaménagées selon les envies des habitantes de RAFT, freine brutalement en débouchant sur la petite placette et passe sous l'enseigne en néon vert dessinant un Brocoli dansant du Naked Brocoli.
"Salut, Kit!" Il reconnaît cette voix qui l'interpelle, un habitué du lieu. Mais il n'a pas encore réussi à mémoriser tous les noms des piliers du lieu. Son oracle personnel identifie le timbre et l'associe à une demi douzaine de candidats potentiel, mais il est trop en retard pour peaufiner son agent et il le renvoie en arrière plan d'une pensée. L'oracle ne bronche pas et retourne hiberner dans les circuits en silicium que Kit s'est récemment fait installer. Là, au côté d'une collection qui s'étend de jour en our d'agents et d'assistants numérique, Kit essaye de se constituer une boîte à outils sociaux lui permettant de gérer facilement le boulot d'idol.
Il réduit sa vélocité à un vecteur nul grâce à un blocage en arrière, en équilibre sur les pointes de ses patins, après un slalom nerveux entre les longues tables du Naked Brocoli sur lesquelles le repas sera prochainement servi. Il s'accoude au comptoir, autant pour assurer son équilibre que pour épater la galerie. Il a plus de quinze minutes de retard maintenant, Xicha va lui passer un savon. Il salut rapidement Simena qui est accoudée au comptoir, seule, en train de potasser une quelconque documentation d'un système de filtration.
Le visage de l'hybride se fend d'un sourire lorsqu'il l'interpelle, mais il n'as pas le temps de discuter. Pas pour le moment, il doit récupérer les en-cours auprès de Xicha. Qui tiens à toujours le faire en face à face. Il éjecte ses rollers, les glisses rapidement dans le casier qui lui sert de vestiaire, au milieu des fringues qu'il accumule pour développer son iconographie de Coelia. Le miroir écran affiche une des premières photos qu'il a pris avec ses relations, juste après cette soirée dans l'anneau nadiral, les unes dans les bras des autres, dans un des box du Xenon Girl presque désert. Une des dernières fois qu'ielles ont étés toustes ensembles.
"Kit, ramène toi par ici! Tu diras bonjour à tout le monde plus tard." La voix grave de Xicha s'élève depuis l'arrière salle, couvrant les mesures tranquilles de la bande son du lieu. Il s'engouffre dans la cuisine collective, sans prendre le temps de retrouver son souffle et enfile un des tabliers qui sert de seul signal qu'il est maintenant un  des volontaires en service dans le lieu.
Au centre de la pièce, en train de finir de nettoyer un des plans de travail inoxydable, l'imposante silhouette de Xicha l'attend de pied ferme. Ses traits acérés et sa couleur de peau brune affiche sa parenté avec les travailleurs philippins trimballés dans les chantiers spatiaux du consortium, du moins quand celui-ci s'essayait à coloniser le système, avant son repli sur les puits de gravité.
Ses cheveux courts sont retenus par un bandana rouge aux motifs complexe, un hommage à l'insurrection de ces mêmes travailleurs, et à leur sort tragique. Kit le sait parce que Xicha ne manque pas une occasion de le rappeler.
Elle en a le droit, elle fait parti des fondatrices du lieu, et elle et l'une des rares qui continue de venir participer au quotidien. Le Naked Brocoli est, de facto, son domaine. Elle est la mémoire du lieu, sa boussole, la personne qui arrive à garder la tête froide quand les autres paniquent ou sont perdus. Mais aussi celle qui tant à disparaître quand tout fonctionne, pour laisser les autres volontaires et habitués faire du lieu ce qu'ils veulent.
"Je sais que ce n'est pas exactement ce que tu veux faire, lui dit Xicha, enlevant le tablier qui ceint son large corps, mais essayes d'être à l'heure. On est toutes volontaires ici, mais on a aussi des choses à faire en dehors de nos gardes. D'accord?"
Ce n'est effectivement pas exactement ce que veut faire Kit de sa vie. Mais volontaire dans un lieu communautaire est un bon substitut à être une idol. Voire un bon point de départ. Il ne travaille pas r des mèmeplexe à l'échelle de la station, mais il peut déjà commencer à l'échelle de cette petite communauté. Et il peut aussi bosser ses karaoké.
"Désolée. Je vais faire plus attention. Promis." répond-il un peu penaud.
"Fais pas la tête," lui répond Xicha, souriant. "On est pas dans la mine. Mais au moins préviens d'accord? Qu'on sache que tu viens toujours et qu'on te fasse de la place dans le planning."
Il a un peu l'impression d'être toujours considéré comme un voca. Il sait que Xicha essaye de lui expliquer le fonctionnement du lieu, les conventions sociales en vigueur au Naked Brocoli, et que c'est pour que ça se passe au mieux pour lui au sein de cette communauté. Mais il n'est pas obligé d'aimer ça.
Elle lui donne la liste des en-cours, un complément oral de la liste des tâches prévues auquel tous les volontaires ont accès pour les aider à prendre des initiatives. Elle a une façon de prioriser et de lire entre les lignes qui illustre sa connaissance de la communauté qui fréquente son lieu, qui lui permet d'ignorer certaines des demandes urgentes postées par certains, arguant qu'ils n'ont pas vraiment besoin de plus de stock de Flux Shine et qu'ils peuvent boire plus d'Hydrazine.
"Mais te prends pas la tête, finit-elle par ajouter, gère comme tu peux. Et si vraiment ils te prennent la tête, rappelle leur qu'ils peuvent toujours aider hein. Tu n'es pas à leur service."
"Ben si, un peu. Non ?"
"Tu es au service de la communauté, pas de l'un ou l'une d'entre elleux. Je les aime bien hein, mais dès fois certaines d'entre eux sont un peu chiant et essaye d'obtenir plus du lieu qu'ils ne sont censés le faire."
"C'est normal pour un tiers lieu comme ici, on est censé en profiter, se détendre," répond Kit.
"Oui, mais le principe c'est que tout le monde est censé participer un peu. C'est comme ça que les choses marche, Ils vous ont pas appris ça dans vos basiques ?"
"L'économie de flux ? Je suis pas certain d'avoir tout compris. Un de ces trucs pour lesquels je me suis dit que je finirais bien par comprendre."
"C'est pas censé être une théorie économique … " Marmonne Xicha. "Bref, ces lieux sont nécessaires. Et un groupe de volontaires décident de le faire fonctionner. Mais il faut quand même que ce soit quelque chose qu'ielles continuent de faire sur du long terme, que des gens comme toi ou moi, on y trouve notre compte."
"Et pour ça, il faut que tout le monde y trouve son compte, qu'il y ait un échange, même inégale ?"
"En quelque sorte. Tu formules ça de manière très transactionnelle, mais c'est l'idée."
"Tu formulerais ça comment ?"
"Autrement. On en reparle plus tard si tu veux, mais là il faut que j'aille dormir." Répond Xicha, au milieu d'un bâillement. Elle laisse Kit en charge de la cuisine, et de mettre les habitué au boulot afin de ranger un peu le lieu. Rien de prévu pour la ronde, ce qui laisse à Kit le temps de mieux connaître les têtes familières du lieu.
Après avoir procédé à un rapide rangement, et remis en route l'infuseur à café, il passe derrière le comptoir et pose deux tasses du liquide noir, chargé en caféine et au goût étrangement sucré sur la surface en zinc. Une pour lui, et une en face de Simena.
"Alors, Simena, comment vas-tu ?" demande Kit. L'hybride est toujours seule, la plupart des personnes du lieu cherchant à éviter de la dévisager. Kit peut comprendre l'idée, elle est étrange, elle a été seule ces dix huit derniers mois et elle viens de l'espace du Consortium. Tout un tas de raisons qui peuvent expliquer pourquoi tout le monde l'évite, pour ne pas la dévisager. Mais du coup, elle se retrouve encore seule. Et Kit se dit que c'est aussi son rôle de faire en sorte que ce ne soit plus le cas.