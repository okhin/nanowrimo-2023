title:          Octane
ID:             1048
type:           md
summaryFull:    Simena cherche un endroit pour rencontrer du monde, essayer de comprendre comment les choses fonctionnnent,
POV:            11
notes:          {C:0:Simena Von Guyen the Third}
                {P:1:Le Naked Brocoli} Hook
                {C:11:Octane}
                
                {W:81:pupilles adaptives}
                {W:44:torche à plasma}
                {W:36:Chernobyl}
                {W:69:groupes de travails}
                {W:50:coursiers de maintenance}
                {W:79:communicateur}
                {W:82:Pipeline}
                {W:6:Naked Broccoli}
                {W:83:karaoké}
                {W:64:bain collectif}
label:          4
status:         5
compile:        2
setGoal:        1500
charCount:      9337


Octane a besoin de se changer la tête. Encore une réunion des groupes de travail qui ne peut pas avancer, à cause de ces questions sur les journaux d'erreurs. Elle a besoin d'une pause et elle pense se remettre au nomadisme, à aller se poser dans un autre secteur.
Une façon de se former, d'aller étudier le travail d'autres personnes, de leur apporter un éventuel regard extérieur. Mais aussi une façon pour les personnes en charge de la maintenance de fréquenter les lieux sociaux, de faire en sorte qu'ielles ne restent pas trop isolées, à ne s'occuper des machines.
Audrey s'est glissée dans l'atelier, sans qu'Octane ne remarque l'entrée de la brunette, et celle-ci passe ses mains autour de la nuque un peu raide d'Octane, se penchant par dessus son épaule.
"Alors ?" demande-t-elle doucement, regardant l'assemblage de pompes, posant sa tête contre le crane fraîchement rasé d'Octane. Octane ne sait pas encore si Audrey est aussi tactile avec tout le monde, ou s'il y a quelque chose d'autre, mais elle est trop préoccupée pour le moment pour avoir envie de répondre à la question.
"Alors ça fuit de partout dès qu'on dépasse 3 bars de pression. J'ai beau essayer d'imprimer une culotte, la réduction de débit génère trop de pression, et j'arrive pas à compenser ça. Il va falloir que j'aille au bazar trouver des pièces céramique.
"Et ça va prendre du temps. Parce que l'encombrement va être un problème. Trouver la pièce aussi, mais j'ai des idées qui implique de tronçonner des tuyères. Il faudra faire des adaptateurs, mais en passant par des capots externes, je devrait pouvoir m'en sortir."
Elle sent qu'Audrey s'est rapprochée d'elle, l'écoutant et cherchant probablement également une solution. Octane apprécie comment la mécanos réfléchit, elles se complète bien lors des interventions de maintenance, échangeant peu de mots et trouvant facilement comment collaborer sur les opérations de maintenance.
"J'ai un service à te demander, pour Sarah." demande Audrey. Ce n'est pas la première fois qu'elle lui parle de Sarah, une nana avec qui elle a grandi. Une nana avec qui elle a une relation, suppose Octane.
"Vas-y ?"
"T'as suivi l'arrivée du Chernobyl récemment ?"
"J'ai vu ça de loin, oui." Octane aurait préféré avoir plus de temps à y consacrer. Un Chernobyl est quelque chose de rare, et d'étrange. Elle aurait voulu y jeter un œil, regarder les cascades de Takeshi, en charge de gérer la métabolisation non explosive du dioxyde de fluor, des injecteur à particule de tungstène qui convertissent les ultra violets en infrarouge pour surchauffer l'hydrogène ou même voir les verres ablatif de saphir synthétique. Mais au lieu de ça, elle est restée coincée à gérer les réunions de groupes de travail inutiles.
"Sarah s'est occupée de l'approche, reprend Audrey, et d'accueillir lea pilote du vaisseau. Oui, lea. Une seule personne pour gérer ce vaisseau pendant presque dix huit mois. Tu le crois ça ? Bref, Sarah se demande si on ne pourrait pas l'aider à s'intégrer, à bosser. Iel à commencé à faire un peu de maintenance, mais Sarah à un peu peur qu'iel reste trop seule et isolée."
"Sarah t'as filé ses infos de contact ? Je promets rien, mais j'ai besoin de me changer les idées."
Audrey lui transfère les infos que Sarah lui a fournie, et l'embrasse dans la nuque avant de disparaître dans la zone d'atelier qu'elle s'est appropriée pour travailler sur ses propres bricolages électromécaniques.
"J'en déduit que tu ne viens pas avec moi ?" demande Octane, alors qu'elle contacte Simena.
"Nan, je vais voir Sarah dans deux heures. Et puis c'est mieux si t'es toute seul pour ellui parler, non ?"
En effet, c'est probablement mieux si les contacts se font individuellement. Octane n'est pas souvent sollicitée pour aider à intégrer du monde, mais ce n'est pas non plus un cas extrêmement fréquent, les nouvelles venues dans le RAFT étant particulièrement rare.
Octane démonte la ligne hydraulique qui repose sur son établi au son de la ci-bi. Elle envoie un rapide message sur la fréquence de rollcall pour signaler qu'elle passe hors ligne, avant d'attraper son épais terminal de poignet et de se mettre en route vers le point de rendez-vous qu'elle a négocié avec Simena.
Elle rejoint rapidement la pilote du Chernobyl, profitant des trottoirs roulant pour la guider, perdue dans ses pensées. Les galères de groupes de travail, ses soucis hydrauliques, Audrey qui parvient à se glisser dans ses pensées aussi. Elle manque de rater l'arrêt pour sa descente, réussi à ne pas tomber en sortant précipitamment de la rambarde en plastique noir pour revenir sur la partie statique du boulevard radial, et se dirige vers un des étages d'habitation.
Octane se dirige vers les flashs bleus, témoin de l'utilisation d'arc électrique pour souder ensemble les pièces métalliques. Elle se faufile à l'arrière d'un bloc de compartiments personnel, après avoir enfilé ses lunettes à cristaux liquides, atténuant la violence de la foudre utilisées par Simena pour ressouder les tuyaux servant à transporter les fluides de refroidissement du bloc.
"Laisse moi deux minutes, je finis de patcher tout ça et je suis à toi." Lui dit Simena, alors qu'Octane arrive enfin à son niveau. Les arcs électriques zèbre l'épaisse peau de Simena, ses pupilles adaptives faisant écran à l'intense luminosité de sa torche à souder.
"Et voilà. L'injection devrait mieux se faire ainsi, il n'y aura plus de coup de bélier à la mise en route. Et les habitants de l'autre côté de la cloison ne devrait plus entendre vibrer leur bloc froid." Simena mets à jour la demande sur son communicateur de poignet, éteint sa torche et se tourne vers Octane. "Tu voulais qu'on se parle?"
"C'est Sarah qui m'envoie, elle ne veut pas que tu reste trop seule. Donc me voilà. J'ai envie de discuter surtout. Savoir comment tu t'en sors dans ton installation, t'inviter à boire un verre quelque part, te brancher avec les nomades de maintenance, ce genre de choses."
"D'accord." Simena rampe sur le dos pour s'extraire de la galerie technique du compartiment et se redresse. Sa combinaison de travail est déchirée par endroit, tâchée de graisse, et la fermeture est bloquée à mi-hauteur.
"Laisse moi ranger ça et enfiler un truc un peu plus clean que cette tenue de travail, et on bouge." Iel attrape le sac de toile renforcée suspendue à la cloison par un crochet magnétique, et fouille à la recherche de quelque chose dans les quelques affaires qui l'accompagnent partout depuis qu'elle a quitté le Chernobyl. Iel se débarrasse de son bleu de travail et enfile une combinaison plus ample, au couleur du pipeline. Quelque chose qu'iel a extrait d'une des friperies du bazar, et qui soit plus confortable que les combinaison de vols un peu encombrantes qui ont constitué sa seule garde robe pendant sa croisière dans le système solaire.
"On va oú ?" demande-t-iel, fermant la combinaison avec sa large fermeture éclair en plastique, avant d'enfiler la sangle de son sac à dos en bandoulière.
Octane ne peux s'empêcher de voir ce sac contenant manifestement l'ensemble des possession, et d'y voir les ressemblances avec le barda qu'elle emporte avec elle quand elle est en mode nomade. Le type de sac que traîne avec elleux les personnes n'ayant aps encore de lieu fixe pour dormir.
"Tu dors où en ce moment ?" demande Octane, gardant sa réponse à la question de Simena pour plus tard.
"Dans la zone de transfert, sans gravité. Je me suis trouvé un coin pour le moment. En attendant de trouver mieux."
L'équivalent de dormir à la rue. Au moins la zone est chauffée, et elle a accès aux services collectifs, comme tout le monde, mais elle n'as pas de zones privées, d'endroit où personne ne viendra a réveiller.
"Bon. Je peux te laisser l'accès à mon compartiment, au moins pour le moment. Ça sera toujours mieux que ça. Et on va essayer de te trouver une solution un peu plus pérenne que ça, si tu veux."
Octane peut partager le cube d'Audrey. Probablement. Ou installer un hamac dans son atelier. Et puis, de toutes façon, elle voulait se remettre en route. Elle ne va pas utiliser son compartiment pendant un moment.
"On se connaît à peine et…"
"Et je ne te laisse pas dormir dehors ? Ben, j'espère bien. Et c'est juste le temps que tu trouves mieux. Pour le fait de ne pas se connaître… Ben, on est là pour changer ça non ?
"En attendant, on va aller se faire un sauna Je sais pas toi, mais moi il faut que je me débarasse du cambouis. Et après, on ira au Naked Broccoli, manger un morceau, et faire connaissance. Ça te va ? Ils ont un concours de karaoké ce soir, avec un thème et tout."
Simena hésite et reste un peu en retrait. Quelque chose qui ne passe pas.
"C'est un plan cul?"
"Non, c'est pas ce que j'ai en tête. J'ai trop de choses en tête en ce moment pour vraiment penser à ça. T'inquiètes pas."
Ielles se dirigent vers le boulevard radial, et vers la station de vapeur la plus proche. Simena est sur la défensive, mais Octane est trop occupée à jouer la guide touristique pour prêter attention au réflexe d'aliénation qui a vaguement essayé de se manifester quand elle a vu pour la première fois le corps de l'hybride.
Et l'idée de plan cul fait son chemin dans sa tête. Non pas forcément avec Simena, mais peut-être avec Audrey. Mais il faut qu'elles en parle d'abord. Elle lui envoie un message pour lui donner rendez-vous au Naked Brocoli ce soir. Et oui, elle peut venir avec Sarah si elle veut.