title:          Simena
ID:             221
type:           md
summaryFull:    Elle arrive au Naked Brocoli.
                Simena est seule, et cherche à rencontrer des personnes
                
POV:            0
notes:          {C:0:Simena Von Guyen the Third}
                {P:1:Le Naked Brocoli} Plot turn 1
                {P:3:Simena et Xicha} Hook
                {C:11:Octane}{W:6:Naked Broccoli}{W:28:Dispatch}{W:85:hotline}{W:25:Vocas}
                {C:2:Coelia}{C:13:Kat}
                {P:10:Kat}
label:          4
status:         1
compile:        2
setGoal:        1500
charCount:      9463


Le Naked Brocoli est devenue la cantine de Simena. Depuis qu'octane l'y a amené l'autre soir, iel repasse régulièrement dans le lieu organisé comme une cantine collective. Iels ont fait une entrée un peu fracassante la première fois que Simena est venue ici.
Après avoir franchit les portes, qui donnent directement sur le boulevard radial, Octane était montée sur le comptoir et, d'un sifflement strident, avait attiré l'attention de l'assistance. Assistance qui s'était assemblée pour un spectacle de karaoké. Un spectacle d'interprétation libre, une scène ouverte, propulsée par l'utilisation de CARA-0K, ces CARA spécifiquement conçus pour n'être que des flash d'émotions. La partie récréative de ces constructs.
Iels avaient un peu protesté, mais Octane ne leur a pas laissé vraiment le choix. C'est ainsi qu'elle avait présenté Simena à la trentaine de personnes présentes. Un patchwork culturel qui s'exprime par leur façon e s'habiller. Un échantillon de ce qu'il se passe dans la station s'était dit Simena.
Les tenues colorées, courtes, jouant sur les transparences et l'iridescence des plastiques lui avait d'abord fait croire qu'iel n'avait pas sa place ici, parmi ces personnes. Mais il s'est avéré que ce n'est pas nécessairement la faune habituelle du lieu, que ce ne sont que les spectateurices qui sont venus juste pour le spectacle.
Une seconde foule, plus regroupées au fond de la salle, installée aux tables communes, et composée de personnes moins hautes en couleurs, probablement plus vieille, dévisageait d'un œil peut-être un peu paternaliste la foule de nouvelles venues.
Une foule qui lui rappelle, par ses façons de s'habiller, de se tenir un peu en retrait des autres, les vieux syndicalistes dans les bars corporatistes des puits de gravité. Ces vieux de la vieille qui ont parfois un peu trop tendance à tout savoir sur tout et à rester entre eux. Mais qui sont, pour autant, capable de faire bloc contre les forces de sécurité qui essayaient parfois de faire des descente dans les bars.
C'est peut-être ce groupe là qui avait donné à Simena l'impression qu'iel était au bon endroit. Qui l'avait rassuré en parti sur le comportement des habitantes. Iel avait remarqué que c'était également elleux qui l'avaient lea plus dévisagée, que c'était d'elleux que viendraient les jugements, les remarques, les rumeurs, si rumeurs il devait y avoir.
Octane avait terminé son petit discours en signalant que Simena cherchait un compartiment libre où crécher, puis l'avait rejointe, s'installant à une des petites tables, qui occupent l'espace, légèrement en retrait.
La soirée avait repris son rythme, laissant Simena dans l'expectative. Mais iel avait passé une bonne soirée. Pas tellement à regarder les participants et participantes se déhancher au rythme des clips porno-chics des idols, mais plus à reprendre en cœur avec une bonne partie de l'assistance certaines des chansons qu'iel connaît.
Iel s'était rendu compte que les textes n'étaient pas exactement les mêmes. Et que la raison pour laquelle le public était venu, avait plus à voir avec passer ce temps ensemble, à chanter, danser parfois, les uns et les unes contre les autres, que de regarder les interprètes se livrer aux regards jugeant de l'audience. Même si le lieu ellui rappelle définitivement l'un de ces bars de strip dans les puits de gravité, l'ambiance y est très différente. Une histoire de rapport aux performeureuses, de les considérer comme des artistes et non comme un corps à disposition des fantasmes du public.
Simena repense à cette soirée avec Octane quand celle-ci se pose à côté d'iel, au comptoir. Elle lui a donné son code d'accès son compartiment, et ça fait deux jours que Simena a un endroit pour dormir. Octane s'étant installé dans le compartiment d'Audrey, qui passe elle-même du temps avec Sarah, du moins quand Sarah n'est pas avec Kit, un des membres du collectif du Naked Brocoli, qui avait fait sensation avec son interprétation de *Nope* de Coelia l'autre soir.
Simena a un peu de mal à suivre le flux des relations entre toutes ces personnes, mais ce n'est pas la seule chose avec lequel iel a du mal, et ce n'est probablement pas très important.
Octane s'est procurée deux bouteilles d'hydrazine. Une des boissons très en vue au Naked brocoli. Un mélange d'alcool issue de la fermentation de diverse choses, Octane n'a pas pu lui donner le détail précis, et d'un arôme cerise clairement synthétique et très acide. Elle s'en ouvre une et pousse l'autre devant l'hybride.
"Ça va ?" demande-t-elle d'une voix légèrement plus haut perchée, pour couvrir le bruit de la riot pop, qui elle-même cherche à couvrir le bruit de la cuisine collective dont s'élève une agréable odeur de légumes en train de frire.
Une odeur nouvelle pour Simena, habituée aux barres synthétique qui constitue l'essentiel de la nourriture parmi les mineurs de fond. Barres parfumées avec des arômes aussi subtils que l'Hydrazine qu'elle boit. Arômes censées leur donner l'impression de variété, mais échouant complètement à leur rôle.
C'est une des choses que Simena préfère pour le moment depuis qu'iel est arrivé. La nourriture. Il y a toujours quelque chose à manger ou à grignoter à proximité. Quelque chose de différent, mais même ce qui semble être les plats basiques de la station sont, pour ellui, l'équivalent d'un repas gastronomique, servi en haut des dômes de Nuevo Tenochtitlàn, sur Mars.
"Ça va oui." répond-iel. "Je me demande juste comment vous faites certaines choses. Tu m'as dit l'autre soir que, grimper sur la table et demander à tout le monde s'ils avaient pas un compartiment de dispo, était ta façon à toi d'activer ton réseau."
"Oui, et ?" répond Octane.
"Et je me demandais pourquoi vous avez pas, genre, un registre des compartiment disponible, ou quelque chose de similaire. Ça faciliterais les choses, non ?"
Octane avale une gorgée d'Hydrazine. Plusieurs en fait, avant de reprendre.
"Tu as entendu parler du Flux ?"
Simena hoche la tête d'approbation.
"Bon, ben c'est ça. On a essayé d'avoir des listes de choses que les uns recherchent, de ce que les autres peuvent fournir, mais ça fonctionne assez mal. Et ça crée des possibilités de contrôle.
"Surtout pour des choses comme l'espace personnel. On a pas vraiment de compartiments libres. On fait de la place. Mais si personne ne se sert d'un bout d'espace cloisonné, celui-ci est rapidement utilisé par les voisins. Comme zone de stockage, comme espace social, peu importe. Maintenir un registre des compartiments libre risquerait de créer des blocs vides. Ou de ne pas être à jour.
"Et, au final, les résultats sont plus rapides quand on demande autour de soi."
"Pourtant vous avez des choses centralisées. Genre la hotline, ce système de suivi d'incident que vous utilisez dans toute la station pour lister les travaux à faire, ce genre de choses ?" répond Simena.
"On a, entre autres, la hotline. La nuance est importante. Il y a d'autres système parallèles aussi, qui marchent tout aussi bien, qui se complète. On a la ci-bi qui nous permet de se parler directement. On dépend aussi beaucoup des messageries informelles de chaque blocs. Ou peut-etre qu'ils marchent tout aussi mal, selon comment on regarde. Tu peux demander à tous les nomades, il y en a pas un qui pense que la hotline fonctionne comme il faudrait. mais il n'y en a pas non plus deux qui sont d'accord sur comment améliorer les choses. Donc ça reste comme ça.
"Et puis les techniciens savent utiliser des systèmes de logs, de tickets, de gestion d'incidents. On s'en assure pendant les vocas de ceux qui veulent aider. C'est plus de la moitié des compétences nécessaires pour valider ses basiques. Je sais que Contrôle propose un truc similaire. Mais quand tu dois aller couper un système de filtration et attaquer les cloisons au perforateur, rien en remplace aller voir sur place.
"Mais comment vous vous y retrouvez dans tout ça ? Je me perds. Je sais jamais comment trouver une pièce de rechange. La hotline peut me dire ce dont j'ai besoin, si les schémas sont à jour, et clairement ce n'est pas toujours le cas. Mais pour toi c'est si simple…"
"Mmm… Il faudrait que t'en parles avec Audrey à l'occasion. Moi ça fiat dix ans que je fais ça. Forcément, j'ai mes habitudes, des contacts. Et, dès fois, je ne trouve pas ce que je cherche, et ça m'énerve. Mais il faut faire avec. Mais hésites à me demander des coups de mains, j'ai besoin de me changer les idées en ce moment."
Simena termine son hydrazine en essayant de laisser les propos d'Octane décanter. Elle a raison, il faut qu'iel prenne son temps. Iel s'imaginait simplement qu'il y aurait un… mode d'emploi, un guide, quelque chose d'aussi concret qu'un guide d'assemblage des pompes Satseku qui propulsent le sodium en fusion des circuits de refroidissement du Just a Big Dumb Spaceship.
La conversation avec Octane se prolonge une bonne partie de la soirée, et oblique sur les problèmes des groupes de travail. Même si elle essaye de s'en tenir à distance, elle semble développer une obsession autour de ce problème.
Mais à chaque fois que Simena se lève pour aller chercher des choses à manger, qui arrivent régulièrement sur le comptoir, au fur et à mesure que les volontaires les préparent, iel ne peut s'empêcher de sentir sur ellui les regards des briscards, de celles et ceux qui, du fond de la salle, semblent passer leur temps à discuter entre elleux et à lea suivre du regard, ravivant cette sensation désagréable qu'iel est jugée, évaluée, sur son apparence physique étrange.