title:          Simena
ID:             1215
type:           md
summaryFull:    Une discussion avec Kat la met mal à l’aise, la confronte à son étrangeté et à la méfiance latente des habitants de la station vis à vis des créations des puits
POV:            0
notes:          {P:1:Le Naked Brocoli} Pinch
                {C:0:Simena Von Guyen the Third}
                {C:13:Kit}
                {W:6:Naked Broccoli}
                {C:11:Octane}
                {C:28:Audrey}
                {W:69:groupes de travails}
                {C:2:Coelia}
                {P:10:Kit}
label:          4
status:         5
compile:        2
setGoal:        1500
charCount:      7752


"Alors, Simena, comment vas-tu ?" La voix du jeune homme sort Simena de sa lecture. Sa tasse d'ersatz de café froid a été remplacée par une autre, pleine du liquide chaud, auquel Simena commence à développer une légère addiction.
Ça fait deux jours que Simena passe ses matinées au Naked Brocoli, avant de rejoindre Octane et son associée, une ancienne mécanicienne enthousiaste répondant au nom d'Audrey. Le lieu est relativement calme mais les participants ne se sont pas vraiment encore habitués à elle, elle le sent au vide un peu toujours présent autour d'elle. Et Octane semble avoir moins de temps pour elle en ce moment, des histoires de gestion interne des groupes de travail qui l'accapare.
"Honnêtement ?", répond Simena, fermant l'interface entoptique de dispatch depuis laquelle elle essaye de trouver de quoi s'occuper, mais il y a surprenament peu de maintenance à faire en ce moment.
"Je sais pas vraiment." reprend-elle à l'invitation silencieuse de Kit. "Je veux dire, physiologiquement ça va. Je suis un peu perdue, je ne comprend vraiment pas comment tout fonctionne et il faut que je trouve mes repères. Mais au moins, je n'ai pas à chercher à atteindre des quotas stupide, ni à devoir me bourrer la gueule jusqu'à sombrer dans un sommeil sans rêve pour trouver du repos dans un dortoir."
"Mais ?" demande Kit, après un court silence.
"Mais tu es probablement la seule personne qui me parles ici. Enfin, à part Octane. Et Audrey quand je la croise aussi. Mais …"
"Audrey? Petite brune aux yeux verts qui ne tiens pas en place ?"
"Euh ouais, je crois… pourquoi ?"
"Rien, c'est une copine à moi. C'est marrant que tu l'ai croisée. Pardon, je t'ai interrompu. Tu parlais du fait que personne ne te parle justement ?"
"Ouais… C'est… Je sais pas. J'imaginais pas les choses comme ça. Et il y a plein de trucs dont j'aimerai parler mais…" Simena baisse les yeux et fixe son ersatz de café qui refroidit entre ses larges mains. "Mais je ne pense pas qu'en parler suffise."
"Tu peux essayer de m'en parler, j'ai que ça à faire," répond Kit.
"Ne le prend pas mal, mais non, pas avec toi du moins," répond Simena rapidement, levant les yeux de son café. Elle n'a pas besoin de l'aide de quelqu'un qui n'a probablement qu'à peine plus que la moitié se son âge.
Enfin, selon comment on compte, se prend-elle à penser. Elle n'aime pas trop réfléchir a "naissance" c'est toujours un peu étrange. D'autant qu'elle sait qu'elle est plus jeune que son corps, enfin, elle croit.
"Tu penses à quoi ?" demande Kit après quelques instants.
"À mon âge. Et donc, à ma naissance, si on peut appeler ça comme ça." Elle termine sa tasse de café d'une traite. La légère acidité traverse sa gorge, pour ne laisser que l'amertume du mélange d'algue séché qui compose cet ersatz caféiné.
"Au fait que selon comment on compte, j'ai quinze ou trente cinq ans."
"Comment ça ?"
"Comment tu comptes ton âge toi ? À partir du moment oú tu es sorti de l'utérus de quelqu'un et que ta conscience à commencé à se développer, non ?"
"Ouais, comme tout le monde je pense…"
"Sauf que moi, mon organisme est sorti d'une matrice utérine artificielle il y a quinze ans, avant de vieillir rapidement en cuve jusqu'à l'âge maximisant la force de travail par rapport aux soucis de santé. Et mon esprit à été assemblé et ajusté à grand coup de CARA pour me donner la maturité d'une personne d'environ une trentaine d'année. Du coup… Je ne sais même pas quel âge j'ai."
Kit reste silencieux. Attentif à Simena, mais silencieux. Il essaye de manifestement s'occuper les mains en rangeant le comptoir pourtant déjà impeccable.
"Donc voilà, je peux pas te parler de mes problème et espérer en tirer quelque chose d'autres que de la gêne ou de la pitié. J'ai besoin de quelqu'un qui a un minimum de référentiel commun, et je ne suis pas certains de pouvoir trouver ça ici." Simena entend sa voix dérailler. Ses émotions la rattrape. Et elle ne sait pas vraiment comment gérer tout ça, comment s'aider des autres. Alors elle sert les poings et se persuade que c'est une forme de colère, parce qu'elle connaît bien la colère, c'est rassurant d'être en colère.
Kit se prépare à répondre, juste avant que Simena ne le coupe dans son élan.
"Et non, tu ne comprends pas. Ce n'est pas ta faute, mais tu ne peux pas comprendre. Pas maintenant en tout cas," finis-t-elle dans un soupir. Reprenant le contrôle de ses émotions, rangeant sa colère dans sa boîte, espérant que celle-ci ne va pas jaillir hors de contrôle.
Kit reste silencieux. C'est la première fois en deux jours qu'il voit Simena dans un tel état de tension. Habituellement ielles parlent de tout et de rien en partageant un coin de comptoir. Et là, alors que Simena semble avoir désespérément besoin d'aide, il se retrouve paralysée émotionnellement, incapable d'agir. Son empathie est plaquée au sol par ses propres émotions et sans comprendre pourquoi, il a une réaction de peur.
Simena perçoit la crispation de Kit, mais c'est le silence dans la salle qui lui fait réaliser à quel point leur conversation, et sa voix, ont escaladé, au point d'attirer l'attention des personnes présentes dans le bar.
"Tout va comme tu veux Kit ?" demande quelqu'un, un habitué. "*Elle* ne te pose pas de problème ?" ajoute-t-il, mettant une emphase équivoque sur le pronom. Elle sait qu'il implique une injure qu'il n'a pas eu l'intelligence de trouver. Ou le courage de formuler.
Elle commence à pivoter, pour faire face a la personne. Pour lui demander s'il a un problème, s'il veut en parler, pour le confronter. Elle sent sa colère pousser contre le couvercle de la boîte dans laquelle elle viens de la ranger, elle sent qu'elle peut jaillir à tout moment. Mais une main délicate, mais ferme, la retiens dans son mouvement.
Kit intercepte son regard. Et dans ses yeux, Simena retrouve le regards de Coelia, elle retrouve quelque chose qui l'ancre dans un espace isolé des autres. Dans un endroit où il n'y a plus qu'elle. Et Kit.
"Il n'en vaut pas la peine" lui dit-il, doucement, sortant de sa stupeur. "T'inquiètes pas, tout va bien," répond-il à la question posée, d'une voix posée. Une voix travaillée par des heures de karaoke, qui couvre tout les chuchotements et ricanements qui commencent à se faire entendre. Une voix qui dissuade la colère de Simena de jaillir hors de sa boîte.
"Fais pas attention à eux", dit Kit à Simena, de sa voix normale, pas de sa vie d'idol, il a rompu le contact physique entre elleux et il pousse devant Simena une autre tasse d'ersatz de café, reprenant la conversation là où ielles l'avaient laissées.
"J'ai l'habitude, répond Simena. Je suis juste… "
"Déçue ?"
"Un truc du genre. Je … m'attendais pas à subir ce genre de merde ici."
"Il faut que tu parles à Xicha."
"*Chica* ?"
Kit étouffe un petit rire.
"Non, Xicha, Dzi-Tcha. Même si, je suppose, qu'elle trouvera certainement ta prononciation pleine d'un certain charme," explique Kit, retrouvant son calme. Simena laisse échapper un soupir, encore une des façons dont elle est mise à l'écart. Le parler dans les mines d'Helium-3 est marqué par les idiomes et prononciation mexicain du consortium. Sur RAFT, l'idiomatique est moins marqué par une seule culture, se composant d'un créole d'accent et de vocabulaire qu'elle a du mal à comprendre par moment.
"D'accord. Xiii-cha, reprend-elle. Pourquoi est-ce que je devrais la voir ?"
"Parce qu'elle a toujours été là. Parce que tu parlais de quelqu'un qui pourrais avoir un bout de référentiel commun avec toi. Et elle n'arrête pas de répéter qu'elle était là avant tout le monde, que ses parents ont aidés à construire cette station."
"Elle a quel âge ?"
"Faudra que tu lui demande, ça fait parti des secrets du Naked Brocoli. Mais parles-lui. D'accord ?"
"J'y penserai," répond Simena, en engloutissant une gorgée de son café. "D'accord, j'y penserai."