title:          Giuse-P
ID:             233
type:           md
summaryFull:    Simena décide de ne pas rester. 
POV:            4
notes:          {C:17:Yas}
                {C:4:Giuse-P}
                {C:12:Tik}
                {P:2:Le Monster Bash} Hook
                {W:11:Monster  Bash}
                {W:25:Vocas}
                {W:26:Brassard}
                {W:14:Idols}
                {W:74:Zone de transfert}
                {C:5:Mark-V}
                {W:105:le mouvement}
                {P:8:Émancipation de Yas}
label:          4
status:         5
compile:        2
setGoal:        1500
charCount:      9966


Giuse-P et Tik sont installés dans la zone VIP du Monster Bash. Yas est assise juste à côté de Giuse-P, zonant dans des recoins de l'infosphère en utilisant les relais pour vocas, ne suivant que d'une oreille distraite cet échange interminable entre Giuse-P et son lieutenant.
"T'es sûr de toi ?" demande Giuse-P, suite au rapport que viens de faire Tik sur sa filature de Simena.
"Absolument. La cochonne est allée au Xenon Girl et est repartie à peine dix minutes après. Manifestement en choc," confirme Tik.
"Et on sait qui iel a vu à l'intérieur ?" demande Yas, sortant de sa réserve.
Tike ne répond pas. Il a décidé d'ignorer complètement Yas. C'est une façon pour lui de montrer sa défiance vis à vis de Giuse-P, son désaccord vis à vis du choix de celui-ci de maintenir une proximité avec la voca qui va au-delà de l'apport stratégique de celle-ci.
Giuse-P est au courant du fqit que Tik remette en cause son commandement. Il n'a pas besoin que Yas le souligne en prenant part à ces conversations, ce n'est pas son rôle. Elle devrait le savoir.
"Et donc, elle a vu qui à l'intérieur ?" répete Giuse-P, s'appropriant la question de Yas, et posant sa main sur la cuisse de la voca, lui intimant ainsi de rester à sa place.
"Aucune idée. Tu sais comment sont les volontaires avec la vie privée de leur client et clientes… " finit par répondre Tik, avalant une gorgée du thé tiède qu'il apprécie.
"Donc tu n'es pas resté à attendre qui en sortait?"
"Je ne peux pas suivre discrètement tout le monde, G. Désolé."
"D'accord, d'accord. Essaye de savoir qui était avec elle. Même si j'ai une petite idée."
Giuse-P fait semblant. Tik essaye de se soustraire à ses ordres, alors il doit réaffirmer son contrôle. Il doit rétablir le fait que c'est lui qui a les idées, que c'est lui qui a démarré toutes ces histoires autour de la cabale. Que c'est lui qui est tombé sur cette vieille base de donnée de Things Speaks alors qu'il cherchait des archives de code.
Mais Giuse-P n'a pas vraiment d'idée de qui Simena a pu aller voir. Personne ne veut vraiment lui parler, sa persona est devenue trop toxique. Ce qui expliquerait d'ailleurs que ce rendez-vous ait eu lieu quelque part d'aussi désert que le Xenon Girl.
"Tu penses à qui, si je peux me permettre ?" demande Tik. Essayant, comme le joueur de carte qu'il est, de déterminer si Giuse-P bluffe.
"C'est forcément un des agents les plus efficaces du système capitaliste. Un élément qui essaye probablement de tuer dans l'œuf une menace." Giuse-P laisse flotter ces quelques mots dans l'air, comme s'il n'avait pas terminé sa phrase, attendant que Tik ne comble les trous de son raisonnement, d'une manière exploitable.
"Une idol ?" suggère Yas.
Giuse-P resserre sa prise sur la cuisse de l'idol, ses doigts enfonçant la toile tissée bleue dans la peau, qu'il sait être parfaite, brune et lisse, de la voca, ses phalanges commençant à blanchir.
Il sent que Yas essaye, discrètement de se dégager de son emprise, et essaye de retenir un cri de douleur, tentant de résister à la soumission qu'il exerce sur elle, mais sachant qu'elle ne peut pas vraiment s'échapper de son aura à lui.
"Une idol," reprend Giuse-P, s'appropriant l'idée de Yas qui se tortille légèrement sur la banquette, essayant de trouver une position moins douloureuse. "Mais pas besoin de savoir laquelle pour le moment, l'idol va de toutes façon vouloir en parler, on aura confirmation de ça bien assez tôt." Une notification importante interromp son train de pensée. Un nouveau message de Simena.
"Salut. Tu parlais d'un endroit où je pourrais être tranquille ? On peut en parler quelque part ?"  La nouvelle manque de le faire sursauter, et il relache sa prise sur Yas. Il répondra à Simena une fois cette réunion terminée, et après avoir recadré Yas. Iel peut attendre un peu, ça ne peut que faciliter son recrutement par la suite.
"Bien," reprend Giuse-P, s'adressant à Tik. "On va passer à une phase plus active de recrutement de Simena. On en est où sur la récupération de son agression ?"
"Ça avance, le changement de sens du message passe bien, et on prépare une deuxième vague de soutien. Tu me fais signe, et on mets en route nos agents pour qu'ils orientent le discours contre la cabale."
Parfait, se dit Giuse-P, il décide du timing de cette opération. Ils ont fait pas mal de progrès ces derniers temps, et une masse de plus en plus importantes de mèmes les impliquent lui et son mouvement. Ils occupent l'espace informationnel bien au-delà des seuls espaces interstitiels maintenant, bien au-delà des simples messages dessinés sur les murs. Ils sont la conversation. La seule conversation qui se produise à l'échelle de la station.
Giuse-P congédie Tik, le laissant retourner se coordonner avec ses équipes. Puis, une fois celui-ci ayant quitté l'espace surplombant le Monster Bash, sans regarder Yas, sans la lâcher, il se mets en tête de lui faire comprende la leçon qu'il veut qu'elle retienne à tout prix.
"Je t'ai dit quoi à propos de ta place dans les réunion stratégique ?"
Yas reste silencieuse. Giuse-P ressert sa main sur la cuisse de la voca. Cette fois il n'a pas besoin de se retenir, ce n'est pas grave si elle crie, il n'y a personne pour le voir.
"Tu me fais mal !" crie-t-elle, un vole de peur dans la voix.
"Ce n'est pas ma faute, tu me forces à agir comme ça en ne restant pas à ta place. Et tu sais ce que ça veut dire."
Il répète sa question, laissant une colère de moins en moins contenue s'exprimer.
"Que je dois écouter, en silence. Noter les réactions, mais rester silencieuse."
"Tu décides donc scemment de ne pas obéir ? De ne pas faire ce que j'attends de toi ? Tu veux que je perde mon temps, mon temps précieux, du temps que je devrais concentrer à calmer les débuts de mutenrie, à te rappeler ce que tu sais déjà au lieu de te comporter comme une assistante ?"
Il est entouré d'incapable. Que Yas ne sache pas se tenir, il peut comprendre. Elle est encore très jeune, il peut l'excuser. Mais elle doit apprendre. En revanche, Tik, lui, n'a pas cette excuse du manque d'expérience, de la jeunesse. Et pourtant, il ne semble pas capable de suivre ses ordres sans le questionner en permanence.
Et surtout, Giuse-P ne peut pas y faire grand chose, il ne peut pas contraindre physiquement Tik à lui obéir. Alors, il se concentre sur Yas, dont le rythme cardiaque semble accélérer alors qu'il réajuste sa prise sur sa cuisse, remontant vers le haut, vers les sangles du harnais de nylon noir qui enserre ses formes discrètes.
C'est son rôle. De faire en sorte qu'elle apprenne, qu'elle ne reproduise pas ses erreurs. C'est elle qui est venu lui demander de s'occuper d'elle. Il remarque qu'elle s'habille comme il le lui suggère, ne portant qu'une salopette de toile bleue, floquée du logo des  Bonecos de Empurar, et d'un harnais.
Sa frustration de ne pouvoir contrôler Tik, alimente sa colère, et se tourne vers Yas, la transformant à ses yeux en un objet à posséder et non plus en une apprentie. Il laisse cette émotion prendre le contrôle, et la jette brutalement sur la banquette élimée qu'occupait Tik encore quelques instants auparavant.
Elle essaye de dire quelque chose, réajustant ses habits, pleinement consciente que son intimité n'est protégé du regard acéré de Giuse-P que par la fine toile de cette salopette. Qui ne suffira jamais à accomplir cette tâche sans avoir une brassière, ou un T-shirt. Mais Giuse-P n'écoute pas ce qu'elle balbutie. Il pourrait contrôler sa colère, verbaliser les choses calmement, respirer et faire redescendre sa tension. Il pourrait. Mais il n'a pas envie.
Il dévisage Yas immobile, hypnotisée par son regard. Son regard qui analyse ses formes, superpose mentalement les photos d'elle qu'il continue de lui voler, la déshabillant mentalement, imaginant la sensation de sentir sa peau sous ses doigts.
D'un pas il franchit l'espace qui les sépare et il l'enjambe, s'asseyant sur son abdomen, l'empêchant de bouger. Il la sent entre ses jambes, qui se laisse presque faire, prise de panique son esprit ne lui permet pas de se défendre, de s'échapper. Il la possède complètement, ses yeux reflète une incompréhension, une peur, sa respiration est agitée, irrégulière, son rythme cardiaque s'envole, pulsant du sang vers ses joues, la faisant rougir. Elle m'attend, pense-t-il, lui attrapant les poignets et la clouant au sol de son poids.
Elle ferme les yeux et il la sent se tendre comme un arc. Une sorte de résignation prend la place de sa peur. Il a gagné, elle a compris la leçon, il peut faire d'elle ce qu'il veut. Alors, il sait qu'il est allé assez loin. Il ferme les yeux, relâche son étreinte et reprend le contrôle de sa respiration et de sa colère.
En quelques secondes sa rage a été purgée de son système, et elle reste immobile à sa place. Il se dégage d'au-dessus d'elle et lui tend la main. Il effleure sa poitrine et il la sent se raidir à son contact, sortir de son état de choc. Il laisse un sourire se dessiner sur ses lèvre et il la contemple. Il se sent bien, apaisé. Il l'aide à se remettre sur pied et, avant de la congédier, il lui rappelle à quel point il a besoin d'elle, et qu'il faut vraiment qu'elle fasse plus attention à rester à sa place. À quel point c'est important pour lui.
Maintenant qu'il s'est occupé de ses subalternes, Giuse-P peut se concentrer sur les tâches importantes.
"Salut Simena, j'espère que tu tiens le coup. On peut se parler oui, retrouves-moi dans la zone d'accueil, juste au niveau du bras Sud. Il y a une pergolas qui est généralement tranquille. Je devrais pouvoir y être rapidement. Dans quatre heures."
Il faut la faire attendre, il faut qu'il se fasse désiré, il faut qu'il soit son dernier recours s'il veut pouvoir l'intégrer à sa lutte.
S'assurant qu'il est bien seul, il rentre s'isoler dans la suite VIP qui est son espace personnel et attrape son appareil photo, et regarde les phots de Yas qui commencent à s'accumuler sur l'espace mémoire, calmant sa frustration et son envie d'elle en la projetant sur les murs écrans qui entourent son gigantesque lit.