title:          Simena
ID:             2025
type:           md
summaryFull:    Giuse-P lui parle d’autres personnes, comme elle, qui voudraient la rencontrer	Simena est invitée au Monster Bash par GiuseP
                
POV:            0
notes:          {P:1:Le Naked Brocoli} Plot turn
                {C:4:Giuse-P}
                {C:0:Simena Von Guyen the Third}
                {C:8:Xicha}
                {W:6:Naked Broccoli}
                {C:11:Octane}
                {C:2:Coelia}
                {W:105:le mouvement}
                {W:82:Pipeline}
                {W:10:Consortium}
                {P:2:Le Monster Bash}
label:          4
status:         3
compile:        2
setGoal:        1500
charCount:      6785



Simena s'est installée dans a zone d'accueil, flottant librement entre les anneaux de la station, face à une des coupoles permettant de contempler le vide spatial, ou de voir les plus petits des vaisseaux s'approcher et s'arrimer aux docks de Rien à Foutre de ta thune.
Iel est arrivé avec plus de deux heures d'avance au rendez-vous que lui à donné Giuse-P et l'attend. Iel n'a rien de mieux à faire de toutes fącon que de regarder les navettes et les drones qui fréquentent le voisinage immédiat de RAFT.
Iel n'a des nouvelles que d'Octane, quand celle-ci a deux minutes à lui consacrer, ce qui se fait de moins en moins fréquemment. Les équipes de maintenance l'ont un peu mis de côté, et de toutes façon iel n'a pas vraiment envie de bosser pour des personnes qui lea déteste. Sarah a essayé de reprendre le contact avec iel, mais la désyndicalisation massive et l'éclatement de la communauté globale de RAFT en clades mémétique plus ou moins en conflit les uns avec les autres compliquent la vie de Contrôle, et il faudrait qu'iel reprenne contact avec Xicha mais iel n'arrive pas à passer outre ses émotions conflictuelle autour de cette situation. Et elle n'a pas non plus cherché à prendre contact, se rappelle Simena, pour valider sa décision de en pas reprendre contact.
Ça fiat maintenant plus d'une demi heure de retard pour Giuse-P. Elle reste dubitative vis à vis de cette personne, notamment parce qu'il est resté très évasif dans leurs quelques contact, et qu'il n'as, pour ainsi dire, pas de présence en ligne. Même en allant se connecter directement au serveur qui héberge ses médias sociaux, elle n'a pas trouvé grand chose sur cette personne.
Mais au moins, il fait parti des quelques personnes qui ne partagent pas la vidéo de son agression. Il n'a pas non plus ressenti le besoin de donner son avis, de commenter, de lea défendre sans lui demander si iel voulait être défendue, de lea déposséder de son histoire.
Giuse-P finit par arriver. Il a plus de quarante minutes de retard.
"Salut," lui dit-iel alors qu'il s'assied en face d'ellui, lea rejoignant dans l'espace de la coupole d'observation.
"Pas top," répond Simena. Puis avant que Giuse-P ne puisse poser d'autres question du genre, ielle ajoute : "J'ai pas envie d'en parler."
"D'accord, répond Giuse-P. Je respecte ça." Comme s'il avait le choix se dit Simena. Quelque chose dans sa voix lea mets sur la défensive. Iel ne saurait dire pourquoi, mais il à l'air de trop réfléchir à ses mots, de parler comme s'il ait persuadé d'être enregistré, comme un acteur.
"Tu voulais qu'on parle ?" lui demande-t-iel.
"Ouais. Tu me disais l'autre fois en sortant du Naked Brocoli, que je n'étais pas seule. Qu'il y avait d'autres personnes comme moi ? Tu voulais dire quoi par là ?"
Giuse-P prend quelques instants pour répondre. Pas tout à fait une seconde, mais quelque chose de perceptible, comme une légère latence. Comme s'il devait lire un prompteur.
"Pas comme toi, dans le sens où il n'y a personne d'autre comme toi. Mais… Un petit groupe de personnes qui se posent des questions sur l'état de notre bonne vielle station, et de comment elle est en train de péricliter, de se déliter. Un groupe de personnes a qui on a menti en leur disant que tout allait bien, que leur vie était bien meilleure qu'avant. Un groupe de personnes abandonnés par le système qui régit nos vies."
"Arrêtes avec les conneries populiste d'accord ? Vous ne pouvez pas savoir ce qui m'est arrivé ou m'arrive. C'est _ma_ vie, _mes_ expériences, vous ne pouvez pas me les prendre."
Simena soupire.
"Pourquoi tu es venu me chercher au Naked Brocoli ?" demande-t-iel, donnant à Giuse-P une chance de s'expliquer clairement, d'arrêter son discours d'insurrectionniste.
"Tout le monde n'est pas forcément d'accord avec la façon dont se passent les choses ici," commence Giuse-P, avant de réorienter son discours alors que Simena lève les yeux au plafond en soupirant.
"_Je_ ne suis pas d'accord avec ce qu'il se passe, avec la façon dont les personnes te traitent. Je ne suis pas d'accord avec les condamnations à l'emporte pièce et l'acharnement dont tu es la cible. Et je veux juste t'offrir la possibilité de ne plus être seul, de rejoindre un groupe d'autres personnes qui sont aussi indignées par cette campagne de haine à ton encontre."
L'idée de ne plus être seul est tentante. Mais Simena est méfiant, iel a déjà été trahi par des personnes qu'iel estimait proche, comme Xicha, et iel n'a pas envie de replonger tout de suite.
Mais iel ne peux pas non plus rester à l'écart à ne rien faire. Et même si quelque chose dans l'attitude de Giuse-P lea perturbe, quelque chose sur lequel iel n'arrive pas à mettre le doigt, iel n'a pas forcément beaucoup d'autre choix que d'essayer, encore une fois, de faire confiance à quelqu'un.
"Bon. J'entends ce que tu dis," dit-iel, "Je ne suis pas encore complètement vendue à ton idée de … lutte contre le système."
"Tu dis ça comme si c'était une mauvaise chose," répond Giuse-P.
Simean lea fixe d'un regard sous entendant qu'il n'a aucune idée de ce dont il parle.
"Bref, on en reparlera plus tard. Mais mettons que j'ai besoin de compagnie et que je veuille rejoindre ce groupe dont tu parles. Ça se passe comment, concrètement ? Vous vous réunissez dans les bas-fond d'un bar mal tenu dont l'adresse est communiqué par message codé à destination des seuls initiés ?"
Giuse-P ignore les piques de Simena, ce qui est un bon point pour lui note-t-iel, il n'y a rien de pire que les personnes qui se prennent trop au sérieux.
"Non, pas toi. Toi tu as un passe-droit. Tout le monde sait qui tu es, donc il suffit juste que tu te pointe à cette salle d'arcade, le Monster Bash, et que tu demande Giuse-P. Ou Yas, une des habituée du lieu."
"Aussi simplement ? Je pointe mon groin là-bas, et on va m'accueillir à bras ouvert ?"
"Oui," répond Giuse-P, un peu étonné. "Pourquoi ça devrait être plus compliqué?"
"Je sais pas. L'expérience."
Simena laisse l'idée de rejoindre un groupe, de continuer de faire confiance a RAFT ou, du moins, à une partie d'entre eux, faire son chemin dans sa tête. Iel n'a de toutes façon plus grand chose à perdre, et elle connaît déjà les groupes anti-systèmes, ça lui parle. Ils sont épuisants et n'obtiennent que rarement des résultats, mais au moins, ils prennent soin les uns des autres. Tant qu'on respecte le dogme local, mais ça ne peut pas être pire que ce qu'iel a connu dans les puits de mine.
Et c'est ce dont iel a besoin. Quelque part dans ses besoins sociaux, génétiquement induits, il y a celui de faire partie d'un groupe, de se sentir entourée, de ne pas affronter seule le vide spatial.
"D'accord," dit Simena." D'accord. J'en suis."
Giuse-P lui donne les détails pour rejoindre le Monster Bash, et lea laisse seule alors qu'il quitte la zone d'accueil, un léger sourire sur ses lèvres.