title:          Yas
ID:             234
type:           md
summaryFull:    Simena quitte le Naked Brocoli pour aller au Monster Bash
POV:            17
notes:          {C:0:Simena Von Guyen the Third}
                {P:1:Le Naked Brocoli} Resolution
                {P:8:Émancipation de Yas}
                {W:11:Monster  Bash}
                {C:17:Yas}
                {C:12:Tik}
                {C:4:Giuse-P}
                {W:102:vitres polarisées}
                {W:25:Vocas}
                {W:26:Brassard}
                {W:122:automaquillage}
                {W:123:WD 40}
                {W:105:le mouvement}
                {P:8:Émancipation de Yas}
label:          4
status:         5
compile:        2
setGoal:        1500
charCount:      7464


Yas est passée par le sauna, et à fait une escale dans la friperie qui partage le palier avec le Monster Bash, en essayant de rationaliser ce qu'il s'est passé avec Giuse-P. Maintenant propre et vêtue d'une combinaison passée de mode, mais exceptionnellement confortable, elle jette sur son épaule un blouson de pilote tout en couleurs fluo et rentre dans la galerie d'arcade qui est devenue, bon an mal an, son domicile.
Quelques personnes sont en train de discuter dans la partie commune. Quelques unes des dernières joueuses à venir encore passer du temps sur les console de Dansudansureboryūshon, et à frapper de leurs pieds le contrôleur-piste de danse, enchaînant les combinaisons requérant un niveau de précision et de réflexe difficile à atteindre sans stimulant quelconque. Une forte quantité de saccharose dans ce cas précis.
Yas les salue rapidement. Elle les connaît et sont, elles aussi, des vocas. Elles constituent l'une des équipes de poursuites qui parcourent ensemble le boulevard radial sur leurs rollers, lancées à pleine vitesse. Elle leur signale qu'elle va bientôt avoir besoin de la place, qu'il va falloir qu'elles partent. C'est probablement ce qui attriste le plus Yas, au final, dans cette histoire. Le fait que le mouvement de Giuse-P se fasse au dépend des habituées du lieu.
Mais elles connaissant la chanson. Elles la rassurent en lui disant qu'il n'y a aucun problème, et qu'elles videront les lieux pour aller s'entraîner et se faire un sauna ensemble, dès qu'elles auront finit leurs verres. En attendant, elle s'occupe et commence à préparer les lieux.
Elle fait attention à ce qui se passe autour d'elle, au fait que Tik est, encore une fois, en retard. Aux clins d'œil que lui lancent les filles en partant, et aux "à bientôt" à son attention qu'elle devine sur leurs lèvres. Mais surtout aux autres qui arrivent les uns après les autres. Ceux qui n'ont pas l'allure des habitués des arcades. Ceux qui discutent à voix basse et se jettent des regards en coin. Elle fait attention à tout ça et prend des notes. Elle n'oublie pas qu'elle est les yeux de Giuse-P quand il n'est pas là. Et elle a compris le message : il ne faut pas le décevoir.
Personne n'est là pour l'aider à organiser l'espace pour l'arriver de Simena. Pas même les deux mecs arrivés avant elle, plus occupés à se rouler des pelles qu'à aider de manière constructive. Alors elle note, soupire, réajuste sa combinaison comme Giuse-P lui a dit de faire. Exciter et paraître vulnérable, pour être sous-estimée et considérée inoffensive. Et, de cette position de faiblesse apparente, avoir la possibilité de neutraliser son adversaire en une seule attaque. C'est, du moins sa théorie.
Elle retourne à l'étage, vérifier et préparer les réassorts stocks. Il s'est approprié la mezzanine qui surplombe la salle commune, dissimulée derrière les vitres polarisées. C'est l'espace de fête du Monster Bash, là où les aficionados d'arcades venaient se détendre après une longue session d'immersion virtuelle. Des couchettes bordent cet espace, séparés par des placards encastrés, reprenant l'architecture de ce qui devait être les quartiers d'un détachement de sécurité. C'est dans ce lieu qu'elle s'est installée.
Que Giuse-P s'est installé en fait. Qu'il s'est approprié cet espace commun pour organiser son mouvement, sa lutte. Les habitués du Monster Bash n'ont pas beaucoup protestés et l'ont plus ou moins laissés faire. Elle a droit à un espace près de lui, parce qu'elle est son assistante. Mais aussi parce qu'elle est censée être disponible à tout instant, sans vraiment de temps pour elle. Ni de personne avec qui parler.
Les autres vocas l'ignorent depuis cette soirée karaoké. Cette soirée dont les images doivent encore circuler entre les communicateurs de ses "camarades" vocas. Tout ça parce qu'elle à fait une crise de dissociation à cause des CARA. Ou peut-être du mélange CARA et euphorisant. Ou peut-être qu'elle voulait agir de la sorte. Quoi qu'il en soit, elle avait déjà du mal à se sentir acceptée par les autres. Depuis cette soirée là, c'est devenu impossible, et elle préfère rester seule.
Seule, elle valide ses basiques les uns après les autres. Minimisant ses contacts avec les autres vocas, et essayant de se demander combien de temps elle doit garder son brassard avant de décider que la station puisse la considérer comme une adulte à part entière.
Et Giuse-P veut qu'elle garde son brassard, il a été assez clair à ce sujet. Les fonctions d'anonymat qu'il procure, empêchant les enregistrements identifiables ou le partage d'images sur l'infosphère publique, lui sont particulièrement pratique. Yas en arrive même à se demander si ce n'est pas la seule chose qui l'intéresse chez elle.
Il lui a demandé de tout faire pour que son invitée se sente bien accueillie. C'est pour ça qu'elle a fait des efforts sur sa tenue, qu'elle a souligné les traits de son visage d'un maquillage voyant, la vieillissant et lui donnant l'impression d'avoir presque vingt ans, qu'elle s'est assurée que les armoires réfrigérées étaient pleine, et qu'elle est devenue un peu impatiente maintenant que tout est prêt. Pas le temps démarrer une partie de quoi que ce soit, même si ça lui ferait du bien. Simena doit arriver d'un instant à l'autre.
Elle redescend de la mezzanine pour constater qu'une trentaine de personnes du mouvement se sont déjà installés. Les mange debout ont été pris d'assaut, et les lumières noires font ressortir les motifs des peintures corporelles sensibles aux ultra-violets qu'ils utilisent pour se reconnaître entre eux, pour se créer un ersatz de culture. L'assemblée est compactée dans un espace fait pour héberger une dizaine de personnes leur donnant l'impression d'être deux ou trois fois plus nombreux qu'ils ne le sont réellement.
Elle sourit, mais personne ne lui prête attention. Elle sort d'un des frigos une paire de bouteille de WD 40, en ouvre une, et s'installe à sa place, contre un des piliers. Elle peut voir tout le monde d'ici, et personne ne la remarque peu importe que sa combinaison ne l'habille qu'à peine, et que son blouson ne fasse que le mettre en évidence. Elle est invisible à leurs yeux.
Puis, face à elle, l'impressionnante carrure de Simena franchit la double porte battante du Monster Bash.
L'hybride scanne du regard les participants du mouvements. Ceux qui sont perdus, se disent oubliés par le système. Ceux qui ne reçoivent pas l'attention qu'ils pensent mériter. Elle intercepte le regard de Yas, et iels maintiennent ce contact quelques instants, avant que Yas ne détourne les yeux, cherchant du regard quelque chose dans la foule.
Simena ignore Tik qui a essayé de l'accueillir, et iel se dirige droit vers la table de Yas. Est-ce que parce qu'elle est la seule vocas ? Ou la seule meuf ? Ou la seule personne qui ne la dévisage pas ? Probablement un mélange de tout ça.
"Salut," lui dit l'hybride. D'une voix légèrement éraillée, de celle de quelqu'un qui n'a pas beaucoup parlé ces derniers jours. Iel est vêtue d'une combinaison de travail, dont la toile est tendue sur son impressionnante carrure. "Je suis …" 
"Simena," l'interrompt Yas, se décidant à plonger son regard dans le sien. Son œil droit est entouré d'une tâche brune, qui se prolonge jusqu'à la base de son oreille. "Giuse-P n'est pas encore là, mais il m'as dit de m'occuper de toi en attendant qu'il arrive."
Elle ouvre la seconde bouteille de WD40 et la tend à Simena, l'invitant à poser son sac et à se détendre.
"Je m'appelle Yas au fait."