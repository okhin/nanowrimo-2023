title:          Giuse-P
ID:             3810
type:           md
summarySentence:Giuse-P réalise l'état du mouvement
POV:            4
notes:          {C:4:Giuse-P}
                {C:17:Yas}
                {W:105:le mouvement}
                {W:11:Monster  Bash}
                {W:102:vitres polarisées}
                {W:79:communicateur}
                {P:8:Émancipation de Yas}
label:          4
status:         2
compile:        2
setGoal:        750
charCount:      5644


Le réveil est difficile. Un peu comme tous les jours se dit Giuse-P, s'extrayant des brumes de ses rêves. Rêves desquels il ne garde que des images mentales, des fantasmes dans lesquels il impose ses envies au corps obéissant de Yas.
Avec une certaine déception, celle du retour brutal à la réalité, il ne la trouve pas auprès de lui, attachée à son lit. Peut-être qu'il aurait du le faire, peut-être qu'ainsi elle le respecterait plus. Les vitres polarisées projettent sur son environnement le contenu des alertes qu'il a programmé. Une liste de mots clefs à garder à l'œil, quelques posts pour lesquels il espère encore voir un engagement. Les chiffres ne bougent plus. Les espaces interstitiels ne gravitent plus autour des mots clefs habituels du mouvement. Sa messagerie personnelle est désespérément vide.
Le seul chiffre qui monte encore sont les réactions sur le papier publié par les idols. Des centaines de partage du lien s'accumulent sous le compteur dont le reflet éclaire le visage de Giuse-P. Il est lucide. Il sait que le mouvement est mort, et que les idols sont à blâmer, qu'elles ont été l'instrument de sa chute. Et il sait aussi qu'il ne peut rien y faire. La machine mémétique de la cabale, nourrie au scripts mémétique du consortium vont finir le travail, et il ne sera bientôt plus personne d'important.
Une vague de réponse déferle soudainement sur l'article. Des centaines de messages, probablement plus, le compteur défile trop rapidement pour qu'il ne puisse savoir exactement combien de mentions sont ajoutées à ce flux.
"Yas!" hurle-t-il à destination de son assistante, tout en enfilant à la hâte les fringues qu'il parvient à retrouver dans la pénombre. Ce pic d'activité n'est pas bon. "Bordel, Yas! J'ai besoin de toi!" Pas de réponse, ni de mouvement. Elle doit  être ailleurs. Il aurait vraiment dû mieux la dresser, la garder près de lui à tout moment. Il tâtonne dans la pénombre et retrouve son communicateur.
"Il se passe quoi sur ce message?" envoie-t-il par messagerie directe à Yas, lui indiquant le message contenant l'article. Il ne comprend pas, ce n'est pas un phénomène normal. Personne ne repartage aussi fort un article qui a déjà fait le tour de l'infosphère.
Il franchit la porte de son compartiment, ressoudée à la hâte suite à l'intervention de Simena et la lumière vive de la zone de vie commune le saisi, le faisant presque tituber. Ses pupilles s'habituent à la lumière blanche, avant qu'il ne descende dans les entrailles du Monster Bash, dans la zone collective, d'où il compte trouver suffisamment de stimulant pour se réveiller et attaquer la journée.
Yas ne répond pas. Et elle n'est pas en bas non plus. Il est seul dans ce lieu. Même les derniers imbéciles comme Bay Lee ont finit par l'abandonner. Et pas d'ersatz caféiné, ni de beignets. Juste d'infâme barres protéinées, abandonnée là par tout le monde. Il ne reconnaît même pas l'emballage. De la bouffe de pilote, juge-t-il, avant de les ignorer.
Il s'installe à l'une des tables désertée, et reprend son communicateur. Les réactions sur l'article des idols continuent de défiler. La vague semble se calmer cependant, mais les discussions semblent animées. Il retourne sur la page de l'article, il doit y avoir quelque qui lui a échappé.
Une mise à jour de contenu. Publiée pendant la ronde rouge. Il n'y a pas tellement de contenu en plus, mais de nombreux liens, des paragraphes supplémentaires qui parlent d'une source anonyme et qui décrivent les rouages interne du mouvement. Qui décrivent comment il a manipulé tout le monde, comment il exerçait du contrôle sur son mouvement, comment il a organisé les choses pour essayer de mettre à mal l'infrastructure sociale de Rien à AFoutre de ta thune.
Il devrait se féliciter. Après tout c'est la reconnaissance de son travail, du fait qu'il a de l'impact sur son entourage et que leur soit-disant système de solidarité sociale ne fonctionne pas aussi bien qu'ils ne le pensent tous. Mais il ne sait plus comment ressentir ces émotions. Et tout cela est publié grâce à des sources, des enregistrements confidentiels, des comptes rendus précis de ce qui s'est dit dans leurs réunions.
À mesure qu'il lit le dossier, qu'il remet mentalement le contexte de ce qui s'est dit autour des citations, il réduit sa liste de suspect. Ce ne peut pas être Tik, il y a trop de conversations qui ont eu lieu après son départ. Ce ne peut pas être Bay-Lee, pour le problème inverse. Peut-être que les deux ses sont mis d'accord, et ont conspirés contre lui? Il imagine bien Tik le faire, organiser ça, se venger de lui.
Mais cela aurait nécessité que Bay-Lee enregistre tout ce qu'il se dit, qu'il anticipe les choses, qu'il soit capable de vouloir prendre du pouvoir et ne pas être complètement atavique. Non, ses subalternes ne sont pas capable de ce genre de choses.
"Yas! Où es-tu?", envoie-t-il à son assistante, ne comprenant pas pourquoi celle-ci ne lui répond pas. À moins que. À moins que ce ne soit elle qui soit à l'origine de cette fuite.
Mais pourquoi? Il lui a donné tout ce qu'elle voulait. Et elle n'est pas du genre à vouloir prendre le contrôle du mouvement. Elle n'a pas la volonté pour prendre le contrôle de ce qu'il a fait. Elle ne fait qu'obéir, se mettre dans un coin et écouter ce qu'il se dit, pour son propre profit.
Non, ce ne peut pas être elle. Elle est bien trop loyale, obéissante et impressionnable pour avoir fait ça. Quelqu'un a du pirater son brassard, ou quelque chose du genre. Ou alors on l'a forcé à le faire. C'est probablement les idols, qui l'ont identifiée et qui lui ont mis la main dessus. Les agents de la cabale qui sont après lui, et qui ne respecteront rien