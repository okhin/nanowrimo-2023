title:          Simena
ID:             3821
type:           md
summarySentence:Simena va voir Yas au Monster Bash
POV:            0
notes:          {W:12:Bonecos de empurar}
label:          4
status:         5
compile:        2
setGoal:        750
charCount:      5848


Simena remonte de son chantier. Assez de sculpture pour aujourd'hui. Avant de retourner au Naked Brocoli, elle a promis à Yas qu'elle passerait au Monster Bash. La voca veut lui parler de quelque chose et n'as pas voulu donner beaucoup plus de détail à Simena. À part que Giuse-P ne sera pas là. Et que ça n'a rien à voir avec le mouvement.
Elle traverse le bazar, juste après avoir fait la jonction avec la section en apesanteur, essayant de ne pas se laisser distraire trop par les étals emplis de pièces en tout genre de fringues ou de simples bibelots créés à partir de ce qui ne pourra plus jamais être réparé. Elle est encore sur la défensive, mais personne ne semble ni la remarquer, ni ne vouloir l'insulter. Et pourtant il y a du monde, le bazar grouille d'une petite foule de connaisseurs, de clients potentiels ou de fourgues. Elle sent cependant quelques regards inquiets se poser sur elle, des regards qui finiront par disparaître avec le temps, et à mesure que Rien à foutre de ta thune oubliera cette histoire.
Le bazar se mue progressivement en un espace de jeu et de jardins, alors qu'elle arrive le long de la dorsale de l'anneau, se laissant tirer par le trottoir roulant, jusqu'à hauteur du Monster Bash. Ou du moins de l'escalier lui permettant d'accéder aux coursives de la salle d'arcade. Elle prend son temps pour monter. Pas qu'elle ait besoin de prendre son temps pour ne pas s'essouffler, mais elle a besoin de ce temps pour reléguer ses angoisses au second plan de ses pensées. Yas veut la voir. Giuse-P ne sera pas là. Il y a des gens qui tiennent à elle.
Les bougainvilliers encadrant la façoade ont été fraîchement taillés, et leurs vieilles fleurs supprimée. Mais surtout, Simena entend des voix s'échappant des portes ouvertes du lieu. Yas est assise sur un canapé dont le faux cuir a connu des jours meilleurs, et qui a été installé dans la coursive, pour profiter de la lumière à l'extérieur.
"Hey ! Simena ! Par ici" lui dit-elle, manifestement enthousiaste de voir l'hybride arriver jusqu'à elle. Elle saute sur ses pieds en enfournant dans ses poches les appareils quelle avait en main quelques instants plus tôt, avant d'attraper Simena dans ses bras.
Elles s'installent à la terrasse et Yas lui raconte ses derniers développements. Comment elle a mis Giuse-P dehors, comment elle a trouvé un groupe d'amies autour de Jooly, comment maintenant du monde reviens au Monster Bash. Simena laisse Yas faire la conversation, profitant d'une cannette d'un jus quelconque que Yas lui a collé dans la main.
"J'ai juste un service à te demander, finit par dire Yas, une fois que le flot de nouvelles se tarit. Tu bosses toujours sur ce protocole de partage avec Tabula Rasa là?"
"Ouais. Ça commence à ressembler à quelque chose." Simena n'as pas envie de trop parler de ce projet qui lui prend tout son temps, y compris quand elle est en bas, à sculpter les cloisons. Ou avec Xicha, à caresser son corps. "Pourquoi ?"
Yas baisse le regard, et sort de sa poche les restes de l'appareil photo de Giuse-P.
"J'ai… besoin d'être sûre qu'aucune copie de ce qu'il y a là-dessus ne puisse être partagé," le ton de sa voix à changé, elle a perdu son entrain.
"Il y a quoi là-dessus?" demande Simena.
Yas lui raconte les photos. Le chantage de Giuse-P. Qu'elle n'a pas envie qu'on puisse échanger librement ces photos. Simena ne peut pas vraiment lui promettre que ces photos ne seront jamais publiées ou partagées. Il suffit de les modifier très légèrement pour éviter tout systèmes de détection algorithmique. Et il est toujours possible de faire du partage hors ligne.
"Je… ne peux pas te promettre ça. Si Giuse-P veut publier ces photos, il trouvera un moyen." Répond Simena, préférant être honnête que mentir pour réconforter son amie. "C'est nul comme sentiment. De se sentir impuissante, de se débattre contre la bêtise des autres et de continuer à se faire traîner dans la fange. Crois moi, j'en ai une petite idée."
"Et je peux faire quoi alors?" demande Yas, retenant les larmes qu'elle cache depuis trop de temps.
"Rien. Te relever, et continuer d'avancer. Tu ne peux pas faire comme si ça n'existait pas. Tout ce que tu peux faire c'est te relever."
"Et lui… Il va s'en sortir? Il peut me foutre à poil et s'en sortir, comme ça?"
"Non. On peut sans doute le coincer dans un sas et laisser faire le vide. On peut sans doute le battre comme plâtre et le laisser à vie sous respirateur. On peut faire plein de choses. Mais rien de tout ça ne t'aidera à marcher la tête haute, à ne pas avoir honte sans raison."
"Mais j'ai quand même envie de le faire."
"Je sais."
Yas s'enfonce dans les bras de Simena, et hoquette en pleurant.
"Tout va bien?" demande une grande noire au cheveux rouge sortant delas salle d'arcade.
"Ça ira, répond Yas, se rasseyant à côté de Simena en essuyant ses larmes d'un revers de la main, Simena, c'est Jooly, meilleure joueuse de DDR de ce côté de la ceinture d'astéroïdes. Jooly, voici Simena, extraordinaire pilote de Chernobyl."
"Salut," répond Jooly, accompagnant ses paroles d'un bref geste de la main. "Si tu as finis ici, je te rappelle que tu nous doit une partie. Et les filles ont enfin réussies à se mettre d'accord sur le morceau."
Yas remonte sur ses pieds, ajuste sa combinaison et les sangles de son harnais et suit Jooly qui la pousse dans la salle d'arcade. Avant que celle-ci ne disparaisse elle aussi dans le Monster Bash, et alors que les cris d'encouragement ne s'échappent de l'enceinte du lieu, elle se retourne vers Simena et, avec un léger sourire, l'invite à les rejoindre.
Simena décline l'invitation, elle préfère laisser Simena s'amuser avec ses copines. Et elle a aussi envie de rejoindre Xicha au plus vite. Elle se remet en route laissant derrière elle les rifs d'intro d'un des morceaux de _Bonecos de empurar_, à peine couvert par les cliquetis des talons sur les contrôlleurs de jeu.