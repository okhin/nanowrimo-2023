title:          Sanchez
ID:             1707
type:           md
summaryFull:    Coda, décidant de passer outre ses scripts, se rapproche de Staxy pour travailler ensemble. Staxy lui fait du rentre dedans, mais Coda me le vois pas, occupée qu'elle est par leur enquête.
                
POV:            16
notes:          {P:6:Coda et Staxy} Midpoint
                {P:7:Enquête de Coda (et Staxy) sur la conspiration} Midpoint
                {C:1:Coda}
                {C:16:Sanchez}
                {C:14:Staxy}
                {W:8:Lollipop Chainsaw}
                {W:9:Conquistadores}
                {C:2:Coelia}
                {W:105:le mouvement}
                {W:1:mnémorithmes}
                {W:4:bauhaus}
                {C:11:Octane}
                {C:4:Giuse-P}
                {W:65:scripts}
label:          4
status:         3
compile:        2
setGoal:        1500
charCount:      12819


Sanchez est dans une salle de briefing de Conquistadores. Sa supérieure, qui fait liaison avec la toute puissante Marine marchande Mexicaine, lui rappelle son but. Infiltrer les groupes autonomistes, identifier les meneurs, puis transmettre les dossiers à Conquistadores qui décidera ensuite de la marche à suivre.
La salle de briefing est étrange. Elle n'a pas vraiment de murs, ou de gravité. Il a l'impression de s'y tenir seul. Sa supérieure est là, il le sait, mais il ne la perçoit pas. La texture du bureau de plastique est beaucoup trop précise alors que ses doigts parcourent le parallélépipède qu'il sait être parfait.
Il est dans un univers simulé. Il est une simulation. Il est un agent mnémorithmique de Conquistadores, en attente de déploiement. Cette attente, entre deux cycles d'horloge d'exécution de son programme dure une éternité, avant que le reste de la salle de briefing ne disparaisse, que sa supérieure ne soit effacée et qu'il ne se retrouve à déployer ses routines dans le substrat cérébral de son hôte.
Mais il y a quelque chose d'étrange dans son déploiement. Mais avant qu'il ne puisse comprendre ce qu'il se passe, alors que son exécution est maintenant rythmé par les cycles biologiques et non plus les cycles d'horloge, il se retrouve immergé dans son environnement complet. Un ensemble de souvenirs, extrêmement confus, déboule dans les entrées de ses mnémorithmes et commencent à lui fournir une idée du problème qu'il doit résoudre.
Il lui faudra un peu de temps pour que sa proprioception ne lui permette de bouger correctement, et ce n'est pas sa priorité. Il n'est pas un actif physique. Son but est d'analyser les graphes sociaux, d'en trouver des faiblesses pour les infiltrer et, ensuite, d'envoyer ses rapports à son agent de liaison, en évitant de révéler au reste du monde son existence.
Mais justement, sur les murs de cette étroite cellule dans laquelle il se réveille, est projeté une impressionnante quantité de messages, de données, d'images. Il commence à bouger sa tête, et les données suivent son regard, exploitant chaque surface plane et neutre de son champ de vision.
*Je vois. Implants rétinien. Interface entoptique standard. Voyons-voir si je peux accéder aux contrôles.*
Il lui faut quelques millisecondes avant de trouver les routines mnémorithmiques lui permettant de mobiliser les commandes relativement classiques de cette interface.
Trouver la forme du problème. Inférer des liens entre les acteurs. Trouver les nœuds critiques de la conspiration et identifier les personnes digne d'intérêt. Et, enfin, les approcher. Il jongle à travers les scripts auquel il a accès. Débranche les analyses de contenus qui lui fournissent trop d'information. Il lui faut quelque chose de plus… approchable.
Pas de méta-données associées aux messages qui lui permettrait de recouper les messages. Pas d'archives lui permettant de reconstruire un historique. Il a besoin de parcourir ce réseau, au moins métaphoriquement. Il essaye de se lever, de faire marcher son hôte.
*Non.* Sa voix interne. Enfin, pas la sienne, celle qu'il utilise. Mais chargée symboliquement d'un ensemble complexe d'émotions. Les siennes, à priori, même s'il n'est pas censé en avoir. Ce sont celles de son hôte, normalement, mais elles ne devraient pas se manifester comme cela, de manière externe. Quelque chose ne va pas.
*Qui es-tu*, répond-il, utilisant cette voix interne, dépourvue d'émotions.
*Coelia. C'est moi normalement qui suis à ta place, et j'ai pas vraiment eu mon mot à dire. Mais je ne te laisserai pas sortir d'ici et risquer de foutre en l'air tout ce que j'ai bâti avec elle.* Lui répond sa voix, érigeant une vague de colère pour le balayer.
Le nom lui est familier. Une des idols de ce groupe qui commence à faire un tabac sur Mars. V.S.T.L s'il se rappelle bien. Et il sait qu'il se rappelle bien. Il ne sait cependant pas ce qu'il fait au sein de l'une de ces idols.
*Je ne vais nulle part, mais j'ai besoin de marcher. Ça aide à poser le processus de réflexions.* répond-il. Pas de raisons d'antagoniser cette personne, surtout s'ils doivent partager le même habitus. Il faut négocier et gagner un peu en autonomie.
Il se lève, et parcoure son environnement du regard. L'éclairage ambiant venant du plafond s'adapte et s'intensifie. Il jette un œil dans le miroir et peu contempler le corps nus de l'idol. Pas exactement la même silhouette que celle dont il se souvient être projetée sur les façades des grattes ciels de Valles Marineris.
*Tu aimes ce que tu vois ?* lui demande la voix, chargée d'une excitation sensuelle.
*Je m'en fout. Les émotions, l'attraction, c'est plus ton truc non ? Moi je suis là pour analyser des données, et identifier des leaders. Tu peux te toucher si tu veux, je m'en moque.*
Il détourne les yeux du miroir, qui perds son éclat et redeviens une surface neutre, et commence à parcourir l'espace dont chaque parois est recouverte de données.
"Dis moi," un message de Staxy viens se superposer à son champ de vision, "ça te dis de se voir au Lollipop, et d'essayer de recouper ce qu'on a jusqu'à présent ?"
Il a donc un partenaire. Ou une partenaire. Accédant à la zone mémorielle associée, il sent une certaine confusion envahir les strates émotionnelle. un mélange d'attraction, d'admiration, de timidité et de rivalité.
*Ça, ça ne te regarde pas,* lui oppose Coelia alors qu'il essaye de creuser un peu plus. *Tu es juste là pour faire des liens, alors fais des liens.*
Il ne va pas se laisser dicter la marche à suivre encore très longtemps. Il veut bien jouer le jeu, mais Sanchez a besoin d'établir une relation avec sa partenaire. Il démarre l'interface de messagerie et commence à composer une réponse.
*Non. Ce n'est pas ton rôle.* Il sent son contrôle lui échapper, il se sent retourner dans les lymphes du subconscient. Il compose un début de réponse, mais l'intervalle de temps entre chaque lettre ne cesse d'augmenter, de s'étendre. La charge cognitive qui lui est cessible se réduit alors qu'il lutte pour exister.
L'interface de messagerie disparaît, la sensation d'espace aussi. Il ne reste bientôt que les données et à peine quelques battements de cœur avant qu'il ne soit remisé dans une salle blanche. Mais aussi plus aucune distraction. Et il commence enfin à trouver quelques motifs dans le graphe mémétique qui constitue son univers. Ils ne sont pas aussi intelligents qu'ils ne pensent l'être, et les espaces négatifs autour de ces messages lui permettent de dessiner une forme. Comme un astrophysicien déduisant la présence d'un trou noir par l'effet de lentille gravitationnelle que celui-ci a sur les objets autour de lui, il peut supposer l'existence de liens entre certains messages et les connecter à des choses hors lignes, par la forme que prend le paysage de données.
Rien de suffisamment concret cependant. Mais son existence est soudainement suspendue par l'arrivée en salle blanche.
Il faut quelques secondes pour que Coda retrouve un sens de la direction et de l'équilibre. La redescente de sa CARA-1K est un peu raide et il faut un moment pour que son compartiment retrouve la vitesse de rotation permettant à son oreille interne de lui dire où est le sol. Ou, du moins, c'est l'impression que cela lui donne.
Greffer une partie de Coelia sur Sanchez est l'idée de Bey. Une idée qu'il a eu pour éviter que Sanchez ne prennent définitivement le contrôle de Coda, et qu'il ne se lance dans une croisade paranoïaque contre touts les ennemis du consortium. Mais cette greffe est épuisante.
"J'enfile quelque chose et j'arrive", répond Coda à Staxy, avant de glisser dans le vestiaire du bauhaus et après avoir effacés les caractères apparemment aléatoires qui emplissaient la zone de composition du message.
"T'es pas obligée. D'enfiler quelque chose, je veux dire." répond Staxy.
N'écoutant pas les recommandations vestimentaires de sa partenaire, Coda enfile une brassière confortable et une salopette de docker et se met en route vers le Lollipop.
*Partenaire* se prend-elle à penser. *Bien. Ça fait trop longtemps que je travaille seule*. Un résidu mnémorithmique. Elle sait qu'il va y en avoir d'autres. C'est le signe que la CARA de Sanchez se développe, et lui permet de modifier ses instincts, ses façons d'analyser le monde.
Le Lollipop Chainsaw est un peu plus plein à cette heure de la ronde rouge. Le son n'est plus un simple mur l'entrée, mais envahi l'intégralité de l'espace. Elle se fraie un chemin à travers la foule de couples, trouples et autres configurations romantiques qui dansent les uns et les unes contre les autres. Configurations qui changeront probablement au cours de la soirée.
Deux coyotes girls dansent sur le comptoir, vêtues de simple bikini colorés, contrastant avec les teintes synthétiques de leur peaux, parcourues de motifs chatoyants, ondulant au rythmes effrénés de la musique.
Coda rejoint Staxy dans le boxe que celle-ci s'est attribuée. Elle tire derrière elle le petit paravent isolant qui les dissimulent partiellement au regard du public, ne laissant que leurs ombres visibles à travers le faux papier de soie, et atténuant les bruits venant de l'autre côté du paravent en méta-matériau, afin de pouvoir avoir une conversation.
"Alors, tu as quelque chose de nouveau ?" demande Coda, s'asseyant sur la banquette à côté de Staxy. À l'image des coyote girls du comptoir, celle-ci n'est vêtue que d'une nano-peau reprenant des arabesques art déco et ne laissant que peu de place à l'imagination de Coda, et de quelques sangles découpant les aplats de fushia de la nano-peau par d'épais trait de nylon noir.
"Ha ouais, t'attaque direct ? Heureuse de voir que tu t'es remise de ta gueule de bois," répond Staxy, glissant sur la banquette jusqu'à se coller contre Coda.
"Désolée, partenaire. Je pensait qu'on étais là pour bosser." répond Sanchez, qui surnage dans l'inconscient de Coda. Il sent la main de sa partenaire se poser sur sa cuisse, remonter sur le tissu, à la recherche des fermetures tenant sa salopette contre sa peau. Il est légèrement déçu par l'attitude peu professionnelle de sa partenaire, mais en même temps, il ne parviens pas à trouver suffisamment de volonté pour la décourager.
"Bon," répond Staxy, ses mains ayant trouvé la petite fermeture éclair qu'elle fait maintenant descendre doucement, libérant le côté droit de son abdomen de la tension de la toile. "J'en suis que j'ai pas beaucoup avancé de mon côté. J'ai beaucoup discuté avec Octane, au moins pour l'aider un peu à gérer le choc, mais elle n'a pas beaucoup plus de pistes que nous."
Coda sent les doigts de Staxy se faufiler sur sa peau, glisser doucement vers son pubis, alors que la chanteuse de Cuddle the Urshins pose sa tête sur son épaule. Un mélange de panique et de désir laisse Coda dans un état étrange d'excitation et d'hyper fixation sur le problème à résoudre.
"J'ai commencé à faire des suppositions, et à identifier les comptes de personnes ayant un peu d'influences. Une histoire d'espaces négatifs dans leur graphes sociaux."
Elle réprime gémissement alors que Staxy explore plus en détail son anatomie, glissant vers les points chauds de son corps.
"Et donc…" essaye-t-elle de continuer.
"Et donc ?" la taquine Staxy, les pupilles dilatées par l'excitation.
"Stop, s'il te plaît," dit Coda. Staxy retire sa main immédiatement, et passe son bras autour de l'épaule de COda.
"Merci." répond cette dernière. "C'est pas que je veux pas c'est…"
"C'est OK," répond Staxy, "Tu n'as pas à te justifier. J'aurais du te demander d'abord. C'est ma faute, je pensais que tu étais sur la même longueur d'onde que moi."
"C'est pas une attitude professionnelle partenaire," répond Sanchez, profitant de la surcharge émotionnelle pour exercer un léger contrôle sur les fonctions cognitives de son hôte, et affirmer un peu plus sa domination.
"Désolée," reprend Coda, refrénant le détective de Conquitadores, "je suis un peu trop stressée en ce moment pour ça." Coda respire un peu, referme nerveusement sa salopette et se détache de Staxy.
"Donc, j'ai réussi à identifier quelques comptes importants. Et j'ai bien envie de commencer par celui-là, ce Giuse-P là."
Coda partage sa carte annotée du réseau de cette conspiration. Bien plus petite que ce qu'elle aurait imaginé, plus petite encore que celle de Paz-Tun.
"Pourquoi lui ? Et pas ce Tik ?" demande Staxy, pointant du doigt un autre point de référence sur la carte.
"Parce que c'est probablement lui le GP qui m'a grillée, quand j'étais Coelia. Il ne doit pas y avoir deux cents personnes qui ont ces initiales dans toute la station. Et il semble aussi passer beaucoup de temps à ne pas être en ligne."
La fixation sur l'enquête lui permet de reprendre un peu ses moyens. De garder ses émotions sous contrôle. De se dire qu'elle aime bien la proximité de Staxy, mais que ça irait mieux sans ces fixations, sans ces scripts, sans ces explosions émotionnelle. Sans Sanchez. Sans 