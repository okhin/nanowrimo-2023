title:          Staxy
ID:             226
type:           md
summaryFull:    Suivant leurs scripts d'icône, elles se séparent avec une faible cohérence et un sentiment que quelque chose ne va pas.
POV:            14
notes:          {C:1:Coda-6}
                {C:14:Staxy}
                {P:6:Coda et Staxy} Pinch
                {W:1:mnémorithmes}
                {W:65:scripts}
                {W:8:Lollipop Chainsaw}
                {C:11:Octane}
                {W:3:construct artificiel de réminiscence autosuggestive}
                {W:110:coyote girl}
                {W:14:Idols}
                {W:111:bubblegum-japplastic-techno}
                {W:23:Cuddle the Urchins}
                {W:112:interface entoptique}
                {W:15:V.S.T.L}
                {W:25:Vocas}
                {W:24:Syndicat}
label:          4
status:         5
compile:        2
setGoal:        1500
charCount:      8756


Depuis le square de béton, la vue de la foule en colère est intimidante. Même pour Staxy, pourtant habituée à faire face à un public passionné. Mais là, c'est autre chose que l'enthousiasme partagé pour une forme de musique qui assemble la foule hurlante qui envahi l'espace de discussion qu'Octane et d'autres ont essayé de préservé.
Elle se rappelle le défilé de Pak Gul, il y a quelques jours. Des lieux similaires, une fréquentation similaire. La juxtaposition mentale des deux évènement ne sert qu'à la faire réaliser à quel point la situation actuelle est en train de sombrer dans la violence. Elle doit sortir de là, lui hurlent ses synapses. Ainsi qu'Octane, de manière plus directe.
Tant pis pour la réunion. Le monstre composé de corps qui cherchent à les avaler se rapproche, ses nombreuses bouches leur hurlant dessus leur haine, ses yeux emplis de rage déclenchant en elle une réaction de panique, ses poings fermés s'élançant dans sa direction. Il n'y a plus d'intelligence ou de rapports sociaux dans ce qui lui f ait face. Juste de la haine te de la colère qui se nourrit d'un sentiment de frustration.
Octane tire Staxy vers l'arrière. Elle, ainsi que la demi douzaine de personne de son collectif, se précipite dans une des coursives qui s'éloigne de la cour commune, de la foule. Elles descendent les échelles, en se laissant glisser sur les rampes, et se regroupent dans le jardin du boulevard radial.
Au milieu des buissons de groseillers, abritée sous une pergola couvertes de glycines et de bougainvilliers, Staxy se remet à respirer, essayant de calmer sa panique et de reprendre le contrôle sur sa peur.
Autour d'elle, les autres membres du collectifs se sont assis dans l'herbe, essayant de gérer leur stress, se tenant les uns aux autres, laissant leur peur s'exprimer. Au moins quelques instants. Staxy n'est pas incluse dans ces pratique, elle ne fait pas parti du groupe, ielles n'ont pas la bande passante émotionnelle pour l'aider à se gérer.
Elle dit au-revoir à Octane, qui tiens dans ses bras une petite brune en état de choc, lui laisse une ligne directe pour pouvoir parler de ça plus tard et laisse son pilote automatique la guider. Quelque part en elle, comme une suggestion hypnotique, est gravée l'idée d'aller au Lollipop Chainsaw.
Marcher dans l'herbe lui fait du bien, la sensation des herbes hautes qui, à travers le fin tissue de sa combinaison, jouent contre sa peau,  les huiles essentielles qui, s'échappant des fleurs, viennent stimuler ses récepteurs odorifères, les couleurs chatoyantes des corolles des malvacées qui, réfléchissant une partie du spectre lumineux, ponctuent son environnement, toutes ses sensations l'aide à prendre conscience qu'elle ne fait plus face au monstre, qu'elle est dans un endroit en sûreté, que le cauchemar est fini.
Vingt minutes plus tard, elle arrive chez elle, au Lollipop Chainsaw. Elle franchit le mur sonore de japlastic-techno qui agresse ses tympans, et se dirige vers l'arrière salle, vers les vestiaires des Cuddle the Urshins. Elle se change, adopte une tenue plus confortable. Un long pull noir à capuche lui servant de barrière avec le monde extérieur. Puis après s'être aspergée le visage d'eau, pour sortir de la torpeur dans laquelle elle se trouve encore, elle retourne dans la salle commune.
Staxy repère Coda qui l'attend. Assise dans un des boxes du bar, baignée par les teintes roses des néons, entourées des photos des Cuddle the Urshins, laminées sur les cloisons en faux bois, l'interprète de Coelia a le regard perdu dans une entoptique, que seule celle-ci peut percevoir.
Celle-ci voulait parler à Staxy, et cette dernière se demande bien ce qu'une idol bénéficiant de l'audience de Coelia peut vouloir à quelqu'un comme elle. Et Staxy a un peu de mal à dominer une certaine timidité.
"Tu t'en es sortie sans trop de problème ?" demande Staxy, s'asseyant dans le boxe.
Coda referme d'un geste son entoptique et la dévisage.
"Oui, relativement. J'essayais un peu de voir si j'arrivais à identifier du monde dans le flux. Et toi, ça a été ?" répond l'idol.
"Non, pas vraiment. J'ai… C'était…" Staxy ne trouve pas de mots pour décrire comment elle a vécu les choses. Pas encore du moins. Peut-être qu'à un moment, elle parviendra à les poser sur un riff synthétique et que ça pourra en faire une bonne chanson. Peut-être qu'elle arrivera à en parler à quelqu'un de proche, d'intime à un moment. Mais là, elle n'a pas les mots.
Elle sent la chaleur de la main de Coda se poser doucement sur la sienne. Elle réalise qu'elle a les mains moites, qu'elle est sous le choc. Staxy lutte encore contre ses instincts pour trouver les mots, mais échoue. Elle retire instinctivement sa main, et la cache sous la table.
"Ça ira," répond elle. "Tu voulais me parler de quelque chose ?" ajoute-t-elle, changeant de sujet.
"De ça, répond Coda. Je m'intéresse à ce qu'il se passe autour des groupes de travail en ce moment, et des liens que ça pourrait avoir avec ce mème que je vois partout là, col-dash, ou que sait-je. Ce truc là, sur ces tags." Coda fait glisser sur la surface vernie de la table une galerie d'image reprenant le motif qui l'obsède de plus en plus : :-.
Staxy regarde les photos que lui montre Coda, l'écoute lui parler des liens qu'elle a identifié entre :- et ce qu'elle appelle le mouvement. Elle parcourent ensemble la carte des interactions de l'idol, et Staxy se laisse guider n'ayant pas besoin de réfléchir, prenant le temps de digérer le choc.
"Et donc, tu as besoin de mon aide ?" finit-elle par lui demander, interrompant un silence dans la conversation.
"Oui. C'est plus dans ce que tu fais habituellement ce genre d'investigations, non ? Je me disait que tu pourrais m'aiguiller sur une piste, sur une méthode. Parce que je sèche. Je ne trouves pas un évènement d'origine. Et…"
"Et tu ne pensais pas que les choses pouvaient dégénérer comme ça. Je… ne suis pas dans le meilleur état d'esprit pour plonger dans ce sujet. Pour le moment. Et j'ai été prise de cours aussi. Ça fait combien de temps qu'on a pas eu à devoir gérer autant d'hostilité ? Physiquement je veux dire ?"
"Je pensais qu'on était passé au-delà de ça en fait, que recourir à l'intimidation physique ne faisait pas partie des réflexes de notre culture. Je veux dire, c'est ce qu'on fait dans notre branche, non ? Donner aux personnes les possibilités d'exprimer leurs tensions et leurs conflits autrement que par le recours à la violence directe et physique, non ?" répond Coda, une certaine fébrilité dans la voix, visiblement préoccupée par la situation.
Staxy écoute ce que dit Coda, de sa façon d'imaginer son rôle d'idol du Syndicat. Staxy, elle, n'a jamais voulu être dans cette position d'avoir autant d'audience. Pas qu'elle n'aime pas ça, mais c'est quelque chose qui est arrivé, accidentellement. De nombreux groupes comme Cuddle ne percent jamais, et leurs membres s'amusent tout autant.
Mais Coda c'est autre chose. Elle a choisit d'abandonner une présence publique au profit d'un outil de propagande. Elle a choisit de passer une partie de sa vie à essayer d'expliquer à RAFT comment vivre ensemble. À proposer des façons différentes de voir les relations interpersonnelle, y compris conflictuelle.
Et elle a fait ce choix, au dépend de sa personnalité, en se dopant émotionnellement aux CARA pour faire face aux scénarios du Syndicat. Staxy ressent soudainement une forte sympathie pour Coda, qui est soumise à ce qui doit être l'échec complet de son travail. Staxy a eu peur ce soir, et il est probable que les incidents lui aient laissés plus de traumatisme qu'elle ne veut bien l'admettre pour le moment. Mais Coda vient de voir le travail de sa vie exploser.
C'est à son tour de tendre sa main pour prendre l'idol dans ses bras, de lui proposer d'être la bouée de sauvetage à laquelle s'accrocher, le temps qu'elles rejoignent une rive émotionnellement stable.
"Écoute, lui dit-elle, on va faire ça ensemble si tu veux. J'avais de toutes façon l'intention de traiter le sujet, d'enquêter là-dessus. Et c'est stupide de faire ça chacune dans notre coin non ?"
Coda hoche de la tête.
"Mais pas ce soir. J'ai besoin de laisser les choses se développer. Toi aussi je pense. On peut passer le reste de la soirée ensemble, à parler de… je sais pas. De trucs. Et on en reparle plus tard."
"Je ne sais pas si on a tant de temps que ça," répond Coda.
Staxy ignore cette remarque. Elles n'ont pas le choix, elles doivent prendre ce temps de toutes façon. Elles ne peuvent rien faire là. Elle ne peut rien faire.
Alors elles finissent par parler musique. Pas de la leur, mais du reste de ce qu'il se fait dans RAFT, ou ailleurs. A parler de soucis de productions, ou de foirage techniques lors de représentations. De sujets légers, qui garde leur esprit éloigné de :- et du mouvement qui le soutiens.