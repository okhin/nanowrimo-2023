title:          Simena
ID:             241
type:           md
summaryFull:    Simena réalise que Xicha lui manque, et elle réétablit le contact avec elle
POV:            0
notes:          {C:0:Simena Von Guyen the Third}
                {C:8:Xicha}
                {C:4:Giuse-P}
                {P:3:Simena et Xicha} Midpoint
                {C:17:Yas}
                {W:11:Monster  Bash}
                {W:6:Naked Broccoli}
                {W:62:Compte social}
                {W:89:beignets}
                {C:12:Tik}
label:          4
status:         5
compile:        2
setGoal:        1500
charCount:      9057


Un des avantages d'être un hybride conçu pour le travail forcé, c'est que les cycles de sommeil sont réparateurs, et arrivent à la demande, se dit Simena, émergeant d'une nuit de sommeil. La couchette dans laquelle iel a dormi n'est cependant pas confortable, et son squelette proteste quelque peu après avoir passé la nuit tassée dans cette fente, découpée à la torche au plasma dans une cloison.
Iel enfile la même combinaison que la veille, et démarre son moniteur de poignet. L'horloge lui indique qu'iel n'a dormi que quatre heures, mais iel se sent en forme, reposé. Peut-être le soulagement d'avoir passé une bonne soirée. La première fois depuis quelques jours.
Son compte social est cependant silencieux. Iel a changé de plateforme, grâce à Octane, mais elle est restée discrète. Qui voudrait lui parler ? Iel essaye de se dire que c'est mieux que les dizaines de milliers de notifications d'avant que son compte au Naked Brocoli ne soit fermé, mais iel a du mal à s'en convaincre.
Surtout, iel n'a pas de nouvelles de Xicha. Et maintenant qu'iel a enfin un peu d'espaces mental pour y réfléchir, iel réalise que c'est qu'il lui manque, qu'iel aimerait avoir des nouvelles de la cuisinière du Naked Brocoli.
Le quartier VIP est silencieux. Autant que peu l'être un habitat spatial du moins. Les rassurants bruits des pompes et circulateurs d'air divers constituent un léger bruit de fond, que la respiration de Yas couvre par moment, lea sortant de ses pensées.
La voca dort dans un des casiers juste à côté du sien. Le rideau qui est censé la protéger des autres, et lui donner un peu d'intimité est absent. Simena la regarde dormir d'un sommeil manifestement agité. Iel la recouvre de la couverture qui est tombé au pied de sa paillasse, essayant de lui offrir une certaine protection contre les regards de celles et ceux qui pourraient passer dans le coin.
Iel descend dans l'espace commun, son estomac commençant é réclamer sa dose journalière de nutriment. Et puis iel a besoin de pouvoir s'étirer un peu, de voir qui est encore là, et de se demander quelle est la suite.
Le comptoir qui sert à séparer la salle commune de l'espace de préparation des repas, est jonché de piles de plats et d'assiettes sales, de verres et de bouteilles vide ou partiellement vide, une odeur d'alcool éventé émane de la pièce déserte.
Simena farfouille dans les maigres réserves, et parvient à s'assembler quelque chose qui peut ressembler à un petit déjeuner. Clairement en-dessous de ce qu'iel aurait pu espérer au Naked Brocoli, mais malgré tout bien au-dessus des standards auquel iel a été habitué dans les puits de mines.
Iel sourit en réalisant qu'iel devient un peu exigeant avec ce qu'iel ciusine ou se prépare à manger. Quelque chose qu'iel à appris après être arrivé sur Rien à foutre de ta thune, qu'iel a appris avec Xicha. Xicha, qui probablement, serait révoltée par l'état dans lequel les habituées ont laissés les lieux ou l'état des stocks.
Et même si ce n'est que réchauffer quelques pâtisserie un peu passée dans les micro-onde pour les accompagner de cette crème épaisse et sucrée qui sert d'accompagnement un peu partout où iel a pu aller dans la station, iel à l'impression de faire de la grande cuisine.
Mais préparer ce petit-déjeuner, lui rappelle la proximité qu'iel avait commencé à avoir avec Xicha, juste avant qu'iel ne soit plus autorisé à participer au Naked Brocoli. Et iels n'ont jamais vraiment eu l'occasion d'en parler, Xicha n'ayant pas repris contact avec ellui. Ce n'est probablement pas de sa faute à elle, mais reste que Simena est un peu amer. Et, pour le moment, elle veut juste finir son petit déjeuner.
Une fois que son estomac à cessé de lui réclamer son du, et s'être assuré qu'iel n'a absolument aucun message, iel se décide finalement à refaire un pas vers Xicha. Iel récupère rapidement son compte via la plateforme du Naked Brocoli. Iel ignore les messages publics, une habitude qu'iel essaye de prendre. Ceux-ci sont généralement encore beaucoup trop en train de parler d'ellui, et iel ne veut pas replonger dans cette spirale de haine, iel veut juste récupérer une adresse de messagerie pour Xicha.
"Hey, c'est Simena," commence iel à écrire, dans le silence du Monster Bash. Iel essaye de trouver une façon de transcrire une partie de ses sentiments en mots et échoue une dizaine de fois. 
Iel finit par se convaincre que ce simple message est suffisant, laissant à Xicha la responsabilité de demander plus d'information. Et iel commence à ranger le comptoir, il faut qu'iel reste occupée et ne pas trop réfléchir, une angoisse commençant à la saisir alors que les minutes s'écoulent, attendant une réponse.
Une notification déclenche une vibration à son poignet, et manque de lui faire lâcher les plats qu'iel a commencé à charger dans le lave vaisselle. Iel les enfourne rapidement dans la machine et se précipite sur son communicateur, pour lire le message.
"Salut Simena," commence le message de Xicha, "C'est un peu la panique ici, et je me retrouve presque seule pour faire tourner le Brocoli. Je n'ai donc pas vraiment le temps de parler avec toi "
Même pas une signature, même pas une phrase pour au moins faire semblant de prendre des nouvelles. Juste Xicha qui lui dit qu'elle ne veut pas lui parler. Une doche froide pour Simena, du carburant pour ses angoisses.
Un autre message arrive et s'affiche sous le précédent.
"maintenant. Désolée, le message est parti trop vite. C'est juste en ce moment que je n'ai pas le temps de te parler. J'espère que tu as réussi à retomber sur tes pattes. Il faut qu'on trouve du temps pour se parler. X"
Elle veut lui parler. L'anxiété est brûlée par une combustion d'excitation et de joie. Une décharge de joie d'une puissance suffisante à l'envoyer en orbite, loin de ses pensées circulaire. Suffisante pour qu'un sourire se dessine sur son groin. Iel ne prend pas trop le temps de réfléchir, et lui répond dans la foulée.
"Ça va, je suis au Monster Bash. Au moins pendant un temps. C'est pas la même ambiance qu'au Naked Brocoli, et les habitués sont un peu étrange. Mais au moins, je peux aller et venir comme je l'entends."
"Contente d'entendre que tu aies trouvé un endroit où tu peux t'installer. Je vais vraiment devoir y aller par contre. X."
Xicha a vraiment l'air débordée. De ce que Siemena en avait vu au Naked Brocoli, elle se maintenait occupée par la gestion du lieu, la participation aux cuisines, mais elle trouvait toujours le temps de parler, de discuter ou d'écouter qui semblait en avoir besoin.
Là, elle semble ne plus avoir le temps pour les autres. Quelque chose se passe au Naked Brocoli, quelque chose qui va au-delà la simple expulsion de Simena.
"Bonnes nouvelles?" lui demande Giuse-P, descendant de la zone VIP, suivi par une Yas vêtue d'un T-shirt, le visage encore complètement pris dans les brumes du sommeil, tirée de son sommeil agité par Giuse-P.
Simena répond rapidement, et iels échangent quelques banalités, le temps que Giuse-P s'asseye au comptoir. Il jette un regard désapprobateur sur l'état de celui-ci et attends manifestement quelque chose.
Yas, laissant échapper un grognement à l'attention de Simena qui doit correspondre à un genre de "Bonjour, je suis désolée, j'aurais bien dormi un peu plus" disparaît dans la réserve et en sort de quoi faire un ensemble de boisson chaude pour elleux trois.
Simena n'a pas encore eu le temps de vraiment prendre ses marques, de comprendre comment se passent les choses au Monster Bash, mais iel à une certitude. Celle que Giuse-P est une personne dont iel doit se méfier. Toujours à attendre que les autres fassent quelque chose pour lui qui est quelque chose qu'iel n'avait pas encore trop vu.
Mais surtout, ce qu'iel a remarqué et la façon dont il regarde Yas à la dérobée. Les regards qu'il lui jette lui rappelle ceux des clients des bars de strip, clients dont iel à fait partie. Iel connaît bien ce regard.
Mias Giuse-P est encore pire que ça. Au moins dans les bars de strips, ou dans les bars à idols, ni les artistes ni les clients ne cherchent à se dissimuler de cette intention de luxure, d'excitation, de fantasme. Mais Giuse-P cache ces regards dès que Yas peut en avoir conscience, lui mentant sur ses intentions vis à vis d'elle.
Il lea surprend à le dévisager alors qu'il déshabille du regard la très jeune voca et, concentrant son attention sur Simena, il lui fait un clin d'œil, un sourire de conspirateur se dessinant sur son visage allongé, comme s'ielles partageaient maintenant un secret commun.
Simena n'a pas envie de se mettre à dos la personne qui peut probablement la jeter dehors, pas maintenant. Et puis iel fait confiance à Yas. Elle peut se défendre elle-même et si elle a besoin d'aide, elle peut venir lea chercher. Simena doit juste s'assurer de lui passer le message d'abord.
Mais en attendant, iel ne veut pas rester là avec Giuse-P à attendre que le temps passe. Iel termine de ranger le comptoir, rapidement, souhaite une bonne journée à Yas et sort du Monster Bash. Iel n'a pas envie de se prendre la tête avec Giuse-P et iel préfère passer du temps à penser à Xicha.