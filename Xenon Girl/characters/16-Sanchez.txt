Name:                Sanchez
ID:                  16
Importance:          0
POV:                 True
Motivation:          être un détective respecté.
Goal:                Arreter les coupables et résoudre l'enquête.
Phrase Summary:      CARA-1K de {W:9:Conquistadores}, utilisée par {C:1:Coda} dans son enquête.
Color:               #85c1c7
